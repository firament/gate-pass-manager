#!/bin/bash


# OBSOLETE (this file '01-Notes/bootstrap-tree-structure.sh')
# Use 'bootstrap-tree-structure.sh'


# Script to setup documentation tree for a project

touch README.md;
touch LICENSE;
# touch .gitignore;

# mkdir -vp 01-Notes;
touch 01-Notes/README.md;
# touch 01-Notes/_lcl-notes.md;
# touch 01-Notes/_lcl-notes-db.md;
# touch 01-Notes/_lcl-commands.md;
# touch 01-Notes/_lcl-sample-codes.md;
# touch 01-Notes/bootstrap-tree-structure.md;

# mkdir -vp 10-Init;
# mkdir -vp 10-Init/110-Client-Docs;
# mkdir -vp 10-Init/120-Features;
# mkdir -vp 10-Init/130-Scope;
# mkdir -vp 10-Init/140-Charter;
touch 10-Init/README.md;
touch 10-Init/110-Client-Docs/README.md;
touch 10-Init/120-Features/README.md;
touch 10-Init/130-Scope/README.md;
touch 10-Init/140-Charter/README.md;

# mkdir -vp 20-Plan;
# mkdir -vp 20-Plan/210-Infrastructure;
# mkdir -vp 20-Plan/220-Schedule;
# mkdir -vp 20-Plan/230-Resources;
# mkdir -vp 20-Plan/240-Sprints;
# mkdir -vp 20-Plan/250-CRs;
# mkdir -vp 20-Plan/260-GuidelinesAndStandards;
# mkdir -vp 20-Plan/270-Process;
# mkdir -vp 20-Plan/280-TechnicalDebt;
touch 20-Plan/README.md;
touch 20-Plan/210-Infrastructure/README.md;
touch 20-Plan/220-Schedule/README.md;
touch 20-Plan/230-Resources/README.md;
touch 20-Plan/240-Sprints/README.md;
touch 20-Plan/250-CRs/README.md;
touch 20-Plan/260-GuidelinesAndStandards/README.md;
touch 20-Plan/270-Process/README.md;
touch 20-Plan/280-TechnicalDebt/README.md;

# mkdir -vp 30-Design/310-DB/3110-SQL;
mkdir -vp 30-Design/310-DB/3111-ProdSeeds;
# touch 30-Design/init-soln.sh
touch 30-Design/README.md;
# touch 30-Design/30-Todo-Design.md;
touch 30-Design/310-DB/README.md;
# touch 30-Design/310-DB/DB-Entities.md
# touch 30-Design/310-DB/DB-Domains.md
# touch 30-Design/310-DB/DB-Seed-Data-GPM.ods
# touch 30-Design/310-DB/DD-dropdowns.md
touch 30-Design/310-DB/3110-SQL/README.md;

#-# touch 30-Design/310-DB/3110-SQL/db-10a-init.sql.txt;
#-# touch 30-Design/310-DB/3110-SQL/db-20a-create-tables.sql.txt;
#-# touch 30-Design/310-DB/3110-SQL/db-30a-create-views.sql.txt;
#-# touch 30-Design/310-DB/3110-SQL/db-50a-seed-data.sql.txt;
#-# touch 30-Design/310-DB/3110-SQL/db-51-seed-data-raw.sql.txt;
#-# touch 30-Design/310-DB/3110-SQL/db-80z-drop-views.sql.txt;
#-# touch 30-Design/310-DB/3110-SQL/db-90z-drop-tables.sql.txt;

mkdir -vp 30-Design/320-Server;
mkdir -vp 30-Design/330-UI-Web;
mkdir -vp 30-Design/380-Artefacts;
mkdir -vp 30-Design/380-Diagrams;
touch 30-Design/320-Server/README.md;
touch 30-Design/330-UI-Web/README.md;
touch 30-Design/380-Artefacts/README.md;
touch 30-Design/380-Diagrams/README.md;
touch 30-Design/VPUML-Model-Customize.md;

# mkdir -vp 40-Develop;
touch 40-Develop/README.md;

mkdir -vp 50-QA/510-Reviews;
mkdir -vp 50-QA/520-PlansAndCases;
mkdir -vp 50-QA/530-Data;
mkdir -vp 50-QA/540-Results;
mkdir -vp 50-QA/550-Automation;
touch 50-QA/README.md;
touch 50-QA/510-Reviews/README.md;
touch 50-QA/520-PlansAndCases/README.md;
touch 50-QA/530-Data/README.md;
touch 50-QA/540-Results/README.md;
touch 50-QA/550-Automation/README.md;

mkdir -vp 60-Release/610-ReleaseNotes;
touch 60-Release/README.md;
touch 60-Release/610-ReleaseNotes/README.md;
touch 60-Release/Changelog.md;

mkdir -vp 70-Track/70-IssueLogs;
mkdir -vp 70-Track/70-StatusUpdates;
touch 70-Track/README.md;
touch 70-Track/70-IssueLogs/README.md;
touch 70-Track/70-StatusUpdates/README.md;

mkdir -vp 90-Miscellaneous/910-ref-docs;
touch 90-Miscellaneous/README.md;
touch 90-Miscellaneous/README.md;
touch 90-Miscellaneous/910-ref-docs/README.md

mkdir -vp xport-out-md;
touch xport-out-md/README.md;
