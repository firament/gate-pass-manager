# Access Camera from browser
# Browser Printing
> Take photos from a browser, and post to server.
> 
> 10-08-2019


## Docs & Specs
- https://w3c.github.io/mediacapture-main/getusermedia.html
	- `navigator.mediaDevices.enumerateDevices()`
	- `getUserMedia()` can only be called from an `HTTPS` URL, `localhost` or a `file://` URL
- https://developer.mozilla.org/en-US/docs/Web/API/Media_Streams_API
- https://developer.mozilla.org/en-US/docs/Web/API/Media_Streams_API/Constraints
- https://developer.mozilla.org/en-US/docs/Web/API/MediaStream

- `Navigator.mediaDevices`
- `navigator.mediaDevices.getUserMedia()`
- `navigator.mediaDevices.enumerateDevices()`


## Links / Notes

- RTCPeerConnection
	- acts simultaneously as both a sink and a source for over-the-network streams.
	- https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection

- https://developers.google.com/web/fundamentals/media/capturing-images/
	- Access the camera interactively
	- Grab a snapshot

- https://honeywellaidc.force.com/supportppr/s/article/How-to-use-HTML5-to-capture-a-camera-image
	- `<input type="file" id="mypic" accept="image/*" capture="camera">`

- https://www.html5rocks.com/en/tutorials/getusermedia/intro/
	- `navigator.mediaDevices.getUserMedia()`
	```html
	<input type="file" accept="image/*;capture=camera">
	<input type="file" accept="video/*;capture=camcorder">
	<input type="file" accept="audio/*;capture=microphone">
	```
	- **Support:**
		- 
    Android 3.0 browser - one of the first implementations. Check out this video to see it in action.
    Chrome for Android (0.16)
    Firefox Mobile 10.0
    iOS6 Safari and Chrome (partial support)

- https://github.com/chrisjohndigital/CameraCaptureJS
	- Backbone web application for HTML5 video capture and playback using getUserMedia and the MediaRecorder API 

- **Test:**
	```js
	function hasGetUserMedia() {
	return !!(navigator.mediaDevices &&
		navigator.mediaDevices.getUserMedia);
	}

	if (hasGetUserMedia()) {
	// Good to go!
	} else {
	alert('getUserMedia() is not supported by your browser');
	}
	```

- Screen Capture API
	- https://developer.mozilla.org/en-US/docs/Web/API/Screen_Capture_API

- WebRTC 
	- Web Real Time Communications
	- https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection
	- https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API


## Media Capture and Streams API
- https://developer.mozilla.org/en-US/docs/Web/API/Media_Streams_API
- Interfaces
    - BlobEvent
    - CanvasCaptureMediaStreamTrack
    - InputDeviceInfo
    - MediaDeviceKind
    - MediaDeviceInfo
    - MediaDevices
    - MediaStream
    - MediaStreamConstraints
    - MediaStreamEvent
    - MediaStreamTrack
    - MediaStreamTrackEvent
    - MediaTrackCapabilities
    - MediaTrackConstraints
    - MediaTrackSettings
    - MediaTrackSupportedConstraints
    - NavigatorUserMedia
    - NavigatorUserMediaError
    - OverconstrainedError
    - URL
- Events
    - addtrack
    - ended
    - muted
    - overconstrained
    - removetrack
    - started
    - unmuted

