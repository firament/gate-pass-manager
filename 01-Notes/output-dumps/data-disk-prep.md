# fdisk Output

#------------------------------------------------------------------------------#
blkid
/dev/sdb1: UUID="3d3284a1-0c7f-422f-9f98-f458a7eae338" TYPE="ext4" PARTUUID="135f77d7-01"
/dev/sda1: LABEL="cloudimg-rootfs" UUID="cd899ca9-1943-46c8-9c55-42fc136ef940" TYPE="ext4" PARTUUID="2f441e81-315b-4f69-8176-3c7b8a16729a"
/dev/sda15: LABEL="UEFI" UUID="66CA-0134" TYPE="vfat" PARTUUID="cddf6177-2b9b-4d68-9685-58a5ef523881"

#------------------------------------------------------------------------------#
dmesg | grep SCSI
[    1.905403] SCSI subsystem initialized
[    2.627211] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 243)
[    3.067650] sd 0:0:0:0: [sda] Attached SCSI disk
[    3.316411] sd 3:0:1:0: [sdb] Attached SCSI disk
[    3.511462] sd 5:0:0:0: [sdc] Attached SCSI disk
[    8.324248] Loading iSCSI transport class v2.0-870.


#------------------------------------------------------------------------------#
sudo fdisk /dev/sdc

Welcome to fdisk (util-linux 2.31.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table.
Created a new DOS disklabel with disk identifier 0x4f371dc9.

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-1073741823, default 2048): 
Last sector, +sectors or +size{K,M,G,T,P} (2048-1073741823, default 1073741823): 

Created a new partition 1 of type 'Linux' and of size 512 GiB.

Disk /dev/sdc: 512 GiB, 549755813888 bytes, 1073741824 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x4f371dc9

Device     Boot Start        End    Sectors  Size Id Type
/dev/sdc1        2048 1073741823 1073739776  512G 83 Linux

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

#------------------------------------------------------------------------------#
sudo mkfs -t ext4 -v -L DB_DATA_DIR /dev/sdc1
mke2fs 1.44.1 (24-Mar-2018)
fs_types for mke2fs.conf resolution: 'ext4'
Filesystem label=DB_DATA_DIR
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
33554432 inodes, 134217472 blocks
6710873 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=2281701376
4096 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Filesystem UUID: fd8d858c-83ea-4ce1-9327-50494445c6a5
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
	4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968, 
	102400000

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (262144 blocks): done
Writing superblocks and filesystem accounting information: done     

#------------------------------------------------------------------------------#
sudo blkid | sort
/dev/sda14: PARTUUID="598610d3-97c7-46d3-aae1-aea425e66b0d"
/dev/sda15: LABEL="UEFI" UUID="66CA-0134" TYPE="vfat" PARTUUID="cddf6177-2b9b-4d68-9685-58a5ef523881"
/dev/sda1: LABEL="cloudimg-rootfs" UUID="cd899ca9-1943-46c8-9c55-42fc136ef940" TYPE="ext4" PARTUUID="2f441e81-315b-4f69-8176-3c7b8a16729a"
/dev/sdb1: UUID="3d3284a1-0c7f-422f-9f98-f458a7eae338" TYPE="ext4" PARTUUID="135f77d7-01"
/dev/sdc1: LABEL="DB_DATA_DIR" UUID="fd8d858c-83ea-4ce1-9327-50494445c6a5" TYPE="ext4" PARTUUID="4f371dc9-01"

#------------------------------------------------------------------------------#
sudo mount -v -t ext4 /dev/sdc1 /NoveltyData/work-t15/data-disk-2
mount: /dev/sdc1 mounted on /NoveltyData/work-t15/data-disk-2.

#------------------------------------------------------------------------------#
mount -l | grep ^/dev/ | sort
/dev/sda1 on / type ext4 (rw,relatime,discard) [cloudimg-rootfs]
/dev/sda15 on /boot/efi type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro,discard) [UEFI]
/dev/sdb1 on /mnt type ext4 (rw,relatime,x-systemd.requires=cloud-init.service)
/dev/sdc1 on /NoveltyData/work-t15/data-disk-2 type ext4 (rw,relatime) [DB_DATA_DIR]

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
