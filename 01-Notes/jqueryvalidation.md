# JQuery Validation - Ubobtrusive

https://johnnycode.com/2014/03/27/using-jquery-validate-plugin-html5-data-attribute-rules/
data-rule-[rule name separate by dashes]="true" 
data-msg-[rule name separate by dashes]="The message you want."


https://jqueryvalidation.org/documentation/
    required – Makes the element required.
    remote – Requests a resource to check the element for validity.
    minlength – Makes the element require a given minimum length.
    maxlength – Makes the element require a given maximum length.
    rangelength – Makes the element require a given value range.
    min – Makes the element require a given minimum.
    max – Makes the element require a given maximum.
    range – Makes the element require a given value range.
    step – Makes the element require a given step.
    email – Makes the element require a valid email
    url – Makes the element require a valid url
    date – Makes the element require a date.
    dateISO – Makes the element require an ISO date.
    number – Makes the element require a decimal number.
    digits – Makes the element require digits only.
    equalTo – Requires the element to be the same as another one


http://www.mobzystems.com/blog/setting-up-jquery-unobtrusive-validation/

    data-val-required="message". Shows the message when the element has no value
    data-val-length="message" + data-val-length-min="min length" and/or data-val-length-max="max length". Shows the message if the contents of the element are too long or too short
    data-val-number="message" or data-val-date="message". Shows the message when the value of the element is not of the right type. Other types: data-val-creditcard, data-val-digits, data-val-email, data-val-ur 
    data-val-regex="message" + data-val-regex-pattern="^pattern$". Shows the message if the value of the element does not match the pattern
    data-val-equalto="message" + data-val-equalto-other="jQuery selector". Shows the message if the contents of the element are not the same as the contents of the element selected by the jQuery selector (usually "#someid")
    data-val-range="message" + data-val-range-min="min" + data-val-range-max="max". Shows the message if the contents of the element are not in the range. Specify both min and max!


https://gist.github.com/johnnyreilly/5867188
      data-msg-number="The field RangeAndNumberDemo must be a number." 
      data-msg-range="The field RangeAndNumberDemo must be between -20 and 40." 
      data-rule-number="true" 
      data-rule-range="[-20,40]" 
      
      data-msg-date="The field RequiredDateDemo must be a date." 
      data-msg-required="The RequiredDateDemo field is required." 
      data-rule-date="true" 
	  data-rule-required="true" 
