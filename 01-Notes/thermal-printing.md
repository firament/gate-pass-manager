## Links
- https://www.youtube.com/watch?v=MQMD4Pb7jzY
- https://www.neodynamic.com/articles/How-to-print-raw-ESC-POS-commands-from-Javascript/
- https://github.com/mike42/escpos-php/issues/742
- rawbt
- EPSON
    - https://c4b.epson-biz.com/modules/community/index.php?content_id=3
    - https://download.epson-biz.com/modules/pos/index.php?page=soft&pcat=3&scat=57
- Android Bluetooth printer driver for ESC/POS printers
    - https://www.youtube.com/watch?v=2k3-39YyuBI

## Shortlisted Devices
1. [WZATCO 58mm Bluetooth 4.0 Wireless Thermal Receipt Printer](https://www.amazon.in/WZATCO-Bluetooth-Wireless-Compatible-Rechargeable/dp/B07SK6MNHT/)
2. [OVIO 58mm Bluetooth 4.0 Wireless Thermal Receipt Printer](https://www.amazon.in/OVIO-Bluetooth-Wireless-compatible-Rechargeable/dp/B07HCLYH46)
3. [HOIN 58mm Bluetooth + USB Thermal Printer](https://www.amazon.in/HOIN-58mm-Bluetooth-Thermal-Printer/dp/B07SDBW8M5)


| NAME                                                                                                 | Price  | Brand | Wt.  (gms) | Size W (in mm) | Size L (in mm) | Size D (in mm) | Vol       | Paper W (in mm) | Print W (in mm) | DPI  | Emulation     | Note                                    |
| ---------------------------------------------------------------------------------------------------- | ------ | ----- | ---------- | -------------- | -------------- | -------------- | --------- | --------------- | --------------- | ---- | ------------- | --------------------------------------- |
| [AT-E200](https://www.amazon.in/ATPOS-Portable-Wireless-Bluetooth-Rechargeable/dp/B07S251X43)        | 4,750  |       | 224        | 80             | 114            | 50             | 4,56,000  | 58              |                 | 384  | ESC/POS       |                                         |
| [MPT-03](https://www.amazon.in/OVIO-Bluetooth-Wireless-compatible-Rechargeable/dp/B07HCLYH46)        | 4,990  |       | 490        | 120            | 120            | 100            | 14,40,000 | 58              |                 | 384  | ESC/POS/STAR  |                                         |
| [JT58BT](https://www.amazon.in/HOIN-58mm-Bluetooth-Thermal-Printer/dp/B07SDBW8M5)                    | 3,199  |       | 829        | 130            | 180            | 130            | 30,42,000 | 58              |                 |      | ESC/POS/STAR  | Larger size, for desktop usage          |
| [ASHWA](https://www.ngxtechnologies.com/product/android-mobile-handheld-pos-ashwa/)                  | 15,000 |       | -NA-       | 55             | 216            | 82             | 9,74,160  | 58              |                 | 203  | SDK           | Integrated Android and Printer          |
| [ATPOS-E-200](https://www.amazon.in/dp/B07MZRR7LT/)                                                  | 5,000  |       | 224        | 80             | 114            | 50             | 4,56,000  | 58              |                 |      | SDK + ESC/POS | Carry case included                     |
| [WZATCo-H58](https://www.amazon.in/WZATCO-Bluetooth-Wireless-Compatible-Rechargeable/dp/B07SK6MNHT/) | 4,900  |       | 490        | -NA-           | -NA-           | -NA-           | -NA-      | 58              |                 | -NA- | ESC/POS/STAR  | Poor reviews, 57mm paper, 1 AAA battery |


The [ESC/POS](https://play.google.com/store/apps/details?id=com.loopedlabs.escposprintservice&hl=en_IN) is a universal thermal printer driver that can be used for printers from multiple manufacturers. This driver costs Rs. 360/installation/device and is identified for use on all android devices.

The device images are:

## Print from browser
- https://www.neodynamic.com/articles/How-to-print-raw-ESC-POS-commands-from-Javascript/
- https://www.neodynamic.com/downloads/jspm/
- http://code.activestate.com/recipes/578925-print-directly-from-web-application-to-poseps-ther/
- https://medium.com/@davidkelley87/javascript-interface-for-esc-pos-18123ead9219
- NodeJS
    - https://www.npmjs.com/package/escpos
    - https://github.com/bailabs/tiny-esc-pos
- Epson
    - https://download.epson-biz.com/?eid=1413&PHPSESSID=5fce0dacf58a6dd9a905f832cecc60cb
    - https://reference.epson-biz.com/pos/reference/
    - https://download.epson-biz.com/modules/pos/index.php?page=prod&pcat=3&pid=4983
- C#
    - https://github.com/lukevp/ESC-POS-.NET
- Command Reference
    - https://mike42.me/escpos-printer-db/

### Links 2019-Dec-03 23:02:39 
- https://parzibyte.me/blog/en/2019/10/10/print-receipt-thermal-printer-javascript-css-html/
- https://www.neodynamic.com/articles/How-to-print-raw-ESC-POS-commands-from-Javascript/
- https://github.com/imTigger/webapp-hardware-bridge

## Mobile Drivers
> google play store ESC POS BLUETOOTH PRINT

- https://github.com/diegoveloper/quickprinter/

- https://play.google.com/store/apps/details?id=com.loopedlabs.escposprintservice&hl=en_IN
    - https://loopedlabs.com/esc-pos-bluetooth-print-service/
    - Printing via Custom Print Share Intent
    - https://github.com/looped-labs/ESCPOSPrintServiceDemo
    - Bangalore based (Looped Labs Pvt. Ltd. | +91-886 179 7683 | contactus@loopedlabs.com)

- https://quickprinterforandroid.firebaseapp.com/
    - https://play.google.com/store/apps/details?id=pe.diegoveloper.printerserverapp&hl=en_IN

- https://expressexpense.com/blog/express-thermal-print-app-for-ios-and-android/
    - https://play.google.com/store/apps/details?id=com.expressexpense.bluetooth.print&hl=en

- https://play.google.com/store/apps/details?id=ru.a402d.rawbtprinter
- https://play.google.com/store/apps/details?id=infixsoft.imrankst1221.printer&hl=en_IN
    - https://github.com/imrankst1221/Thermal-Printer-in-Android
    - Print Text
    - Print Title
    - Print Unicode
    - Print Icon
- Also 
    - https://star-emea.com/products/webprnt/
    - https://star-emea.com/products/passprnt/

- Looped Labs Pvt. Ltd.
    - Printer drivers
    - 3rd floor, No. 1139, Maruthi Complex, 3rd Stage,
    - BEML Layout, Rajarajeshwarinagar,
    - Bangalore - 560098
    - India
    - +91-886 179 7683
    - contactus@loopedlabs.com

- NGX Technologies Pvt. Ltd. 
    - Printers and devices
    - Bluetooth Thermal Printers
    - #12, 20th Cross, Malagala, Nagarabhavi 2nd Stage, Bengaluru - 560091
    - (+91) 9986015063
    - enquiry@ngxtechnologies.com
    - also see 'Android Mobile Handheld Terminal'
    - Products of Interest
        - BTP120
        - ASHWA

## Notes
 - https://www.freelancer.com/projects/php/connecting-thermal-printer-web-app/
    - I know that it is possible to print with one tap from web apps if you are using a star thermal printer and open your web page/app on star webprnt browser.
 - https://www.epson.co.in/For-Work/Printers/POS-Printers/Epson-TM-P20-2%22-Mobile-Thermal-POS-Receipt-Printer/p/C31CE14061
 - https://github.com/januslo/react-native-bluetooth-escpos-printer
 - 


## Working notes
- Question asked
    - Does this support direct ESC/POS printing?
- https://www.amazon.in/ACURALUSB-Portable-Bluetooth-Compatible-Commands/dp/B07KSJ3CQF?tag=googinhydr18418-21&tag=googinkenshoo-21&ascsubtag=_k_EAIaIQobChMI5LPhk4Ot5QIVVA4rCh148wyREAQYASABEgLNgPD_BwE_k_&gclid=EAIaIQobChMI5LPhk4Ot5QIVVA4rCh148wyREAQYASABEgLNgPD_BwE
- Loyverse Android 
    - https://www.youtube.com/watch?v=VRCNh3o1b7o 
    - 7276699939 Vishal V
- https://www.amazon.in/HOIN-58mm-Bluetooth-Thermal-Printer/dp/B07SDBW8M5
    - To print download an App supporting Bluetooth printer ESC POS BLUETOOTH PRINT from play store or app store) on your phone/ tablet 
    - USB Port with Laptops / Computers. In Bluetooth mode works with Mobiles / Tabs Only
    - 
