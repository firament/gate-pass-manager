# Commands for convenience

## Local Development Commands
- Clean VPUML work files
	```sh
	rm -vf 30-Design/gpm-system-model.vpp.bak_*;
	```

- Clean application log files
	```sh
	rm -vf 40-Develop/gpm_web/wwwroot/nlog-output/*;
	```
- Diagrams export location
	- /MODEL-DIAGRAMS
	- Add symlink for diagrams
	```sh
	sudo ln -fsvT $(realpath 30-Design/380-Diagrams) /MODEL-DIAGRAMS;
	```
- Open port for mobile testing
	```sh
	# Open port and do forward
	sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5020;
	# verify configuration
	sudo iptables -t nat -L
    # get curr addr
    ip addr show
    ip -4 -ts route
	```
	
- Create hotspot
    ```sh
    create_ap -n wlo1 gpm asdfg09876
    ```


## Publish
```sh
# Clean
rm -vfr x-out/GPM-Website/*

# Publish
DAY_TAG=$(date +"%Y%m%d-%s");
OUT_DIR="x-out/GPM-Website/R-${DAY_TAG}";
echo "Building to DIR ${OUT_DIR}";

mkdir -vp ${OUT_DIR};
dotnet publish \
	-v n \
	-f netcoreapp2.2 \
	-c Release \
	-o ${PWD}/${OUT_DIR} \
	40-Develop/gpm_web/gpm_web.csproj \
	2>&1 | tee ${OUT_DIR}-pub.log

# Scrub
rm -vfr ${OUT_DIR}/wwwroot/nlog-output;
rm -vf ${OUT_DIR}/appsettings.Development.json;
rm -vf ${OUT_DIR}/NLog.Development.config;

# Run Test
pushd ${OUT_DIR};
dotnet gpm_web.dll --urls "http://*:5020"
# dotnet gpm_web.dll --urls="http://*:5020;https://*:5030"
popd;

# Pack
pushd ${OUT_DIR}/../
tar cvzf gpmweb-${DAY_TAG}.tar.gz R-${DAY_TAG}
echo "Packed file:"
echo "gpmweb-${DAY_TAG}.tar.gz";
echo "${PWD}/gpmweb-${DAY_TAG}.tar.gz";
popd
```

## Generate data dump
```sh
#	Transient userid, will not exist to provide an attack surface
# 	"server=127.0.0.1;port=3306;user=gpm-dev-rw;password=bcc4986d8b3745d6;database=SSSIHMS_GPM" \
mysqldump \
	--result-file="30-Design/310-DB/3110-SQL/db-80a-seed-raw.sql" \
	--user=gpm-dev-rw \
	--password=bcc4986d8b3745d6 \
	--default-character-set=utf8 \
	--disable-keys=FALSE \
	--create-options=FALSE \
	--order-by-primary=TRUE \
	--extended-insert=FALSE \
	--complete-insert=TRUE \
	--lock-tables=FALSE \
	--add-locks=FALSE \
	--no-create-info=TRUE \
	--no-tablespaces=TRUE \
	--skip-triggers=TRUE \
	--skip-comments=TRUE \
	SSSIHMS_GPM \
	A_DB_INFO \
	DD_CATEGORIES \
	DD_ITEMS \
	GEN_APP_SETTINGS
```

## Scaffold Command
```sh
# Pre-Requisites
# dotnet add package Microsoft.EntityFrameworkCore.Design
# dotnet add package Pomelo.EntityFrameworkCore.MySql

# Clear earlier generations
rm -vrf 40-Develop/gpm.Efc/dbsets/*.cs

pushd 40-Develop/gpm.Efc;

# Using MySql.Data.EntityFrameworkCore - PREFERRED
dotnet ef dbcontext scaffold \
	"server=127.0.0.1;port=3306;user=gpm-dev-rw;password=bcc4986d8b3745d6;database=SSSIHMS_GPM" \
	MySql.Data.EntityFrameworkCore \
	-v -f \
	-d \
	-o dbsets \
	-c GPM_DBContext \
	--framework netcoreapp2.2

# Using Pomelo.EntityFrameworkCore.MySql
dotnet ef dbcontext scaffold \
	"server=127.0.0.1;port=3306;user=gpm-dev-rw;password=bcc4986d8b3745d6;database=SSSIHMS_GPM" \
	Pomelo.EntityFrameworkCore.MySql \
	-v -f \
	-d \
	-o dbsets \
	-c GPM_DBContext \
	--framework netcoreapp2.2

popd;
```
