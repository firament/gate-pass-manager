# Complex forms in MV with Razor

## Links
- http://www.bipinjoshi.net/articles/b1e0b153-47f4-4b29-8583-958aa22d9284.aspx
- https://www.pluralsight.com/guides/asp.net-mvc-getting-default-data-binding-right-for-hierarchical-views
	- Data Binding for Hierarchical Views
- https://www.learnrazorpages.com/razor-pages/model-binding
	- Binding Complex Collections
	- Binding Related Collections to a Complex Object

- **Complex Model Binding**
	- https://docs.microsoft.com/en-us/aspnet/core/mvc/models/model-binding?view=aspnetcore-2.2 **
		- Dictionaries
	- https://www.learnrazorpages.com/razor-pages/model-binding#binding-complex-collections
		```cs
		[BindProperty]
		public List<Contact> Contacts { get; set; }
		```
		```razor
		@for (var i = 0; i < 5; i++)
		{
			<tr>
				<td><input type="text" name="Contacts[@i].FirstName" /></td>
				<td><input type="text" name="Contacts[@i].LastName" /></td>
				<td><input type="text" name="Contacts[@i].Email" /></td>
			</tr>
		}	
		```
		- also see explicit index

	- https://docs.microsoft.com/en-us/aspnet/core/mvc/views/working-with-forms?view=aspnetcore-2.2
		- Navigating child properties
		- Expression names and Collections
		- The following Razor shows how to iterate over a collection:


- https://docs.microsoft.com/en-us/aspnet/core/client-side/libman/?view=aspnetcore-2.2
- https://docs.microsoft.com/en-us/aspnet/core/client-side/using-browserlink?view=aspnetcore-2.2
