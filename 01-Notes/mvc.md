# MVC Notes

## Global exception handling
- https://wakeupandcode.com/handling-errors-in-asp-net-core/
- https://kenhaggerty.com/articles/article/aspnet-core-22-error-and-exception-handling
- https://www.strathweb.com/2018/07/centralized-exception-handling-and-request-validation-in-asp-net-core/
- https://code-maze.com/global-error-handling-aspnetcore/

## API Endpoints in MVC
- use [apicontroller] attribute
- along with

## Error Codes
- Microsoft.AspNetCore.Http.StatusCodes

## Tagging request logs
System.Diagnostics.Activity
HttpContext.TraceIdentifier
correlation ID

https://www.infoworld.com/article/3201975/how-to-use-correlation-ids-in-aspnet-web-api.html
https://github.com/stevejgordon/CorrelationId

https://stackoverflow.com/questions/41323580/using-dependency-injection-with-net-core-class-library-net-standard
https://mcguirev10.com/2018/01/31/net-core-class-library-dependency-injection.html

==> 2
Thread.CurrentPrincipal.Identity.Name in class library
https://tpodolak.com/blog/2017/02/18/asp-net-core-tracking-flow-requests-nlog/


https://github.com/NLog/NLog.Extensions.Logging/wiki/NLog-properties-with-Microsoft-Extension-Logging


https://github.com/NLog/NLog/wiki/Trace-Activity-Id-Layout-Renderer
https://lowleveldesign.org/2012/05/27/grouping-application-traces-using-activityid/
https://github.com/NLog/NLog/wiki/All-Event-Properties-Layout-Renderer
https://github.com/NLog/NLog/wiki/Identity-Layout-Renderer
	${identity:authType=Boolean:separator=String:name=Boolean:isAuthenticated=Boolean}

https://github.com/NLog/NLog/wiki/ProcessId-Layout-Renderer
	${processid}
	
https://github.com/NLog/NLog/wiki/ThreadId-Layout-Renderer
	${threadid}
	
https://github.com/NLog/NLog/wiki/ThreadName-Layout-Renderer
	${threadname}
	
https://github.com/NLog/NLog/wiki/AspNetItem-layout-renderer
	
https://github.com/NLog/NLog/wiki/MDLC-Layout-Renderer
