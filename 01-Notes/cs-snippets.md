# C# Snippets

## Number formats
- https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-numeric-format-strings
- https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-numeric-format-strings


Format specifier 	Name
"0" 	Zero placeholder
"#" 	Digit placeholder
"." 	Decimal point
"," 	Group separator and number scaling

"C" or "c" 	Currency

## String formatting
```cs
String.Format("{0,-10} {1,-10}\n", "Year", "Population");

{index[,alignment][:formatString]} 
```

index
The zero-based index of the argument whose string representation is to be included at this position in the string. If this argument is null, an empty string will be included at this position in the string.

alignment
Optional. A signed integer that indicates the total length of the field into which the argument is inserted and whether it is right-aligned (a positive integer) or left-aligned (a negative integer). If you omit alignment, the string representation of the corresponding argument is inserted in a field with no leading or trailing spaces.

If the value of alignment is less than the length of the argument to be inserted, alignment is ignored and the length of the string representation of the argument is used as the field width.

formatString
Optional. A string that specifies the format of the corresponding argument's result string. If you omit formatString, the corresponding argument's parameterless ToString method is called to produce its string representation.

## Date formats
```
dd MMM yyyy HH:mm tt
```
