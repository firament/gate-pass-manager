# Thread locking
> For exclusive updates of counters, 
> use psuedo-pessimistic locks.

## Notes

## Monitor
- https://docs.microsoft.com/en-us/dotnet/api/system.threading.monitor?view=netcore-2.2
- The Monitor class allows you to synchronize access to a region of code by taking and releasing a lock on a particular object by calling the Monitor.Enter, Monitor.TryEnter, and Monitor.Exit methods. 
- Object locks provide the ability to restrict access to a block of code, commonly called a critical section. 
- While a thread owns the lock for an object, no other thread can acquire that lock. 
- You can also use the Monitor class to ensure that no other thread is allowed to access a section of application code being executed by the lock owner, unless the other thread is executing the code using a different locked object. 
- Note that you can synchronize on an object in multiple application domains if the object used for the lock derives from MarshalByRefObject. 
- Use the Enter and Exit methods to mark the beginning and end of a critical section. 
	```cs
	// Define the lock object.
	var obj = new Object();

	// Define the critical section.
	Monitor.Enter(obj);
	try {
	// Code to execute one thread at a time.
	}
	// catch blocks go here.
	finally {
	Monitor.Exit(obj);
	}
	```

- If a critical section spans an entire method, the locking facility can be achieved by placing the System.Runtime.CompilerServices.MethodImplAttribute on the method, and specifying the Synchronized value in the constructor of System.Runtime.CompilerServices.MethodImplAttribute. 
- When you use this attribute, the Enter and Exit method calls are not needed. 
	```cs
	[MethodImplAttribute(MethodImplOptions.Synchronized)]
	void MethodToLock()
	{
	// Method implementation.
	} 
	```

- https://docs.microsoft.com/en-us/dotnet/api/system.threading.monitor.enter?view=netcore-2.2



## Mutex
> Has issues with cross process locks on POSIX systems

- https://docs.microsoft.com/en-us/dotnet/api/system.threading.mutex?view=netcore-2.2
- https://docs.microsoft.com/en-us/dotnet/standard/threading/mutexes
- https://www.dotnetcurry.com/dotnet/1360/concurrent-programming-dotnet-core
- https://blog.jeremylikness.com/blog/2017-12-15_five-restful-web-design-patterns-implemented-in-asp.net-core-2.0-part-4-optimistic-concurrency/
	```cs
	var original = new Dictionary< int, int >().ToImmutableDictionary();
	var modified = original.Add(key, value);
	```

- in SharedBuffer.Open(), use MemoryMappedFile.CreateFromFile(), which is supported - and has the additional advantage of adding persistence to the buffer, something I needed
- in BufferWithLocks, replace usage of EventWaitHandle (which depends on named mutexes, also unsupported on Linux, though perhaps that has changed - see dotnet/coreclr#3422) with a custom InterLocked.Exchange / spinWait locking

