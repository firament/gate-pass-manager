# Host a asp.net core 2.2 web app on Ubuntu

## Reboot 
```sh
sudo shutdown -r now reason-for-reboot
```

## Links
- https://blog.todotnet.com/2017/07/publishing-and-running-your-asp-net-core-project-on-linux/
- https://docs.microsoft.com/en-us/aspnet/core/fundamentals/servers/kestrel?view=aspnetcore-2.2

- https://o7planning.org/en/11363/redirecting-port-80-443-on-ubuntu-server-using-iptables
	```sh
	sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5000;
	sudo iptables -t nat -L
	```
	- check application running and port used
		- `netstat -ntl`
		```
		Active Internet connections (only servers)
		Proto Recv-Q Send-Q Local Address           Foreign Address         State      
		tcp   0      0      127.0.0.1:5000          0.0.0.0:*               LISTEN     
		==> unning on port 5000
		```
	- command to redirect port 80 traffic to port 5000
		```sh
		sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5000
		```
	- verify that redirect
		```sh
		sudo iptables -t nat -L
		```
	- save the configuration information above to make sure that it is still useful when you restart the OS
	```sh
	sudo sh -c "iptables-save > /etc/iptables.rules"
	sudo apt-get install iptables-persistent	
	```
	- To remove the redirecting of 80 to 5000
	```sh
	sudo iptables -t nat -D PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 5000
	```

- UFW Configuration
	- https://www.linode.com/docs/security/firewalls/configure-firewall-with-ufw/
		- Map port 80 to 5000
		```sh
		# To deny all incoming and allow all outgoing connections
		# ufw default command also allows for the use of the reject parameter
		sudo ufw default allow outgoing
		sudo ufw default deny incoming

		# allow both incoming and outgoing connections on port 22 for SSH, you can run:
		sudo ufw allow ssh
		# or
		sudo ufw allow 22

		# To deny traffic on a certain port
		sudo ufw deny 111

		# allow TCP packets on port 80
		sudo ufw allow 80/tcp
		sudo ufw allow http/tcp

		# enable logging
		# sudo ufw logging low|medium|high
		sudo ufw logging on

		```
		- allow both TCP incoming 20 and 21 ports from any source
	- https://linuxconfig.org/how-to-deny-all-incoming-ports-except-ftp-port-20-and-21-on-ubuntu-18-04-bionic-beaver-linux
		```sh
		sudo ufw enable;	
		sudo ufw allow from any to any port 20,21 proto tcp;
		sudo ufw status verbose;
		```
	- https://help.ubuntu.com/lts/serverguide/firewall.html
	- https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands
		- sudo ufw allow from 15.15.15.0/24 to any port 3306

### Persist IPTables configuration
use the command iptables-save, which writes to stdout.
iptables-save > /etc/network/iptables.rules

The output created by iptables-save can then by read on stdin by iptables-restore. If on a server, without NetworkManager, a common approach is then to use a pre-up command in /etc/network/interfaces.
iface eth0 inet static
        ....
        pre-up iptables-restore < /etc/network/iptables.rules

If you are using NetworkManager it should be possible to run the same command from a script created under /etc/NetworkManager/dispatcher.d/.
commands iptables, iptables-save and iptables-restore are IPv4 only. For IPv6 traffic the equivalent commands are ip6tables, ip6tables-save and ip6tables-restore.

see also https://help.ubuntu.com/community/IptablesHowTo

## Startup Commands
- https://www.kompulsa.com/run-a-program-on-startup-console-on-ubuntu-18-04/
- https://www.tecmint.com/auto-execute-linux-scripts-during-reboot-or-startup/
```sh
chmod +x /etc/rc.d/rc.local
add script at the bottom of the file
```
- http://manpages.ubuntu.com/manpages/bionic/man1/systemd.1.html


## Other Notes
- Listen on all IPs
	- https://weblog.west-wind.com/posts/2016/sep/28/external-network-access-to-kestrel-and-iis-express-in-aspnet-core
	- `dotnet run --urls http://0.0.0.0:5010;https://0.0.0.0:5001`
	- can also set in `40-Develop/gpm_web/Properties/launchSettings.json`
		- with "applicationUrl": "http://0.0.0.0:5000",
	- Defining ASPNETCORE_URLS environment variable.
		- `SET ASPNETCORE_URLS=https://0.0.0.0:5001` or `export ASPNETCORE_URLS=https://0.0.0.0:5001`
	- Via command line passing --server.urls parameter
		- `dotnet run --server.urls=http://0.0.0.0:5001`
	- IPv6
		- `http://::5000;`


## SSH Login
- https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-portal
- Type ssh-keygen -t rsa -b 2048 to create the ssh key
	- file location []
	- passphrase	[]
- cat ~/.ssh/id_rsa.pub
	- output to be pasted when asked for SSH public key
- Also see
	- https://github.com/squillace/azure-linux/blob/master/docs/Advanced-Linux-Tasks/disable-ssh-passwords-on-your-linux-vm-by-configuring-sshd.md


## dotnet core SDK/Runtime
```sh
	echo "Setting up .NET Core now";
	ClearFolder ${DNETCORE_PATH};   # Required to avoid parallel versions
	makeOwnFolder ${DNETCORE_PATH}  # Folder should exist for tar to work
	tar -xz -C ${DNETCORE_PATH} -f ${DNETCORE_TAR};

	tar -xz --strip-components=1 -C ${ATOM_PATH} -f ${ATOM_TAR};
	sudo ln -vsT ${ATOM_PATH}/atom ${PUBLIC_BIN_LOCN}/atom

```

## FTP Server
- Links
	- https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-for-a-user-s-directory-on-ubuntu-18-04
	- https://www.serv-u.com/features/file-transfer-protocol-server-linux/commands
	- https://servertest.online/ftp
		- test authentication, connectivity, encryption and mode
	- https://linuxconfig.org/how-to-setup-ftp-server-on-ubuntu-18-04-bionic-beaver-with-vsftpd

	- Contents of /etc/vsftpd.conf - as required
		- min required is
			- write_enable=YES
			- listen_ipv6=YES
	- https://help.ubuntu.com/lts/serverguide/ftp-server.html

## Apache2 HTTPD
- Lnks
	- https://blog.todotnet.com/2017/07/publishing-and-running-your-asp-net-core-project-on-linux/
	- https://www.digitalocean.com/community/tutorials/how-to-use-apache-http-server-as-reverse-proxy-using-mod_proxy-extension
	- https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-apache?view=aspnetcore-2.2
	- https://medium.com/@sebishenzenn/running-ours-asp-net-core-apps-using-apache-server-with-reverse-proxy-c0784ae7babd
	- https://www.maketecheasier.com/name-based-virtualhost-apache/
	- [Multiple Hosts on one server](https://httpd.apache.org/docs/2.4/vhosts/examples.html)
		```
		ServerName sub1.domain.tld
		ServerAdmin adm1@domain.com

		ServerName sub2.domain.tld
		ServerAdmin adm2@domain.com
		```
		


### 1
- https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-apache?view=aspnetcore-2.2
- Invoke the UseForwardedHeaders method in **Startup.Configure** before calling UseAuthentication or similar authentication scheme middleware. 
- Configure the middleware to forward the X-Forwarded-For and X-Forwarded-Proto headers:
	```sh
	app.UseForwardedHeaders(new ForwardedHeadersOptions
	{
		ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
	});
	app.UseAuthentication();
	```

## Users
- https://www.tecmint.com/find-user-account-info-and-login-details-in-linux/
	- good commands
- https://vitux.com/add-and-manage-user-accounts-in-ubuntu-18-04-lts/
	```sh
	sudo adduser [username]

	# list of users
	awk -F':' '$2 ~ "\$" {print $1}' /etc/shadow

	# lock and unlock any user account
	sudo passwd -l username
	sudo passwd -u username

	# give root privilege to a user
	sudo usermod -aG sudo [username]

	# Other option
	sudo nano visudo
	# Add the following lines in the file:
	# [username] ALL=(ALL) ALL

	# Set or change the password for the user
	sudo passwd [username]

	# delete a user
	sudo deluser [username]
	```

- Groups
	```sh
	# add a new user group
	sudo addgroup [groupname]

	# add an already existing user to a group
	sudo adduser [username] [groupname]

	# view members of a group
	groups username
	id username

	# change the primary group of a user
	sudo usermod -g [newPrimaryGroup] [username]

	# assign a group to a user simultaneously while creating a new user
	sudo useradd -G [groupname] [username]

	```	

- Setup a user with root privilages
	```sh
	sudo useradd -m -g sudo -G sudo scott
	```

## DB
```sh
-- Create new user, with workbench access
CREATE USER 'gpm-dba'@'*' IDENTIFIED BY 'some-password-text';
GRANT ALL PRIVILEGES ON * . * TO 'gpm-dba'@'*';
GRANT GRANT OPTION ON * . * TO 'gpm-dba'@'*';
```
### MySQL Data Directory Relocate
- Links
	- https://hostadvice.com/how-to/how-to-relocate-a-mysql-data-directory-on-ubuntu-18-04/
	- Also see
		- https://hostadvice.com/how-to/how-to-install-and-create-mysql-sandboxes-with-dbdeployer-on-an-ubuntu-18-04-vps-or-dedicated-server/

> New Locn: /mnt/volume- can3-01

### Steps
```sh
NEW_SQL_DIR="/mnt/volume-can3-01";

# before starting
sudo systemctl status apparmor

# Get current directory
select @@datadir;

sudo systemctl stop mysql
sudo systemctl status mysql

# Copy contents to new location
sudo rsync -av /var/lib/mysql ${NEW_SQL_DIR}
sudo mv /var/lib/mysql /var/lib/mysql.bak
sudo chown mysql:mysql ${NEW_SQL_DIR}

# Configuring Your New Data Directory
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
	datadir=${NEW_SQL_DIR}/mysql
	# # Also see
	# log_error = /var/log/mysql/error.log
	# port		= 3306
	
# Enable new directory
sudo nano /etc/apparmor.d/tunables/alias
	alias /var/lib/mysql/ -> ${NEW_SQL_DIR}/mysql/,

sudo mkdir /var/lib/mysql/mysql -p
sudo chown -R mysql:mysql /var/lib/mysql/
sudo systemctl restart apparmor

sudo systemctl start mysql
sudo systemctl status mysql
journalctl -xe

mysql -u sak -p
select @@datadir;

sudo rm -Rf /var/lib/mysql.bak
```

### Trail 2
> Did not work

```sh
sudo ls -l /var/lib/mysql
sudo ls -l /media/sak/LinuxOS/mysql-2/mysql

sudo mv /var/lib/mysql /var/lib/mysql-bak
sudo ln -vsT /media/sak/LinuxOS/mysql-2/mysql /var/lib/mysql

sudo chown -h mysql:mysql /var/lib/mysql
sudo chown mysql:mysql /media/sak/LinuxOS/mysql-2/mysql

sudo systemctl start mysql
sudo systemctl status mysql
journalctl -xe

Trail 2 - roll back
sudo rm -v /var/lib/mysql
sudo mv /var/lib/mysql-bak /var/lib/mysql

sudo systemctl start mysql
journalctl -xe
```

### Files of Interest
/etc/mysql/my.cnf
	/etc/mysql/conf.d/
	/etc/mysql/mysql.conf.d/
/etc/mysql/mysql.conf.d/mysqld.cnf
	/var/run/mysqld/mysqld.sock
	/var/run/mysqld/mysqld.pid
	/var/lib/mysql
	/usr/share/mysql
	/var/log/mysql/error.log

### Trail 3
> Works, compare performance before committing
```sh
SRC_DIR="/media/sak/LinuxOS/mysql-2/mysql";
DST_DIR="/var/lib/mysql";
mkdir -vp ${DST_DIR};
sudo mount -o bind ${SRC_DIR} ${DST_DIR};
# List mounted volumes
mount -l | grep "fuseblk";

sudo ls -l /var/lib/mysql | grep mysql
sudo ls -l /var/lib/ | grep mysql
sudo ls -l /media/sak/LinuxOS/mysql-2/mysql

sudo systemctl stop mysql
sudo systemctl start mysql
sudo systemctl status mysql
journalctl -xe
```

# Test
```sh
mysql -u sak -p
select @@datadir;
```

### Port Change
```sh
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf

mysql -v -P 8804 -u sak -p
select @@datadir;

# mysql [OPTIONS] [database]
# 
#   -D, --database=name Database to use.
#   -h, --host=name     Connect to host.
#   -p, --password[=name] 
#                       Password to use when connecting to server. If password is
#                       not given it's asked from the tty.
#   -P, --port=#        Port number to use for connection or 0 for default to, in
#                       order of preference, my.cnf, $MYSQL_TCP_PORT,
#                       /etc/services, built-in default (3306).
#   -t, --table         Output in table format.
#   --tee=name          Append everything into outfile. See interactive help (\h)
#                       also. Does not work in batch mode. Disable with
#                       --disable-tee. This option is disabled by default.
#   -u, --user=name     User for login if not current user.
#   -v, --verbose       Write more. (-v -v -v gives the table output format).
```


## Mount additional disks
- Links
	- https://docs.microsoft.com/en-us/azure/virtual-machines/linux/attach-disk-portal
	- https://blog.e-zest.com/how-to-create-attach-and-mount-a-disk-to-linux-vm-microsoft-azure

### Steps
- https://docs.microsoft.com/en-us/azure/virtual-machines/linux/attach-disk-portal

```sh
sudo fdisk /dev/sdc
# <- n
# <- p
# <- 1
# <- 
# <- 
# <- p
# <- w

sudo mkfs -t ext4 -v -L DB_DATA_DIR /dev/sdc1

# trail 2
sudo mkdir -vp /NoveltyData/work-t15/data-disk-2
sudo touch /NoveltyData/work-t15/data-disk-2/local-file.txt
ls -l /NoveltyData/work-t15/data-disk-2

sudo mount -v -t ext4 /dev/sdc1 /NoveltyData/work-t15/data-disk-2
ls -l /NoveltyData/work-t15/data-disk-2
sudo touch /NoveltyData/work-t15/data-disk-2/mount-file.txt
ls -l /NoveltyData/work-t15/data-disk-2

sudo -i blkid | sort
# Output
/dev/sda14: PARTUUID="598610d3-97c7-46d3-aae1-aea425e66b0d"
/dev/sda15: LABEL="UEFI" UUID="66CA-0134" TYPE="vfat" PARTUUID="cddf6177-2b9b-4d68-9685-58a5ef523881"
/dev/sda1: LABEL="cloudimg-rootfs" UUID="cd899ca9-1943-46c8-9c55-42fc136ef940" TYPE="ext4" PARTUUID="2f441e81-315b-4f69-8176-3c7b8a16729a"
/dev/sdb1: UUID="3d3284a1-0c7f-422f-9f98-f458a7eae338" TYPE="ext4" PARTUUID="135f77d7-01"
/dev/sdc1: LABEL="DB_DATA_DIR" UUID="fd8d858c-83ea-4ce1-9327-50494445c6a5" TYPE="ext4" PARTUUID="4f371dc9-01"

# Entry of interest
/dev/sdc1: LABEL="DB_DATA_DIR" UUID="fd8d858c-83ea-4ce1-9327-50494445c6a5" TYPE="ext4" PARTUUID="4f371dc9-01"

sudo nano /etc/fstab
UUID=fd8d858c-83ea-4ce1-9327-50494445c6a5   /NoveltyData/work-t15/data-disk-2   ext4   defaults,nofail   1   2

# To be used post mysql install
# UUID=fd8d858c-83ea-4ce1-9327-50494445c6a5   /var/lib/mysql   ext4   defaults,nofail   1   2

```

## SSH Configuration
```sh
sudo ls -l /etc/ssh/
sudo nano /etc/ssh/sshd_config
sudo systemctl restart ssh
```

## Cleanup users
```sh
DROP USER IF EXISTS `nds-dba`@`*`
RENAME USER `nds-dba`@`localhost` TO `nds-dba`@`%`, `dd-move-test`@`*` TO `dd-move-test`@`localhost`
FLUSH PRIVILEGES
SELECT User, Host, plugin, password_expired, password_last_changed, password_lifetime, account_locked FROM mysql.user;

```
