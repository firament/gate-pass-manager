# Notes to refine tasks

## Command syntax
- options of interest.
```
dotnet build -help
Microsoft (R) Build Engine version 16.1.76+g14b0a930a7 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

Syntax:              MSBuild.exe [options] [project file | directory]

  -fileLogger[n]     Logs the build output to a file. By default
                     the file is in the current directory and named
                     "msbuild[n].log". Events from all nodes are combined into
                     a single log. The location of the file and other
                     parameters for the fileLogger can be specified through
                     the addition of the "-fileLoggerParameters[n]" switch.
                     "n" if present can be a digit from 1-9, allowing up to
                     10 file loggers to be attached. (Short form: -fl[n])
    
  -fileLoggerParameters[n]:<parameters>
                     Provides any extra parameters for file loggers.
                     The presence of this switch implies the
                     corresponding -fileLogger[n] switch.
                     "n" if present can be a digit from 1-9.
                     -fileLoggerParameters is also used by any distributed
                     file logger, see description of -distributedFileLogger.
                     (Short form: -flp[n])
                     The same parameters listed for the console logger are
                     available. Some additional available parameters are:
                        LogFile--path to the log file into which the
                            build log will be written.
                        Append--determines if the build log will be appended
                            to or overwrite the log file. Setting the
                            switch appends the build log to the log file;
                            Not setting the switch overwrites the
                            contents of an existing log file.
                            The default is not to append to the log file.
                        Encoding--specifies the encoding for the file,
                            for example, UTF-8, Unicode, or ASCII
                     Default verbosity is Detailed.
                     Examples:
                       -fileLoggerParameters:LogFile=MyLog.log;Append;
                                           Verbosity=diagnostic;Encoding=UTF-8

                       -flp:Summary;Verbosity=minimal;LogFile=msbuild.sum
                       -flp1:warningsonly;logfile=msbuild.wrn
                       -flp2:errorsonly;logfile=msbuild.err
    
  -detailedSummary
                     Shows detailed information at the end of the build
                     about the configurations built and how they were
                     scheduled to nodes.
                     (Short form: -ds)
    
  @<file>            Insert command-line settings from a text file. To specify
                     multiple response files, specify each response file
                     separately.

                     Any response files named "msbuild.rsp" are automatically
                     consumed from the following locations:
                     (1) the directory of msbuild.exe
                     (2) the directory of the first project or solution built
```

