# Browser Printing
> Print from a browser without additional dialogs.
> 
> 10-08-2019

- Key words
	- mozilla silent printing
	- chrome silent printing
	- always_print_silent
	- print.always_print_silent
	- "print to thermal printer from browser"
- https://printjs.crabbly.com/
	- PDF Printing
	- Image Printing
	- https://printjs-4de6.kxcdn.com/print.min.js
	- https://printjs-4de6.kxcdn.com/print.min.css
- https://www.youtube.com/watch?v=C6XHpRTKnzU
	- POS (Point of Sales) + direct print from all web browser to all receipt printer
- https://www.youtube.com/watch?v=MQMD4Pb7jzY
	- POS print direct to printer from all web browsers without plugin
- JSPrintManager
	- https://www.neodynamic.com/products/printing/js-print-manager/
	- Javascript + Client App solution for Client-side Printing
	- without showing or displaying any print dialog box!
- https://www.neodynamic.com/articles/How-to-print-raw-ESC-POS-commands-from-Javascript/
- https://stackoverflow.com/questions/4292373/javascript-print-without-print-dialog-box
	- on Firefox, where you could change the browser setting to enable what they called "Silent Printing", you should set it to "Always"
	- Google Chrome Version 18.xx.xx.xx and you can use flags to turn OFF the printer dialog
		- `--kiosk-noprint`

- **Turn off printer prompts**
	- http://manual.koha-community.org/3.16/en/firefoxreceipt.html
		- Open File > Page Setup
		- Make all the headers and footers blank
		- Set the margins to 0 (zero)
		- `about:config`
			- `print.always_print_silent` = false
			- check what is listed for `print.print_printer`
			- choose Generic/Text Only (or whatever your receipt printer might be named)
	- http://forums.mozillazine.org/viewtopic.php?t=48336
	- `about:config`
	- `"NEW" > "BOOLEAN"`
	- `print.always_print_silent` = `true`
	- Restart the browser.
	- `<a href="#" OnClick="window.print();">PRINT THIS WINDOW</a>`

- **Kiosk Mode**
	- https://www.shoptill-e.com/support/48
	- `chrome --kiosk-printing --kiosk  "https://`
	- `about:flags`
	- Print preview must be enabled for Kiosk Printing to work

- **Page Cut command**
	- https://github.com/bpampuch/pdfmake/issues/1352
		- printer can cut the paper on page break through settings in their device driver
	- https://posguys.com/Faq/Details/2164/How-do-I-get-my-printer-to-cut-when-using-a-Generic-Text-Only-driver
		- The Generic/Text Only driver isn't set to cut by default, so the command needs to be added.
		- Open the start menu and open the Control Panel
		- Select Devices and Printers
		- Right click on the printer and select the Printer Properties.
		- Go to the Printer Commands tab
		- Enter `<1D>V<00>` into the End Print Job field.
		- Hit Apply and then OK.

	- https://www.telpar.com/files/drivers_support/tech_bulletins/GenericTextOnlyPrinterWithCutCommand.pdf

- **Also See**
	- https://www.jeffersonscher.com/res/sumomarklets.html#marginfix
	- https://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/flash/printing/PrintJob.html
	- https://www.codeproject.com/Tips/643952/Print-directly-to-selected-local-or-network-printe
	- http://sikorsky.pro/en/blog/how-to-print-from-a-web-page-to-a-pos-printer

