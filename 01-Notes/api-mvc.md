# MVC API Notes

- Toolsets
    - https://editor.swagger.io/


## Authentication pipeline

## Documentation
- API level info
    - https://github.com/domaindrivendev/Swashbuckle.AspNetCore#retrieve-swagger-directly-from-a-startup-assembly
    - https://aspnetcore.readthedocs.io/en/stable/tutorials/web-api-help-pages-using-swagger.html
    ```cs
    services.ConfigureSwaggerGen(options =>
    {
        options.SingleApiVersion(new Info
        {
            Version = "v1",
            Title = "ToDo API",
            Description = "A simple example ASP.NET Core Web API",
            TermsOfService = "None",
            Contact = new Contact { Name = "Shayne Boyer", Email = "", Url = "http://twitter.com/spboyer"},
            License = new License { Name = "Use under LICX", Url = "http://url.com" }
        });
    });
    ```

- Authentication
    - https://github.com/domaindrivendev/Swashbuckle.AspNetCore#add-security-definitions-and-requirements

- Annotations
    - https://github.com/domaindrivendev/Swashbuckle.AspNetCore#install-and-enable-annotations
    - https://github.com/domaindrivendev/Swashbuckle.AspNetCore#retrieve-swagger-directly-from-a-startup-assembly

- Offline docs
    - https://github.com/domaindrivendev/Swashbuckle.AspNetCore#retrieve-swagger-directly-from-a-startup-assembly

- Other
    - https://github.com/domaindrivendev/Swashbuckle.AspNetCore#retrieve-swagger-directly-from-a-startup-assembly
