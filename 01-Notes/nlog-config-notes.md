# NLog design

## DB Columns

| Col              | Web |  Dll  |
| ---------------- | :-: | :---: |
| ENTRY_ID         |     |       |
| APP_CODE         |     |       |
| APP_INSTANCE_ID  |     |       |
| TIME             |  Y  |   Y   |
| LEVEL            |  Y  |   Y   |
| LOGGER           |  Y  |   Y   |
| MESSAGE          |  Y  |   Y   |
| EXCEPTION        |  Y  |   Y   |
| STACKTRACE       |  Y  |   Y   |
| URL              |  Y  | **N** |
| USER_AGENT       |  Y  | **N** |
| REQUEST_REFERRER |  Y  | **N** |
| ASSEMBLY_VERSION |  Y  |   Y   |
| MACHINE          |  Y  |   Y   |
| CLIENT_IP        |  Y  | **N** |
| CALL_SITE        |  Y  |   Y   |
| ALL_EVENT_PROPS  |  Y  |   Y   |



## Files
- wwwroot/nlog-output/nlog-10-internal.log
	```
	2019-05-29 23:15:29.4853 Info Adding target Debugger Target[debuglogger]
	2019-05-29 23:15:29.5365 Info Configured from an XML element in bin/Debug/netcoreapp2.2/NLog.Development.config...
	2019-05-29 23:15:29.5589 Info Found 195 configuration items	
	```
- wwwroot/nlog-output/nlog-2019-03-01-lib.json
	```json
	{
	"time": "2019-03-01 22:06:51.6461",
	"level": "INFO",
	"logger": "ncr.Efc.DbFuncs.DbViews",
	"message": "1 Candidataes found scored by Member 1",
	"stacktrace": "lambda_method at offset 94 in file:line:column <filename unknown>:0:0\nGetResult at offset 369 in file:line:column \/media\/sak\/70_Current\/Work\/Novelty-Data\/NIMHANS-1902\/40-Develop\/420-WebService\/ncrwebservice\/Controllers\/CandidateListController.cs:74:6\nCandidateListForMember at offset 1905 in file:line:column \/media\/sak\/70_Current\/Work\/Novelty-Data\/NIMHANS-1902\/40-Develop\/420-WebService\/ncr.Efc\/DbFuncs\/DbViews.cs:103:6\n",
	"assembly-version": "1.0.0",
	"machine": "sak",
	"call-site": "ncr.Efc.DbFuncs.DbViews.CandidateListForMember(DbViews.cs:103)"
	}	
	```
- wwwroot/nlog-output/nlog-2019-03-01-web.json
	```json
	{
	"time": "2019-03-01 22:06:44.0391",
	"level": "INFO",
	"logger": "ncrwebservice.Program",
	"message": "Using log file NLog.Development.config",
	"stacktrace": "Main at offset 346 in file:line:column \/40-Develop\/420-WebService\/ncrwebservice\/Program.cs:36:5\n",
	"assembly-version": "1.0.0",
	"machine": "sak",
	"call-site": "ncrwebservice.Program.Main(Program.cs:36)"
	}	
	```
- wwwroot/nlog-output/nlog-gen-2019-03-01.txt
	```
	2019-03-01 22:06:43.9148|INFO|ncrwebservice.Program|init main => public static void Main(string[] args)|ncrwebservice.Program.Main(Program.cs:35)|
	2019-03-01 22:06:44.0391|INFO|ncrwebservice.Program|Using log file NLog.Development.config|ncrwebservice.Program.Main(Program.cs:36)|
	2019-03-01 22:06:51.6265|DEBUG|ncr.Efc.DbFuncs.DbViews|Connection status = Open|ncr.Efc.DbFuncs.DbViews.CandidateListForMember(DbViews.cs:43)|
	2019-03-01 22:06:51.6461|INFO|ncr.Efc.DbFuncs.DbViews|1 Candidataes found scored by Member 1|ncr.Efc.DbFuncs.DbViews.CandidateListForMember(DbViews.cs:103)|	
	```
- wwwroot/nlog-output/nlog-sql-2019-03-01.log
	```
	2019-03-01 22:02:41.1092|INFO|Microsoft.EntityFrameworkCore.Database.Command
	Executed DbCommand (1ms) [Parameters=[@__piRDID_0='?' (DbType = Int32), @__psDepartment_1='?' (Size = 60) (DbType = AnsiString), @__psPost_2='?' (Size = 60) (DbType = AnsiString)], CommandType='Text', CommandTimeout='30']
	SELECT `f.Scores`.`RD_ID`, `f.Scores`.`USERID`, `f.Scores`.`CODE`, `f.Scores`.`ADD_BY`, `f.Scores`.`ADD_ON`, `f.Scores`.`EDIT_BY`, `f.Scores`.`EDIT_ON`, `f.Scores`.`SCORE`, `f.Scores`.`STATUS`
	FROM `NIMHANS_NCR`.`SCORES` AS `f.Scores`
	INNER JOIN (
		SELECT `f0`.`CODE`
		FROM `NIMHANS_NCR`.`CANDIDATES` AS `f0`
		WHERE (((`f0`.`STATUS` = 1) AND (`f0`.`RD_ID` = @__piRDID_0)) AND (`f0`.`DEPARTMENT` = @__psDepartment_1)) AND (`f0`.`POST_APPLIED` = @__psPost_2)
	) AS `t` ON `f.Scores`.`CODE` = `t`.`CODE`
	ORDER BY `t`.`CODE`

	2019-03-01 22:03:29.3551|INFO|Microsoft.EntityFrameworkCore.Database.Command
	Executed DbCommand (61ms) [Parameters=[@__psTicket_0='?' (Size = 60) (DbType = AnsiString)], CommandType='Text', CommandTimeout='30']
	SELECT `f`.`MSID`, `f`.`DEVICE_SIG`, `f`.`ISSUE_TIME`, `f`.`LAST_USED`, `f`.`ROLE`, `f`.`STATUS`, `f`.`TOKEN`, `f`.`USERID`
	FROM `NIMHANS_NCR`.`MAINT_SESSIONS` AS `f`
	WHERE (`f`.`TOKEN` = @__psTicket_0) AND (`f`.`STATUS` = 1)

	2019-03-01 22:06:51.2220|INFO|Microsoft.EntityFrameworkCore.Database.Command
	Executed DbCommand (57ms) [Parameters=[@__psTicket_0='?' (Size = 60) (DbType = AnsiString)], CommandType='Text', CommandTimeout='30']
	SELECT `f`.`MSID`, `f`.`DEVICE_SIG`, `f`.`ISSUE_TIME`, `f`.`LAST_USED`, `f`.`ROLE`, `f`.`STATUS`, `f`.`TOKEN`, `f`.`USERID`
	FROM `NIMHANS_NCR`.`MAINT_SESSIONS` AS `f`
	WHERE (`f`.`TOKEN` = @__psTicket_0) AND (`f`.`STATUS` = 1)
	
	```
- 
