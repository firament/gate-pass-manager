# HTTPS Development cert in Linux

## Links
- https://www.humankode.com/asp-net-core/develop-locally-with-https-self-signed-certificates-and-asp-net-core
- https://devblogs.microsoft.com/aspnet/configuring-https-in-asp-net-core-across-different-platforms/
- https://github.com/dotnet/corefx/issues/32875
	- to load a certificate by a thumbprint in a .NET Core 2.2 application:
- https://andrewlock.net/creating-and-trusting-a-self-signed-certificate-on-linux-for-use-in-kestrel-and-asp-net-core/

- Cert Utilities
	- http://manpages.ubuntu.com/manpages/bionic/man1/certutil.1.html
	- https://chromium.googlesource.com/chromium/src/+/master/docs/linux_cert_management.md
	- https://packages.ubuntu.com/bionic/admin/libnss3-tools
		- `sudo apt-get install libnss3-tools` 

## Steps - A
> https://www.humankode.com/asp-net-core/develop-locally-with-https-self-signed-certificates-and-asp-net-core

- Create a Self-Signed Certificate
	```sh
    sudo openssl req \
        -x509 \
        -nodes \
        -days 365 \
        -newkey rsa:2048 \
        -keyout localhost.key \
        -out localhost.crt \
        -config 01-Notes/samples/cert-localhost.conf \
        -passin pass:YourSecurePassword

		# see also
		# -passin val         Private key password source
		# 					file:pathname and stdin
		# -batch              Do not ask anything during request generation
		# -verbose            Verbose output
		# -subj val           Set or modify request subject




	# ALERT: be sure to enter your password when it prompts for an export password
	sudo openssl pkcs12 -export -out localhost.pfx -inkey localhost.key -in localhost.crt
	```
	- This creates 3 files:
    	1. localhost.cer - The public key for the SSL certificate
    	2. localhost.key - The private key for the SSL certificate
    	3. localhost.pfx - An X509 certificate containing both the public and private key. This is the file that will be used by ASP.NET Core app to serve over HTTPS.
	- Copy the localhost.pfx to the project root folder

- To trust it, add the certificate to the trusted CA root store
- `certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n "localhost" -i localhost.crt`

- Commands
	```sh
	# list the trusted certificates installed with certutil
	certutil -L -d sql:${HOME}/.pki/nssdb

	# delete the certificate from the trusted CA root store
	certutil -D -d sql:${HOME}/.pki/nssdb -n "localhost"
	```

### Configuring HTTPS in ASP.NET Core 2.0
- copy the .pfx certificate to the root of the project directory.
- certificate.json
    ```json
    {
      "certificateSettings": {
        "fileName": "localhost.pfx",
        "password": "YourSecurePassword"
      }
    }
    ```

## Interesting Snippets
- https://docs.microsoft.com/en-us/aspnet/core/security/enforcing-ssl?view=aspnetcore-2.2&tabs=visual-studio
	```cs
	public void ConfigureServices(IServiceCollection services)
	{
		services.AddMvc();

		services.AddHsts(options =>
		{
			options.Preload = true;
			options.IncludeSubDomains = true;
			options.MaxAge = TimeSpan.FromDays(60);
			options.ExcludedHosts.Add("example.com");
			options.ExcludedHosts.Add("www.example.com");
		});

		services.AddHttpsRedirection(options =>
		{
			options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
			options.HttpsPort = 5001;
		});
	}
	```
	
- https://docs.microsoft.com/en-us/aspnet/core/security/cookie-sharing?view=aspnetcore-2.2
