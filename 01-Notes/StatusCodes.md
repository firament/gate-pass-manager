Microsoft.AspNetCore.Mvc.StatusCodeResult
https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.statuscoderesult?view=aspnetcore-2.2

va = new Microsoft.AspNetCore.Mvc.OkResult();                    // 200
va = new Microsoft.AspNetCore.Mvc.NoContentResult();             // 204
va = new Microsoft.AspNetCore.Mvc.BadRequestResult();            // 400
va = new Microsoft.AspNetCore.Mvc.UnauthorizedResult();          // 401
va = new Microsoft.AspNetCore.Mvc.NotFoundResult();              // 404
va = new Microsoft.AspNetCore.Mvc.ConflictResult();              // 409
va = new Microsoft.AspNetCore.Mvc.UnsupportedMediaTypeResult();  // 415
va = new Microsoft.AspNetCore.Mvc.UnprocessableEntityResult();   // 422


// 200	OkResult
// 204	NoContentResult
// 400	BadRequestResult
// 401	UnauthorizedResult
// 404	NotFoundResult
// 409	ConflictResult
// 415	UnsupportedMediaTypeResult
// 422	UnprocessableEntityResult


new System.ArgumentOutOfRangeException() as IActionResult;
