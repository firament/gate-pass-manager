# Doxygen documentation for dotnet core on linux
> See also 
> - [DocFX](https://dotnet.github.io/docfx/tutorial/docfx_getting_started.html)
> - doxygen online editor


- Other dependencies
	- www.graphviz.org
	- sourceforge.net/projects/dia-installer/files/dia/
	- http://sourceforge.net/projects/plantuml/
	- http://www.gnu.org/software/global/
	- http://www.gnu.org/software/global/
	- https://www.mathjax.org/
	- https://github.com/mathjax/MathJax
	- http://www.mcternan.me.uk/mscgen/

## Install from repo
```sh
sudo apt-get install doxygen
graphviz
dia
plantuml
global

```

## Build from source
- http://www.doxygen.nl/download.html
- https://github.com/doxygen/doxygen
```sh
# get
git clone https://github.com/doxygen/doxygen.git
cd doxygen

# configure and build
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make
# To force a fresh build after an earlier check-out simple remove the build directory and redo the steps above. 

# Install
make install

```

