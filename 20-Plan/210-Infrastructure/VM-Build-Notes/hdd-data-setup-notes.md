# Raw Notes

Disk /dev/sdb: 8 GiB, 8589934592 bytes, 16777216 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x135f77d7

Device     Boot Start      End  Sectors Size Id Type
/dev/sdb1         128 16775167 16775040   8G  7 HPFS/NTFS/exFAT


Disk /dev/sda: 30 GiB, 32212254720 bytes, 62914560 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 7F3093F1-9648-4115-8866-7D9CA1F5F44B

Device      Start      End  Sectors  Size Type
/dev/sda1  227328 62914526 62687199 29.9G Linux filesystem
/dev/sda14   2048    10239     8192    4M BIOS boot
/dev/sda15  10240   227327   217088  106M EFI System

Partition table entries are not in disk order.


Disk /dev/sdc: 512 GiB, 549755813888 bytes, 1073741824 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes

--------------------------------------------------------------------------------
/dev/sdb1: UUID="3d3284a1-0c7f-422f-9f98-f458a7eae338" TYPE="ext4" PARTUUID="135f77d7-01"
/dev/sda1: LABEL="cloudimg-rootfs" UUID="cd899ca9-1943-46c8-9c55-42fc136ef940" TYPE="ext4" PARTUUID="2f441e81-315b-4f69-8176-3c7b8a16729a"
/dev/sda15: LABEL="UEFI" UUID="66CA-0134" TYPE="vfat" PARTUUID="cddf6177-2b9b-4d68-9685-58a5ef523881"
/dev/sda14: PARTUUID="598610d3-97c7-46d3-aae1-aea425e66b0d"

--------------------------------------------------------------------------------
/dev/sda1 on / type ext4 (rw,relatime,discard) [cloudimg-rootfs]
/dev/sda15 on /boot/efi type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro,discard) [UEFI]
/dev/sdb1 on /mnt type ext4 (rw,relatime,x-systemd.requires=cloud-init.service)

--------------------------------------------------------------------------------
dmesg | grep SCSI
[    1.905403] SCSI subsystem initialized
[    2.627211] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 243)
[    3.067650] sd 0:0:0:0: [sda] Attached SCSI disk
[    3.316411] sd 3:0:1:0: [sdb] Attached SCSI disk
[    3.511462] sd 5:0:0:0: [sdc] Attached SCSI disk
[    8.324248] Loading iSCSI transport class v2.0-870.


