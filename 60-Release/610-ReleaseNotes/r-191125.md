# Release Note
> Release date: 2019 Nov 15

## In this Update
- Set server time to use IST. Date for applying Quotas was using UTC time.
- Disable caching on generated content. Cache was interfering with availaible quotas on mobile browsers.
- Fix validation error in quota edit form.

***
