#!/bin/bash

# Script to setup documentation tree for a project
# Keep in workspace root.
# Conventions
#	#-# Sample, do NOT run
#	#   code run already

# TODO:
#	Clean up redundant lines

touch README.md;
touch LICENSE;
# touch .gitignore;

# mkdir -vp 01-Notes;
touch 01-Notes/README.md;
# touch 01-Notes/_lcl-notes.md;
# touch 01-Notes/_lcl-notes-db.md;
# touch 01-Notes/_lcl-commands.md;
# touch 01-Notes/_lcl-sample-codes.md;

# mkdir -vp 10-Init;
# mkdir -vp 10-Init/110-Client-Docs;
# mkdir -vp 10-Init/120-Features;
# mkdir -vp 10-Init/130-Scope;
# mkdir -vp 10-Init/140-Charter;

# mkdir -vp 20-Plan;
# mkdir -vp 20-Plan/210-Infrastructure;
# mkdir -vp 20-Plan/220-Schedule;
# mkdir -vp 20-Plan/230-Resources;
# mkdir -vp 20-Plan/240-Sprints;
# mkdir -vp 20-Plan/250-CRs;
# mkdir -vp 20-Plan/260-GuidelinesAndStandards;
# mkdir -vp 20-Plan/270-Process;
# mkdir -vp 20-Plan/280-TechnicalDebt;

# mkdir -vp 30-Design/300-DB/3000-SQL;
mkdir -vp 30-Design/300-DB/3010-ProdSeeds;
# touch 30-Design/init-soln.sh
# touch 30-Design/gpm-system-model.vpp;
# touch 30-Design/30-Todo-Design.md;
# touch 30-Design/300-DB/DB-Entities.md
# touch 30-Design/300-DB/DB-Domains.md
# touch 30-Design/300-DB/DB-Seed-Data-GPM.ods
# touch 30-Design/300-DB/DD-dropdowns.md

mkdir -vp 30-Design/310-UI;
mkdir -vp 30-Design/320-Common;
mkdir -vp 30-Design/330-Business;
mkdir -vp 30-Design/380-Artefacts;
mkdir -vp 30-Design/380-Diagrams;
touch 30-Design/VPUML-Model-Customize.md;

# mkdir -vp 40-Develop;
mkdir -vp 40-Develop/480-Environment-Setup;


mkdir -vp 50-QA/510-PlansAndCases;
# mkdir -vp 50-QA/520-Reviews;
mkdir -vp 50-QA/530-Data;
mkdir -vp 50-QA/540-Results;
mkdir -vp 50-QA/550-Automation;

mkdir -vp 60-Release/610-ReleaseNotes;
touch 60-Release/Changelog.md;

mkdir -vp 70-Track/70-StatusUpdates;
mkdir -vp 70-Track/710-MoMs;
mkdir -vp 70-Track/720-RiskLogs;
mkdir -vp 70-Track/730-ClientDocs;
mkdir -vp 70-Track/750-IssueLogs;

mkdir -vp 90-Miscellaneous/910-ref-docs;
# mkdir -vp 90-Miscellaneous/920-working-buffer;
mkdir -vp xport-out-md;
