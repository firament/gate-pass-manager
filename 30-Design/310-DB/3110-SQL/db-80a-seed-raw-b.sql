
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

INSERT INTO `A_DB_INFO` (`ID`, `APP_CODE`, `VERSION`, `NOTE`, `STATUS`, `ADD_BY`, `ADD_ON`, `EDIT_BY`, `EDIT_ON`) VALUES (1,'GPM_WEB','0.0.1.0','First Working Release',1,0,'2019-08-13 22:15:00',0,'2019-08-13 22:15:00');

INSERT INTO `GEN_APP_SETTINGS` (`ID`, `APP_CODE`, `APP_CONFIG_VER`, `APP_CONFIG`, `STATUS`, `ADD_BY`, `ADD_ON`, `EDIT_BY`, `EDIT_ON`) VALUES (1,'GPM_DEV',1,'{ \"AppVersion\": 1, \"DayDeptCapacities\": {}, \"AuthCookieOpts\": { \"AuthSchemeName\": \"gpmauth\", \"AuthCookieName\": \"gpmauth\", \"AuthCookieDomain\": \"localhost\", \"AccessDenyPath\": \"/Pages/Code_403.html\", \"SignOnPath\": \"/Session/Login/\", \"SignOutPath\": \"/Session/Logout/\", \"ReturnURL_TagName\": \"gpmAuthFailURL\", \"SessionTimeout\": \"00:02:00\" } }',1,0,'2019-09-04 04:17:33',0,'2019-09-04 05:40:10');
INSERT INTO `GEN_APP_SETTINGS` (`ID`, `APP_CODE`, `APP_CONFIG_VER`, `APP_CONFIG`, `STATUS`, `ADD_BY`, `ADD_ON`, `EDIT_BY`, `EDIT_ON`) VALUES (2,'GPM_AZ',2,'{ \"AppVersion\": 2, \"DayDeptCapacities\": {}, \"AuthCookieOpts\": { \"AuthSchemeName\": \"gpmauth\", \"AuthCookieName\": \"gpmauth\", \"AuthCookieDomain\": \"gatepass.sssihms.org\", \"AccessDenyPath\": \"/Pages/Code_403.html\", \"SignOnPath\": \"/Session/Login/\", \"SignOutPath\": \"/Session/Logout/\", \"ReturnURL_TagName\": \"gpmAuthFailURL\", \"SessionTimeout\": \"00:450:00\" } }',1,0,'2019-09-04 05:32:05',0,'2019-09-04 05:40:10');
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

