/*
Patch to add API key in app settings
--
SOURCE LOCATION: $/30-Design/310-DB/3110-SQL/db-50a-seed-p2.sql
Version: 0.0.2.1
--
Generated by sak on 2019-09-26 20:04:46
--
Source Reference: 30-Design/310-DB/3110-SQL/db-80a-seed-raw.sql
*/

UPDATE `GEN_APP_SETTINGS` SET `APP_CONFIG`='{ \"AppVersion\": 1, \"AuthKeyName\": \"gpm-api-key\",\"DayDeptCapacities\": {}, \"AuthCookieOpts\": { \"AuthSchemeName\": \"gpmauth\", \"AuthCookieName\": \"gpmauth\", \"AuthCookieDomain\": \"localhost\", \"AccessDenyPath\": \"/Pages/Code_403.html\", \"SignOnPath\": \"/Session/Login/\", \"SignOutPath\": \"/Session/Logout/\", \"ReturnURL_TagName\": \"gpmAuthFailURL\", \"SessionTimeout\": \"00:02:00\" } }' WHERE `ID`='1';
UPDATE `GEN_APP_SETTINGS` SET `APP_CONFIG`='{ \"AppVersion\": 2, \"AuthKeyName\": \"gpm-api-key\",\"DayDeptCapacities\": {}, \"AuthCookieOpts\": { \"AuthSchemeName\": \"gpmauth\", \"AuthCookieName\": \"gpmauth\", \"AuthCookieDomain\": \"gatepass.sssihms.org\", \"AccessDenyPath\": \"/Pages/Code_403.html\", \"SignOnPath\": \"/Session/Login/\", \"SignOutPath\": \"/Session/Logout/\", \"ReturnURL_TagName\": \"gpmAuthFailURL\", \"SessionTimeout\": \"00:45:00\" } }' WHERE `ID`='2';
