/*
Database Tables Create Script for 'Gate Pass Manager'
--
SOURCE LOCATION: $/30-Design/310-DB/3110-SQL/db-20a-create-tables.ddl
Version: 0.0.2.2
--
Generated by sak on 2019-09-12 15:38:26
*/
CREATE TABLE A_DB_INFO (
  ID       INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Numeric system ID of an entity, used for Primary and Foreign keys. Valid values are 1 to 4,294,967,295. Use AUTO_INCREMENT for PK.', 
  APP_CODE varchar(8) NOT NULL comment 'Application Code to identify consuming application, for use in multi-app shared DB. e.g. "aa54acde"', 
  VERSION  varchar(12) NOT NULL comment 'Version in SEMVER format Major.Minor.Patch.Build', 
  NOTE     varchar(255) NOT NULL comment 'Short note or description. Upto 255 chars.', 
  STATUS   int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "RECORD_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY   INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON   timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY  INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON  timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (ID), 
  CONSTRAINT UNIQ_VERSIONS 
    UNIQUE (APP_CODE, VERSION)) comment='Database structure version history.';
CREATE TABLE ATTACHMENTS (
  ATTACHMENT_ID   INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Numeric ID to uniquely identify an attachment', 
  GATE_PASS_ID    INT UNSIGNED NOT NULL, 
  ATTACHMENT_TYPE int(11) DEFAULT 0 NOT NULL comment 'Flag indicating type of attachment. Should always be a valid value from table "DD_ITEMS" and map to "ATTACHMENT_TYPE" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  ATTACHMENT_DATA varbinary(255) NOT NULL comment 'Raw attachment data. FIXME: set appropiate data type.', 
  NOTE            varchar(255) comment 'Short note or description. Upto 255 chars.', 
  MIME_TYPE       varchar(24) NOT NULL comment 'Mime-type tag to be used for display.', 
  STATUS          int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of tha attachment. Should always be a valid value from table "DD_ITEMS" and map to "ATTACHMENT_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY          INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON          timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY         INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON         timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (ATTACHMENT_ID)) comment='Attachments, Photos or documents, linked to a gate pass.';
CREATE TABLE DD_CATEGORIES (
  DDI_CATG_ID   INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Internal ID of the category.', 
  DDI_CATG_NAME varchar(24) NOT NULL UNIQUE comment 'Text code of the category, generally the enum name used in code.', 
  DDI_CATG_DESC varchar(255) NOT NULL comment 'Description or usage of the category.', 
  STATUS        int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "RECORD_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY        INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON       timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (DDI_CATG_ID)) comment='Lookup (or Dropdown) categories for organizing items. Each category ideally maps to an enum in code and should be synchronized.';
CREATE TABLE DD_ITEMS (
  DDI_CATG_ID   INT UNSIGNED NOT NULL, 
  DDI_CODE      INT UNSIGNED NOT NULL comment 'Code of the individual item. Should map to the corresponding enum value in code.', 
  DDI_DISP_SEQ  SMALLINT UNSIGNED DEFAULT 10000 NOT NULL comment 'Seq to use for custom sorting. Max val 65,535', 
  DDI_CODE_TXT  varchar(8) NOT NULL comment 'For text valued keys, e.g. Country code. If not used, use the same value as DDI_CODE.', 
  DDI_DISP_TEXT varchar(24) NOT NULL comment 'Value to be displayed in the UI dropdowns. Ideally should correspond to the elements of the corresponding ENUMs in code.', 
  DDI_DESC      varchar(255) NOT NULL comment 'Short note or description. Upto 255 chars.', 
  STATUS        int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "RECORD_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY        INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON       timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (DDI_CATG_ID, 
  DDI_CODE)) comment='Dropdown items, generally used as items in UI dropdowns.';
CREATE TABLE GATE_PASS_TEMPLATES (
  TEMPLATE_ID   INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'System generated Unique ID for the template.', 
  PASS_TYPE     int(11) DEFAULT 0 NOT NULL comment 'Flag indicating Gate passes for which the template is applicable. Should always be a valid value from table "DD_ITEMS" and map to "GATE_PASS_TYPE" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  NOTE          varchar(255) comment 'Short note or description. Upto 255 chars.', 
  PASS_TEMPLATE text NOT NULL comment 'Template Content in text format. Convert to binary if needed.', 
  STATUS        int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "RECORD_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY        INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON       timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (TEMPLATE_ID)) comment='Templates to use in printing gate passes.';
CREATE TABLE GATE_PASSES (
  GATE_PASS_ID      INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Numeric system ID of an entity, used for Primary and Foreign keys. Valid values are 1 to 4,294,967,295. Use AUTO_INCREMENT for PK.', 
  PASS_TYPE         int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "GATE_PASS_TYPE" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  PASS_NUM          int(11) DEFAULT 0 NOT NULL comment 'Running numbers for each pass type, reset at midnight. Assigned using pass-number-service.', 
  ISSUE_DATE        date NOT NULL comment 'Date for which the Pass is issued. In date format. Date format needed for fast lookups.', 
  ROOT_GATE_PASS    INT UNSIGNED comment 'Linnk to Gate pass for which this pass is generated as an associated gate pass.', 
  PARTY_COUNT       int(11) DEFAULT 0 NOT NULL comment 'Count of people accompanying the primary entrant (Patient/Visitor). Will be used to generate additional passess.', 
  DEPARTMENT        int(11) DEFAULT 0 NOT NULL comment 'Code of Department that pass is valid for. Should always be a valid value from table "DD_ITEMS" and map to "DEPARTMENTS" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  VISIT_TYPE        int(11) DEFAULT 0 NOT NULL comment 'Flag indicating if visist is first or revisit. Should always be a valid value from table "DD_ITEMS" and map to "VISIT_TYPE" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  HOSPITAL_NUM      varchar(24) comment 'Manually entered Hospital number assigned to a patient on first visit. Should be mandatory for revisit patients.', 
  MOBILE_NUM        varchar(16) comment 'Mobile or Phone, with extension, number in text format.', 
  SUPPLY_DELIVERY   int(11) DEFAULT 0 comment 'Flag indicating if the visitor is delivering supplyies or is meeting someone.. Should always be a valid value from table "DD_ITEMS" and map to "YES_NO" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  PERSON_MEETING    varchar(60) comment 'Full, and Display, Name of the person the visitor is meeting with. Or the full name of the Patient, if provided..', 
  HAS_RESOURCES     int(11) DEFAULT 0 NOT NULL comment 'Flag indicating if any hospital resources are assigned to the patient.. Should always be a valid value from table "DD_ITEMS" and map to "YES_NO" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  WHEELCHAIR_NUM    varchar(20) comment 'Wheel chair number, to reduce complexity of mapping resources.', 
  HAS_VEHICLE       int(11) DEFAULT 0 NOT NULL comment 'Flag indicating a vehicle brought by the patient/visitor is being allowed into the campus. If YES a gate pass for the vehicle should be generated. Should always be a valid value from table "DD_ITEMS" and map to "YES_NO" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  VEHICLE_TYPE      int(11) DEFAULT 0 NOT NULL comment 'Flag indicating Type of the Vehicle. Should always be a valid value from table "DD_ITEMS" and map to "VEHICLE_TYPE" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  VEHICLE_PLATE_NUM varchar(24) comment 'Registration / Plate number of the vehicle to which this Pass is issued to.', 
  NOTE              varchar(255) comment 'Short note or description. Upto 255 chars.', 
  TIME_IN           timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Timestamp value, to capture instants. Instant when the pass was generated. i.e. the instant the entity was granted entry into the campus.', 
  TIME_OUT          timestamp DEFAULT CURRENT_TIMESTAMP NULL comment 'Instant when the entity left the campus. i.e. The instant when the gate pass validity expires.', 
  STEP_OUT          timestamp DEFAULT CURRENT_TIMESTAMP NULL comment 'Instant when the entity stepped out of the campus. For future enhancement.', 
  STATUS            int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "GATE_PASS_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY            INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON            timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added. Also used as ISSUE_DATE.', 
  EDIT_BY           INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON           timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (GATE_PASS_ID), 
  INDEX (PASS_NUM)) comment='Details of each gate pass issued.';
CREATE TABLE GEN_APP_SETTINGS (
  ID             INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Version number to uniquely identify the configuration set.', 
  APP_CODE       varchar(8) NOT NULL comment 'Application Code to identify consuming application, for use in multi-app shared DB. e.g. "aa54acde"', 
  APP_CONFIG_VER int(2) NOT NULL comment 'Version number, for retriving configuration set. Used for multiple environments and instances.', 
  APP_CONFIG     text NOT NULL comment 'Configuration data in JSON or XML format data, stored as string.', 
  STATUS         int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "RECORD_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY         INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON         timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY        INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON        timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (ID)) comment='Serialized configuration data for application wide usage.';
CREATE TABLE MAINT_GATEPASS_LOG (
  MGL_ID       BIGINT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Running number for key', 
  GATE_PASS_ID INT UNSIGNED NOT NULL comment 'Reference to the gate pass that is generating this audit entry.', 
  ACTION_TYPE  int(11) DEFAULT 0 NOT NULL comment 'Type of action that is being logged. Should always be a valid value from table "DD_ITEMS" and map to "PASS_LOG_ACTION" category in "DD_CATEGORIES".', 
  STATUS       int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status, i.e. the status being applied, of the gate pass. Should always be a valid value from table "DD_ITEMS" and map to "GATE_PASS_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that applied this change.', 
  ADD_ON       timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this change is recorded.', 
  PRIMARY KEY (MGL_ID)) comment='History of status changes to gate passes in the system for audit and tracking.';
CREATE TABLE MAINT_LOGIN_HISTORY (
  MLH_ID     int(11) NOT NULL AUTO_INCREMENT, 
  USER_ID    INT UNSIGNED NOT NULL comment 'Unique system id representing the user.', 
  USER_LOGIN varchar(24) NOT NULL comment 'Alpha-Numeric login to be used by users to authenticate into the system.', 
  NOTE       text comment 'Large size to save forensics data if required.', 
  STATUS     int(11) DEFAULT 0 NOT NULL comment 'Flag indicating Audit entry type. Should always be a valid value from table "DD_ITEMS" and map to "LOG_ENTRY_TYPE" category in "DD_CATEGORIES".', 
  ADD_ON     timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  PRIMARY KEY (MLH_ID)) comment='Audit lo of user logins, including failed attempts and resurrections. No indexing applied to avoid perf penalty.';
CREATE TABLE MAINT_PASS_SEQUENCE_CACHE (
  CACHE_DATE_INT bigint(20) NOT NULL AUTO_INCREMENT comment 'Date ticks for which the cache is saved.', 
  RAW_DATA       text NOT NULL comment 'Serialized working data in JSON format.', 
  STATUS         int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "RECORD_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY         INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON         timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY        INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON        timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (CACHE_DATE_INT)) comment='Cache of the working copy used by the application, to provide fallback.';
CREATE TABLE MAINT_SESSIONS (
  MSID       BIGINT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Long keys when large number of records are expected. Valid values are 1 to 18,446,744,073,709,551,615', 
  TOKEN      varchar(255) NOT NULL UNIQUE comment 'Session token issued by app, stored in cookie and will be used to validate session and to retrieve session payload.	', 
  DEVICE_SIG text NOT NULL comment 'Device signature to identify caller, for enhanced security.
TODO: Add reference to guildeline for implementing this feature.', 
  USERID     INT UNSIGNED NOT NULL comment 'UserID for whom the session is being generated. Use ''0'' if user in not yet authenticated, and update the correct value when authentication is complete.', 
  ROLE       varchar(24) comment 'Role(e) the user of this session is authorized for.', 
  ISSUE_TIME timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant when the Session was created in the system.', 
  AUTH_TIME  timestamp DEFAULT CURRENT_TIMESTAMP NULL comment 'Instant when the User was successfully authenticated by the system.', 
  LAST_USED  timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'instant when the session was last active, to calculate session timeouts.', 
  STATUS     INT UNSIGNED DEFAULT 0 NOT NULL comment 'Current status of the session. Maps to SESSION_STATUS category in DD_CATEGORIES.', 
  PRIMARY KEY (MSID));
CREATE TABLE QUOTAS (
  DEPARTMENT    int(11) DEFAULT 0 NOT NULL comment 'Flag indicating Department for which the quota applies. Should always be a valid value from table "DD_ITEMS" and map to "DEPARTMENTS" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  DATE_INT      bigint(20) NOT NULL comment 'Date for which the quota is set. Date in integer format, for better key performance.', 
  `DATE`        date NOT NULL comment 'Date for which the quota is set. In date format.', 
  CAP_NEW_VISIT int(11) DEFAULT 0 NOT NULL comment 'Quota or Capacity Value for First Visits. FIXME: Use Unsigned Number.', 
  CAP_REVISIT   int(11) DEFAULT 0 NOT NULL comment 'Quota or Capacity Value for Revisits. Unsigned Number.', 
  STATUS        int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of quota. Should always be a valid value from table "DD_ITEMS" and map to "QUOTA_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY        INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON       timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (DEPARTMENT, 
  DATE_INT)) comment='Department wise and date wise quotas. If no quota is defined for the date, the defaults will be applied';
CREATE TABLE RESOURCE_ASSETS (
  RESOURCE_ID   INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Numeric system ID of an entity, used for Primary and Foreign keys. Valid values are 1 to 4,294,967,295. Use AUTO_INCREMENT for PK.', 
  RESOURCE_TYPE int(11) DEFAULT 0 NOT NULL comment 'Flag indicating Resource Type. Should always be a valid value from table "DD_ITEMS" and map to "RESOURCE_TYPE" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  RESOURCE_CODE varchar(20) NOT NULL UNIQUE comment 'Code identifying one issuable uint of the resource.', 
  RESOURCE_NAME varchar(60) NOT NULL comment 'Descriptive name of the resource, for human reading.', 
  RESOURCE_NOTE varchar(1020) comment 'General note field, upto 1020 chars.For administrative purpose.', 
  STATUS        int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "TBD" category in "DD_CATEGORIES".', 
  ADD_BY        INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON       timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (RESOURCE_ID)) comment='Master list of all resources that can be issued to a patient/visitor.';
CREATE TABLE RESOURCE_PASS_MAP (
  RESOURCE_ID  INT UNSIGNED NOT NULL, 
  GATE_PASS_ID INT UNSIGNED NOT NULL, 
  STATUS       int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of record. Should always be a valid value from table "DD_ITEMS" and map to "RECORD_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON       timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY      INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON      timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (RESOURCE_ID, 
  GATE_PASS_ID)) comment='Mapping of Resources issued to Patient/Visitor.';
CREATE TABLE SYSTEM_LOG_GPM (
  ID               BIGINT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Long keys when large number of records are expected. Valid values are 1 to 18,446,744,073,709,551,615', 
  APP_CODE         varchar(8) DEFAULT 'GPM' NOT NULL comment 'Code identifying the application logging, for use in multi source sinks.', 
  APP_INSTANCE_ID  varchar(8) DEFAULT '01' NOT NULL comment 'App instance identifier, for use in multi instance sources.', 
  LOG_TIME         timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Time stamp from the logger, may differ from system time.', 
  LOG_LEVEL        varchar(8) NOT NULL, 
  LOGGER           varchar(60) NOT NULL, 
  MESSAGE          text NOT NULL comment 'General text field ~64kb (65,535 chars).', 
  EXCEPTION        text comment 'General text field ~64kb (65,535 chars).', 
  STACKTRACE       text comment 'General text field ~64kb (65,535 chars).', 
  URL              varchar(1020) comment 'General text field, upto 1020 chars.', 
  `USER-AGENT`     varchar(60) comment 'Full, and Display, Name of an entity.', 
  REQUEST_REFERRER varchar(60) comment 'Full, and Display, Name of an entity.', 
  ASSEMBLY_VERSION varchar(24), 
  MACHINE          varchar(60) comment 'Full, and Display, Name of an entity.', 
  CLIENT_IP        varchar(24), 
  CALL_SITE        varchar(1020) comment 'General text field, upto 1020 chars.', 
  ALL_EVENT_PROPS  text comment 'General text field ~64kb (65,535 chars).', 
  ADD_ON           timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Timestamp value, to capture instants.', 
  PRIMARY KEY (ID)) comment='Log sink table, app_code should be suffixed for each application using the database for logging.
TODO: Refine columns and sizes.';
CREATE TABLE USERS (
  USER_ID      INT UNSIGNED NOT NULL AUTO_INCREMENT comment 'Unique system id representing the user.', 
  FULL_NAME    varchar(60) NOT NULL comment 'Full Name of the user.', 
  DISPLAY_NAME varchar(24) NOT NULL comment 'Name to be displayed on the application, prefereble the name the user is commonly known by.', 
  USER_LOGIN   varchar(24) NOT NULL comment 'Alpha-Numeric login to be used by users to authenticate into the system.', 
  USER_PWD     text NOT NULL comment 'Hashed password data, that will be used by the user to authenticate into the system. ~64kb (65,535 chars).', 
  ROLE         int(11) DEFAULT 0 NOT NULL comment 'Flag indicating assigned role of the user. Should always be a valid value from table "DD_ITEMS" and map to "USER_ROLE" category in "DD_CATEGORIES". ''0'' is the default value, and should always be kept as ''NOT-SET'' status to indicate the flag was never set.', 
  LAST_LOGIN   timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant when user last successfully authenticated. ', 
  STATUS       int(11) DEFAULT 0 NOT NULL comment 'Flag indicating current status of the user. Should always be a valid value from table "DD_ITEMS" and map to "USER_STATUS" category in "DD_CATEGORIES".', 
  ADD_BY       INT UNSIGNED NOT NULL comment 'USER_ID of the system User that added this record.', 
  ADD_ON       timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was added.', 
  EDIT_BY      INT UNSIGNED NOT NULL comment 'USER_ID of the system User that last edited this record.', 
  EDIT_ON      timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL comment 'Instant, in DB server time, when this record was last edited.', 
  PRIMARY KEY (USER_ID), 
  CONSTRAINT UNIQ_LOGIN 
    UNIQUE (USER_LOGIN)) comment='Users that can access the system.';
CREATE INDEX DD_ITEMS 
  ON DD_ITEMS (DDI_CATG_ID, STATUS);
CREATE INDEX GATE_PASS_TEMPLATES 
  ON GATE_PASS_TEMPLATES (PASS_TYPE, STATUS);
CREATE INDEX GATE_PASSES 
  ON GATE_PASSES (ISSUE_DATE, PASS_TYPE, PASS_NUM asc);
CREATE INDEX GEN_APP_SETTINGS 
  ON GEN_APP_SETTINGS (APP_CODE, APP_CONFIG_VER);
CREATE INDEX MAINT_GATEPASS_LOG 
  ON MAINT_GATEPASS_LOG (GATE_PASS_ID);
CREATE INDEX MAINT_SESSIONS 
  ON MAINT_SESSIONS (TOKEN);
CREATE INDEX USERS 
  ON USERS (USER_LOGIN);
ALTER TABLE DD_ITEMS ADD CONSTRAINT FKDD_ITEMS184240 FOREIGN KEY (DDI_CATG_ID) REFERENCES DD_CATEGORIES (DDI_CATG_ID);
ALTER TABLE RESOURCE_PASS_MAP ADD CONSTRAINT FKRESOURCE_P699772 FOREIGN KEY (RESOURCE_ID) REFERENCES RESOURCE_ASSETS (RESOURCE_ID);
ALTER TABLE RESOURCE_PASS_MAP ADD CONSTRAINT FKRESOURCE_P318182 FOREIGN KEY (GATE_PASS_ID) REFERENCES GATE_PASSES (GATE_PASS_ID);
ALTER TABLE ATTACHMENTS ADD CONSTRAINT FKATTACHMENT379539 FOREIGN KEY (GATE_PASS_ID) REFERENCES GATE_PASSES (GATE_PASS_ID);
ALTER TABLE GATE_PASSES ADD CONSTRAINT FKGATE_PASSE975569 FOREIGN KEY (ROOT_GATE_PASS) REFERENCES GATE_PASSES (GATE_PASS_ID);
ALTER TABLE MAINT_GATEPASS_LOG ADD CONSTRAINT FKMAINT_GATE290571 FOREIGN KEY (GATE_PASS_ID) REFERENCES GATE_PASSES (GATE_PASS_ID);
