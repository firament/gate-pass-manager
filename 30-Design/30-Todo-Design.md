# TODO Items - Design

## Database ERD
- [ ] Identify table for Audit tracking
- [ ] Add audit tracking for identified tables

***

## Done
### 2019 Aug 19
- [x] Add NOTE column to GATE_PASSES
- [x] Add MAINT_GATE_PASS_LOG to track status changes
- [x] Add table MAINT_LOGIN_HISTORY
- [x] Add PASS_LOG_ACTION to DD_CATG, to use in GATE_PASS_LOG

***

## UML Models
> https://www.visual-paradigm.com/guide/

- [ ] UML Association vs Aggregation vs Composition
	- **Association**
		- When two classes in a model need to communicate with each other, there must be link between them, and that is represented by an association
	- **Aggregation** 
		- A relationship where the child can exist independently of the parent. 
		- Example: Class (parent) and Student (child). Delete the Class and the Students still exist.
	- **Composition** 
		- A relationship where the child cannot exist independent of the parent. 
		- Example: House (parent) and Room (child). Rooms don't exist separate to a House.
	- **Specialization** 
		- The reverse of Generalization means creating new sub classes from an existing class. 
	- **Generalization** aka Inheritance
		- The term used to denote abstraction of common properties into a base class in UML. 
		- A mechanism for combining similar classes of objects into a single, more general class. 
		- Generalization identifies commonalities among a set of entities. 
		- The commonality may be of attributes, behavior, or both.
		- The UML diagram's Generalization association is also known as Inheritance.
 

- [ ] **UML Package Diagram**
	- A structural diagram that shows the arrangement and organization of model elements in middle to large scale project. 
	- Package diagram can show both structure and dependencies between sub-systems or modules, showing different views of a system
	- Package diagrams are used to structure high level system elements. 
    - Package Diagram can be used to simplify complex class diagrams, it can group classes into packages.
    - A package is a collection of logically related UML elements.
    - Packages are depicted as file folders and can be used on any of the UML diagrams.
	- A package is a collection of logically related UML elements.
	- Package diagram follows hierarchal structure of nested packages.
	-  Atomic module for nested package are usually class diagrams.
	- Packages are used for organizing large system which contains diagrams, documents and other key deliverables.


- [ ] **UML Component Diagram**
	- Used in modeling the physical aspects of object-oriented systems that are used for visualizing, specifying, and documenting component-based systems 
	- and also for constructing executable systems through forward and reverse engineering. 
	- Component diagrams are essentially class diagrams that focus on a system's components that often used to model the static implementation view of a system. 
	- A component represents a modular part of a system that encapsulates its contents and whose manifestation is replaceable within its environment.
	- The subsystem classifier is a specialized version of a component classifier. 
	- Because of this, the subsystem notation element inherits all the same rules as the component notation element.


- [ ] **UML Object Diagram**
    - A UML structural diagram that shows the instances of the classifiers in models.
	- Object is an instance of a particular moment in runtime, including objects and data values. 
	- A static UML object diagram is an instance of a class diagram; it shows a snapshot of the detailed state of a system at a point in time, thus an object diagram encompasses objects and their relationships at a point in time. 
	- It may be considered a special case of a class diagram or a communication diagram. 
	- An object diagram shows this relation between the instantiated classes and the defined class, and the relation between these objects in the system.
    - Object diagrams use notation that is similar to that used in class diagrams.
    - Class diagrams show the actual classifiers and their relationships in a system
    - Object diagrams show specific instances of those classifiers and the links between those instances at a point in time.
    - You can create object diagrams by instantiating the classifiers in class, deployment, component, and use-case diagrams.



- [ ] **UML Composite structure diagram**
	- One of the new artifacts added to UML 2.0
	- UML structural diagram that contains classes, interfaces, packages, and their relationships, and that provides a logical view of all, or part of a software system. 
	- It shows the internal structure (including parts and connectors) of a structured classifier or collaboration.
	- Performs a similar role to a class diagram, but allows you to go into further detail in describing the internal structure of multiple classes and showing the interactions between them. 
	- You can graphically represent inner classes and parts and show associations both between and within classes.
	- Purpose
		- Composite Structure Diagrams allow the users to "Peek Inside" an object to see exactly what it is composed of.
		- The internal actions of a class, including the relationships of nested classes, can be detailed.
		- Objects are shown to be defined as a composition of other classified objects.

