#! /bin/bash
#
#	SOURCE LOCATION: $/30-Design/init-soln.sh
#	Version: 0.0.1.1
#
#

readonly PROJECT_TAG="gpm";

## TODO:
# Use ${PROJECT_TAG} to make script usable
# Apply naming conventions to component folders, design
# allows to keep 30-Design and 40-Develop sequences in sync
#	300-DB
#	410-web
#	420-common
#	430-efc
#	440-biz
#	450-other-components...
#
# Use --name parameter to apply Naming convention for components
# 	mkdir -vp 420-Common;cd 420-Common;
# 	dotnet new classlib --language "C#" --framework netcoreapp2.2--name "${PROJECT_TAG}.Common";


# Pre-Requisites
#	dotnet core is initialized

##
#	Prepare solution container
##
pushd 40-Develop;
dotnet new sln -n gpm_solution;
#  -n, --name          The name for the output being created. If no name is specified, the name of the current directory is used.
#  -o, --output        Location to place the generated output.

##
#	Web application
##
mkdir -vp gpm_web; cd gpm_web;
dotnet new mvc --language "C#"; # -au None;
dotnet new webconfig;
dotnet new globaljson;
dotnet add package NLog;	# --version 4.6.6
dotnet add package NLog.Web.AspNetCore;	# --version 4.8.4
dotnet add package Pomelo.EntityFrameworkCore.MySql;	# --version 2.2.0
mkdir -vp Infra; 	# for Interfaces and app specific classes
touch Infra/README.md;
touch NLog.config;
touch NLog.Development.config;
touch README.md;
cd ..;



##
#	ORM Library for DB interface - Alternate approach
##
mkdir -vp gpm.Efc;cd gpm.Efc;
dotnet new classlib --language "C#" --framework netcoreapp2.2;
dotnet add package NLog;	# --version 4.6.6
dotnet add package MySql.Data.EntityFrameworkCore;	# --version 8.0.17
dotnet add package Microsoft.EntityFrameworkCore.Design;	# --version 2.2.6
dotnet add package MySql.Data;	# --version 8.0.17
dotnet add package Microsoft.AspNetCore.Mvc;	# --version 2.2.0	# for providing ModelMetadataType and HTTP Status Codes.
touch README.md;
mkdir -vp DbFuncs;
mkdir -vp EfCustomization/EntityExtensions;
mkdir -vp EntityMetaData;
# -- # Done till here
touch DbFuncs/README.md;
touch EfCustomization/README.md;
touch EfCustomization/EntityExtensions/README.md;
touch EntityMetaData/README.md;
cd ..;


##
#	Common Declarations and Functions used across components
##
mkdir -vp gpm.Common;cd gpm.Common;
dotnet new classlib --language "C#" --framework netcoreapp2.2;
dotnet add package NLog;	# --version 4.6.6
dotnet add package Newtonsoft.Json;	# --version 12.0.2	# Include only if needed
touch README.md;
mkdir -vp Constants; 	# for constants and enum definitions
touch Constants/README.md;
mkdir -vp Interfaces; 	# for Interfaces used across the solution
touch Interfaces/README.md;
mkdir -vp Models; 		# for Non-Persistent POCO Models, used in data interchanges
touch Models/README.md;
mkdir -vp Utils; 		# for Static Utility functions
touch Utils/README.md;
cd ..;


##
#	Business logic implementation
##
mkdir -vp gpm.Biz;cd gpm.Biz;
dotnet new classlib --language "C#" --framework netcoreapp2.2;
dotnet add package NLog;	# --version 4.6.6
# dotnet add package Newtonsoft.Json;	# --version 12.0.2	# Include only if needed
touch README.md;
mkdir -vp Constants; 	# for constants and enum definitions
touch Constants/README.md;
mkdir -vp Interfaces; 	# for Interfaces used across the solution
touch Interfaces/README.md;
mkdir -vp Printing; 		# Templating, Formatting and Generators
touch Printing/README.md;
mkdir -vp Sequencing; 		# Manage Sequencing of pass numbers
touch Sequencing/README.md;
cd ..;



##
# Add project references
##
# dotnet add [<PROJECT>] reference [-f|--framework] <PROJECT_REFERENCES> [-h|--help]
dotnet add gpm_web/gpm_web.csproj reference gpm.Common/gpm.Common.csproj;
dotnet add gpm_web/gpm_web.csproj reference gpm.Biz/gpm.Biz.csproj;
dotnet add gpm_web/gpm_web.csproj reference gpm.Efc/gpm.Efc.csproj;

dotnet add gpm.Biz/gpm.Biz.csproj reference gpm.Common/gpm.Common.csproj;
dotnet add gpm.Biz/gpm.Biz.csproj reference gpm.Efc/gpm.Efc.csproj;

dotnet add gpm.Efc/gpm.Efc.csproj reference gpm.Common/gpm.Common.csproj;

# All all the projects to solution
dotnet sln gpm_solution.sln add **/*.csproj;


popd ..;


##
#	Scaffold entity framework from existing database
# Pre-Requisites
# 	dotnet add package Microsoft.EntityFrameworkCore.Design
# 	dotnet add package Pomelo.EntityFrameworkCore.MySql
#   DB Scripts run successfull
#       1. $/30-Design/310-DB/3110-SQL/db-10-init.sql
#       2. $/30-Design/310-DB/3110-SQL/db-20a-create-tables.ddl
##
pushd 40-Develop/gpm.Efc;
dotnet ef dbcontext scaffold \
	"server=127.0.0.1;port=3306;user=DB_USER_ID;password=DB_USER_PASSWORD;database=DATABASE_SCHEMA_NAME" \
	MySql.Data.EntityFrameworkCore \
	-v -f \
	-d \
	-o dbsets \
	-c APP_DBContext \
	--framework netcoreapp2.2
	# --use-database-names \
popd;
