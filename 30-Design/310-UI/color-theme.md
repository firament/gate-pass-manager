# Color Theme
> Primary Color: f5b67a

## Links
- https://paletton.com/#uid=70y0u0kg4uK6oPFbhEikrq6onlN
	- Base Theme
- https://paletton.com/#uid=70y0u0kbjw03cSq6UHQghr3lFlE
	- Lighter

## Color Structure
> WIP

| S# | Name             | Hex Code |
| -- | ---------------- | -------- |
| 0  | cstm-pri         | #ffd1a5  |
| 1  | cstm-pri-dark    | #ac7138  |
| 2  | cstm-pri-light   | #fff2e6  |
| 3  | cstm-sec         | #ffdca5  |
| 4  | cstm-sec-dark    | #ac7f38  |
| 5  | cstm-sec-light   | #fff5e6  |
| 6  | cstm-alt-1       | #a2b4ca  |
| 7  | cstm-alt-1-dark  | #4b698d  |
| 8  | cstm-alt-1-light | #d2dce7  |
| 9  | cstm-alt-2       | #9bbec4  |
| a  | cstm-alt-2-dark  | #437a85  |
| b  | cstm-alt-2-light | #cfe1e5  |
| c  | cstm-dark        | #ac7138  |


## CSS Content
- To be retrofitted, from files 30-Design/310-UI/samples/*.html

- 92-swatch-70y0h0kbjw03cSq6UHQghr3lFlE (Dist 17, Hue 34)
	```css
.color-primary-0 { color: #FFD1A5 }	/* Main Primary color */
.color-primary-1 { color: #FFF2E6 }
.color-primary-2 { color: #FFE3C8 }
.color-primary-3 { color: #D89F6A }
.color-primary-4 { color: #AC7138 }

.color-secondary-1-0 { color: #FFDCA5 }	/* Main Secondary color (1) */
.color-secondary-1-1 { color: #FFF5E6 }
.color-secondary-1-2 { color: #FFEAC8 }
.color-secondary-1-3 { color: #D8AD6A }
.color-secondary-1-4 { color: #AC7F38 }

.color-secondary-2-0 { color: #7189A7 }	/* Main Secondary color (2) */
.color-secondary-2-1 { color: #D2DCE7 }
.color-secondary-2-2 { color: #A2B4CA }
.color-secondary-2-3 { color: #4B698D }
.color-secondary-2-4 { color: #2B4B71 }

.color-complement-0 { color: #67949D }	/* Main Complement color */
.color-complement-1 { color: #CFE1E5 }
.color-complement-2 { color: #9BBEC4 }
.color-complement-3 { color: #437A85 }
.color-complement-4 { color: #245F6A }
	```

- 90-swatch-70y0u0kg4uK6oPFbhEikrq6onlN
	```css
	.color-primary-0 { color: #F5B67A }	/* Main Primary color */
	.color-primary-1 { color: #FFE5CC }
	.color-primary-2 { color: #FFD1A5 }
	.color-primary-3 { color: #D08C4B }
	.color-primary-4 { color: #AE6A29 }

	.color-secondary-1-0 { color: #F5CF7A }	/* Main Secondary color (1) */
	.color-secondary-1-1 { color: #FFEFCC }
	.color-secondary-1-2 { color: #FFE3A5 }
	.color-secondary-1-3 { color: #D0A74B }
	.color-secondary-1-4 { color: #AE8529 }

	.color-secondary-2-0 { color: #5C6DA6 }	/* Main Secondary color (2) */
	.color-secondary-2-1 { color: #BAC3E2 }
	.color-secondary-2-2 { color: #8796C4 }
	.color-secondary-2-3 { color: #3D508D }
	.color-secondary-2-4 { color: #263975 }

	.color-complement-0 { color: #4D8B97 }	/* Main Complement color */
	.color-complement-1 { color: #B1D6DC }
	.color-complement-2 { color: #7AAFB9 }
	.color-complement-3 { color: #307480 }
	.color-complement-4 { color: #1B5E6B }
	```
- 91-swatch-70y0u0kbjw03cSq6UHQghr3lFlE
	```css
	.color-primary-0 { color: #FFD1A5 }	/* Main Primary color */
	.color-primary-1 { color: #FFF2E6 }
	.color-primary-2 { color: #FFE3C8 }
	.color-primary-3 { color: #D89F6A }
	.color-primary-4 { color: #AC7138 }

	.color-secondary-1-0 { color: #FFE3A5 }	/* Main Secondary color (1) */
	.color-secondary-1-1 { color: #FFF7E6 }
	.color-secondary-1-2 { color: #FFEEC8 }
	.color-secondary-1-3 { color: #D8B66A }
	.color-secondary-1-4 { color: #AC8838 }

	.color-secondary-2-0 { color: #7783AD }	/* Main Secondary color (2) */
	.color-secondary-2-1 { color: #D4D9E9 }
	.color-secondary-2-2 { color: #A6B0CE }
	.color-secondary-2-3 { color: #506092 }
	.color-secondary-2-4 { color: #2F3F75 }

	.color-complement-0 { color: #67949D }	/* Main Complement color */
	.color-complement-1 { color: #CFE1E5 }
	.color-complement-2 { color: #9BBEC4 }
	.color-complement-3 { color: #437A85 }
	.color-complement-4 { color: #245F6A }
	```
