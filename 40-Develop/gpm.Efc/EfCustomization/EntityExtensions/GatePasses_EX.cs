﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
	public partial class GatePasses
	{

		/// <summary>
		/// 
		/// </summary>
		/// <value></value>
		[NotMapped]
		public string DepartmentName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <value></value>
		[NotMapped]
		public string VisitTypeName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <value></value>
		[NotMapped]
		public string StatusName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <value></value>
		[NotMapped]
		public int VehiclePassNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <value></value>
		[NotMapped]
		public bool IsPassOfInterest { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <value></value>
		[NotMapped]
		public int SearchType { get; set; }

		public void ScrubData(){
			// Ensure casing in store same as in UI.
			if (!string.IsNullOrWhiteSpace(HospitalNum))
			{
			HospitalNum = HospitalNum.Trim().ToUpper();
			}
				
			if (!string.IsNullOrWhiteSpace(WheelchairNum))
			{
			WheelchairNum = WheelchairNum.Trim().ToUpper();
			}

			if (VehicleType != 0)
			{
				HasVehicle = 1;
			}
			if (!string.IsNullOrWhiteSpace(VehiclePlateNum))
			{
			VehiclePlateNum = VehiclePlateNum.Trim().ToUpper();
			}

			// CR13.4 - Remove spaces from mobile number, from speech-to-text formatting.
			if (!string.IsNullOrWhiteSpace(MobileNum))
			{
			MobileNum = MobileNum.Replace(" ", "");
			}

		}

	}
}
