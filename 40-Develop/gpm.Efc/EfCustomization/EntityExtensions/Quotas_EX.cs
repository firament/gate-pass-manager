using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
	public partial class Quotas
	{
		[NotMapped]
		public string DeprtmentName { get; set; }
		[NotMapped]
		public int UsedNewVisit { get; set; }
		[NotMapped]
		public int UsedRevisit { get; set; }
	}
}
