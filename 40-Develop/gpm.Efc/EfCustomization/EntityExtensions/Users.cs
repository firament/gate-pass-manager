﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    public partial class Users
    {
		[NotMapped]
		[Display(Name = "Confirm Password")]
		public string UserPwdConfirm { get; set; }


    }
}
