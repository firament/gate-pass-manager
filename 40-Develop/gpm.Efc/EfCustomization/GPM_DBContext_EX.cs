using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace gpm.Efc.dbsets
{
	public partial class GPM_DBContext : DbContext
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
		internal string DBConnectionString { get; set; }

		/// <summary>
		/// Override to create custom connetions outside of DI.
		/// Generally for Read-Only connections for reports.
		/// </summary>
		/// <param name="ConnectionString"></param>
		public GPM_DBContext(string psConnectionString)
		{
			DBConnectionString = psConnectionString;
		}

	}
}


