﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace gpm.Efc.dbsets
{
    public partial class GPM_DBContext : DbContext
    {
        public GPM_DBContext()
        {
        }

        public GPM_DBContext(DbContextOptions<GPM_DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ADbInfo> ADbInfo { get; set; }
        public virtual DbSet<Attachments> Attachments { get; set; }
        public virtual DbSet<DdCategories> DdCategories { get; set; }
        public virtual DbSet<DdItems> DdItems { get; set; }
        public virtual DbSet<GatePassTemplates> GatePassTemplates { get; set; }
        public virtual DbSet<GatePasses> GatePasses { get; set; }
        public virtual DbSet<GenAppSettings> GenAppSettings { get; set; }
        public virtual DbSet<MaintGatepassLog> MaintGatepassLog { get; set; }
        public virtual DbSet<MaintLoginHistory> MaintLoginHistory { get; set; }
        public virtual DbSet<MaintPassSequenceCache> MaintPassSequenceCache { get; set; }
        public virtual DbSet<MaintSessions> MaintSessions { get; set; }
        public virtual DbSet<Quotas> Quotas { get; set; }
        public virtual DbSet<ResourceAssets> ResourceAssets { get; set; }
        public virtual DbSet<ResourcePassMap> ResourcePassMap { get; set; }
        public virtual DbSet<SystemLogGpm> SystemLogGpm { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
				optionsBuilder.UseMySQL(DBConnectionString);
				optionsBuilder.EnableSensitiveDataLogging(true);
				optionsBuilder.EnableDetailedErrors(true);
			}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<ADbInfo>(entity =>
            {
                entity.HasIndex(e => new { e.AppCode, e.Version })
                    .HasName("UNIQ_VERSIONS")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AppCode).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Version).IsUnicode(false);
            });

            modelBuilder.Entity<Attachments>(entity =>
            {
                entity.HasIndex(e => e.GatePassId)
                    .HasName("FKATTACHMENT379539");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AttachmentType).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.MimeType).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.GatePass)
                    .WithMany(p => p.Attachments)
                    .HasForeignKey(d => d.GatePassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKATTACHMENT379539");
            });

            modelBuilder.Entity<DdCategories>(entity =>
            {
                entity.HasIndex(e => e.DdiCatgName)
                    .HasName("DDI_CATG_NAME")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DdiCatgDesc).IsUnicode(false);

                entity.Property(e => e.DdiCatgName).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<DdItems>(entity =>
            {
                entity.HasKey(e => new { e.DdiCatgId, e.DdiCode });

                entity.HasIndex(e => new { e.DdiCatgId, e.Status })
                    .HasName("DD_ITEMS");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DdiCodeTxt).IsUnicode(false);

                entity.Property(e => e.DdiDesc).IsUnicode(false);

                entity.Property(e => e.DdiDispSeq).HasDefaultValueSql("10000");

                entity.Property(e => e.DdiDispText).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.DdiCatg)
                    .WithMany(p => p.DdItems)
                    .HasForeignKey(d => d.DdiCatgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKDD_ITEMS184240");
            });

            modelBuilder.Entity<GatePassTemplates>(entity =>
            {
                entity.HasIndex(e => new { e.PassType, e.Status })
                    .HasName("GATE_PASS_TEMPLATES");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.PassTemplate).IsUnicode(false);

                entity.Property(e => e.PassType).HasDefaultValueSql("0");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<GatePasses>(entity =>
            {
                entity.HasIndex(e => e.PassNum)
                    .HasName("PASS_NUM");

                entity.HasIndex(e => e.RootGatePass)
                    .HasName("FKGATE_PASSE975569");

                entity.HasIndex(e => new { e.IssueDate, e.PassType, e.PassNum })
                    .HasName("GATE_PASSES");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Department).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.HasResources).HasDefaultValueSql("0");

                entity.Property(e => e.HasVehicle).HasDefaultValueSql("0");

                entity.Property(e => e.HospitalNum).IsUnicode(false);

                entity.Property(e => e.MobileNum).IsUnicode(false);

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.PartyCount).HasDefaultValueSql("0");

                entity.Property(e => e.PassNum).HasDefaultValueSql("0");

                entity.Property(e => e.PassType).HasDefaultValueSql("0");

                entity.Property(e => e.PersonMeeting).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.StepOut).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.SupplyDelivery).HasDefaultValueSql("0");

                entity.Property(e => e.TimeIn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.TimeOut).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.VehiclePlateNum).IsUnicode(false);

                entity.Property(e => e.VehicleType).HasDefaultValueSql("0");

                entity.Property(e => e.VisitType).HasDefaultValueSql("0");

                entity.Property(e => e.WheelchairNum).IsUnicode(false);

                entity.HasOne(d => d.RootGatePassNavigation)
                    .WithMany(p => p.InverseRootGatePassNavigation)
                    .HasForeignKey(d => d.RootGatePass)
                    .HasConstraintName("FKGATE_PASSE975569");
            });

            modelBuilder.Entity<GenAppSettings>(entity =>
            {
                entity.HasIndex(e => new { e.AppCode, e.AppConfigVer })
                    .HasName("GEN_APP_SETTINGS");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AppCode).IsUnicode(false);

                entity.Property(e => e.AppConfig).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<MaintGatepassLog>(entity =>
            {
                entity.HasIndex(e => e.GatePassId)
                    .HasName("MAINT_GATEPASS_LOG");

                entity.Property(e => e.ActionType).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.GatePass)
                    .WithMany(p => p.MaintGatepassLog)
                    .HasForeignKey(d => d.GatePassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKMAINT_GATE290571");
            });

            modelBuilder.Entity<MaintLoginHistory>(entity =>
            {
                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.UserLogin).IsUnicode(false);
            });

            modelBuilder.Entity<MaintPassSequenceCache>(entity =>
            {
                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.RawData).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<MaintSessions>(entity =>
            {
                entity.HasIndex(e => e.Token)
                    .HasName("MAINT_SESSIONS");

                entity.Property(e => e.AuthTime).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DeviceSig).IsUnicode(false);

                entity.Property(e => e.IssueTime).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LastUsed).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Role).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Token).IsUnicode(false);
            });

            modelBuilder.Entity<Quotas>(entity =>
            {
                entity.HasKey(e => new { e.Department, e.DateInt });

                entity.Property(e => e.Department).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CapNewVisit).HasDefaultValueSql("0");

                entity.Property(e => e.CapRevisit).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<ResourceAssets>(entity =>
            {
                entity.HasIndex(e => e.ResourceCode)
                    .HasName("RESOURCE_CODE")
                    .IsUnique();

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ResourceCode).IsUnicode(false);

                entity.Property(e => e.ResourceName).IsUnicode(false);

                entity.Property(e => e.ResourceNote).IsUnicode(false);

                entity.Property(e => e.ResourceType).HasDefaultValueSql("0");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<ResourcePassMap>(entity =>
            {
                entity.HasKey(e => new { e.ResourceId, e.GatePassId });

                entity.HasIndex(e => e.GatePassId)
                    .HasName("FKRESOURCE_P318182");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.GatePass)
                    .WithMany(p => p.ResourcePassMap)
                    .HasForeignKey(d => d.GatePassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKRESOURCE_P318182");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.ResourcePassMap)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKRESOURCE_P699772");
            });

            modelBuilder.Entity<SystemLogGpm>(entity =>
            {
                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AllEventProps).IsUnicode(false);

                entity.Property(e => e.AppCode)
                    .IsUnicode(false)
                    .HasDefaultValueSql("GPM");

                entity.Property(e => e.AppInstanceId)
                    .IsUnicode(false)
                    .HasDefaultValueSql("01");

                entity.Property(e => e.AssemblyVersion).IsUnicode(false);

                entity.Property(e => e.CallSite).IsUnicode(false);

                entity.Property(e => e.ClientIp).IsUnicode(false);

                entity.Property(e => e.Exception).IsUnicode(false);

                entity.Property(e => e.LogLevel).IsUnicode(false);

                entity.Property(e => e.LogTime).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Logger).IsUnicode(false);

                entity.Property(e => e.Machine).IsUnicode(false);

                entity.Property(e => e.Message).IsUnicode(false);

                entity.Property(e => e.RequestReferrer).IsUnicode(false);

                entity.Property(e => e.Stacktrace).IsUnicode(false);

                entity.Property(e => e.Url).IsUnicode(false);

                entity.Property(e => e.UserAgent).IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasIndex(e => e.UserLogin)
                    .HasName("USERS");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DisplayName).IsUnicode(false);

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FullName).IsUnicode(false);

                entity.Property(e => e.LastLogin).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Role).HasDefaultValueSql("0");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.UserLogin).IsUnicode(false);

                entity.Property(e => e.UserPwd).IsUnicode(false);
            });
        }
    }
}
