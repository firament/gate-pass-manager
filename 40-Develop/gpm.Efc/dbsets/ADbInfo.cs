﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("A_DB_INFO")]
    public partial class ADbInfo
    {
        [Column("ID", TypeName = "int(10) unsigned")]
        public int Id { get; set; }
        [Required]
        [Column("APP_CODE")]
        [StringLength(8)]
        public string AppCode { get; set; }
        [Required]
        [Column("VERSION")]
        [StringLength(12)]
        public string Version { get; set; }
        [Required]
        [Column("NOTE")]
        [StringLength(255)]
        public string Note { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
