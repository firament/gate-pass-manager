﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("QUOTAS")]
    public partial class Quotas
    {
        [Column("DEPARTMENT", TypeName = "int(11)")]
        public int Department { get; set; }
        [Column("DATE_INT", TypeName = "bigint(20)")]
        public long DateInt { get; set; }
        [Column("DATE", TypeName = "date")]
        public DateTime Date { get; set; }
        [Column("CAP_NEW_VISIT", TypeName = "int(11)")]
        public int CapNewVisit { get; set; }
        [Column("CAP_REVISIT", TypeName = "int(11)")]
        public int CapRevisit { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
