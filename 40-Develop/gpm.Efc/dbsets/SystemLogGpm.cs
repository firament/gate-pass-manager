﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("SYSTEM_LOG_GPM")]
    public partial class SystemLogGpm
    {
        [Column("ID", TypeName = "bigint(20) unsigned")]
        public long Id { get; set; }
        [Required]
        [Column("APP_CODE")]
        [StringLength(8)]
        public string AppCode { get; set; }
        [Required]
        [Column("APP_INSTANCE_ID")]
        [StringLength(8)]
        public string AppInstanceId { get; set; }
        [Column("LOG_TIME")]
        public DateTimeOffset LogTime { get; set; }
        [Required]
        [Column("LOG_LEVEL")]
        [StringLength(8)]
        public string LogLevel { get; set; }
        [Required]
        [Column("LOGGER")]
        [StringLength(60)]
        public string Logger { get; set; }
        [Required]
        [Column("MESSAGE")]
        public string Message { get; set; }
        [Column("EXCEPTION")]
        public string Exception { get; set; }
        [Column("STACKTRACE")]
        public string Stacktrace { get; set; }
        [Column("URL")]
        [StringLength(1020)]
        public string Url { get; set; }
        [Column("USER-AGENT")]
        [StringLength(60)]
        public string UserAgent { get; set; }
        [Column("REQUEST_REFERRER")]
        [StringLength(60)]
        public string RequestReferrer { get; set; }
        [Column("ASSEMBLY_VERSION")]
        [StringLength(24)]
        public string AssemblyVersion { get; set; }
        [Column("MACHINE")]
        [StringLength(60)]
        public string Machine { get; set; }
        [Column("CLIENT_IP")]
        [StringLength(24)]
        public string ClientIp { get; set; }
        [Column("CALL_SITE")]
        [StringLength(1020)]
        public string CallSite { get; set; }
        [Column("ALL_EVENT_PROPS")]
        public string AllEventProps { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
    }
}
