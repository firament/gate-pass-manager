﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("RESOURCE_PASS_MAP")]
    public partial class ResourcePassMap
    {
        [Column("RESOURCE_ID", TypeName = "int(10) unsigned")]
        public int ResourceId { get; set; }
        [Column("GATE_PASS_ID", TypeName = "int(10) unsigned")]
        public int GatePassId { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("GatePassId")]
        [InverseProperty("ResourcePassMap")]
        public virtual GatePasses GatePass { get; set; }
        [ForeignKey("ResourceId")]
        [InverseProperty("ResourcePassMap")]
        public virtual ResourceAssets Resource { get; set; }
    }
}
