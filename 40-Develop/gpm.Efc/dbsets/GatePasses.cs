﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("GATE_PASSES")]
    public partial class GatePasses
    {
        public GatePasses()
        {
            Attachments = new HashSet<Attachments>();
            InverseRootGatePassNavigation = new HashSet<GatePasses>();
            MaintGatepassLog = new HashSet<MaintGatepassLog>();
            ResourcePassMap = new HashSet<ResourcePassMap>();
        }

        [Key]
        [Column("GATE_PASS_ID", TypeName = "int(10) unsigned")]
        public int GatePassId { get; set; }
        [Column("PASS_TYPE", TypeName = "int(11)")]
        public int PassType { get; set; }
        [Column("PASS_NUM", TypeName = "int(11)")]
        public int PassNum { get; set; }
        [Column("ISSUE_DATE", TypeName = "date")]
        public DateTime IssueDate { get; set; }
        [Column("ROOT_GATE_PASS", TypeName = "int(10) unsigned")]
        public int? RootGatePass { get; set; }
        [Column("PARTY_COUNT", TypeName = "int(11)")]
        public int PartyCount { get; set; }
        [Column("DEPARTMENT", TypeName = "int(11)")]
        public int Department { get; set; }
        [Column("VISIT_TYPE", TypeName = "int(11)")]
        public int VisitType { get; set; }
        [Column("HOSPITAL_NUM")]
        [StringLength(24)]
        public string HospitalNum { get; set; }
        [Column("MOBILE_NUM")]
        [StringLength(16)]
        public string MobileNum { get; set; }
        [Column("SUPPLY_DELIVERY", TypeName = "int(11)")]
        public int? SupplyDelivery { get; set; }
        [Column("PERSON_MEETING")]
        [StringLength(60)]
        public string PersonMeeting { get; set; }
        [Column("HAS_RESOURCES", TypeName = "int(11)")]
        public int HasResources { get; set; }
        [Column("WHEELCHAIR_NUM")]
        [StringLength(20)]
        public string WheelchairNum { get; set; }
        [Column("HAS_VEHICLE", TypeName = "int(11)")]
        public int HasVehicle { get; set; }
        [Column("VEHICLE_TYPE", TypeName = "int(11)")]
        public int VehicleType { get; set; }
        [Column("VEHICLE_PLATE_NUM")]
        [StringLength(24)]
        public string VehiclePlateNum { get; set; }
        [Column("NOTE")]
        [StringLength(255)]
        public string Note { get; set; }
        [Column("TIME_IN")]
        public DateTimeOffset TimeIn { get; set; }
        [Column("TIME_OUT")]
        public DateTimeOffset? TimeOut { get; set; }
        [Column("STEP_OUT")]
        public DateTimeOffset? StepOut { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("RootGatePass")]
        [InverseProperty("InverseRootGatePassNavigation")]
        public virtual GatePasses RootGatePassNavigation { get; set; }
        [InverseProperty("GatePass")]
        public virtual ICollection<Attachments> Attachments { get; set; }
        [InverseProperty("RootGatePassNavigation")]
        public virtual ICollection<GatePasses> InverseRootGatePassNavigation { get; set; }
        [InverseProperty("GatePass")]
        public virtual ICollection<MaintGatepassLog> MaintGatepassLog { get; set; }
        [InverseProperty("GatePass")]
        public virtual ICollection<ResourcePassMap> ResourcePassMap { get; set; }
    }
}
