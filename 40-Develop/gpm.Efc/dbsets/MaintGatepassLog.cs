﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("MAINT_GATEPASS_LOG")]
    public partial class MaintGatepassLog
    {
        [Key]
        [Column("MGL_ID", TypeName = "bigint(20) unsigned")]
        public long MglId { get; set; }
        [Column("GATE_PASS_ID", TypeName = "int(10) unsigned")]
        public int GatePassId { get; set; }
        [Column("ACTION_TYPE", TypeName = "int(11)")]
        public int ActionType { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }

        [ForeignKey("GatePassId")]
        [InverseProperty("MaintGatepassLog")]
        public virtual GatePasses GatePass { get; set; }
    }
}
