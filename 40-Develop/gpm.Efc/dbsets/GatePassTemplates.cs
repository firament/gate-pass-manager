﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("GATE_PASS_TEMPLATES")]
    public partial class GatePassTemplates
    {
        [Key]
        [Column("TEMPLATE_ID", TypeName = "int(10) unsigned")]
        public int TemplateId { get; set; }
        [Column("PASS_TYPE", TypeName = "int(11)")]
        public int PassType { get; set; }
        [Column("NOTE")]
        [StringLength(255)]
        public string Note { get; set; }
        [Required]
        [Column("PASS_TEMPLATE")]
        public string PassTemplate { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
