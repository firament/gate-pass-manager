﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("MAINT_SESSIONS")]
    public partial class MaintSessions
    {
        [Key]
        [Column("MSID", TypeName = "bigint(20) unsigned")]
        public long Msid { get; set; }
        [Required]
        [Column("TOKEN")]
        [StringLength(255)]
        public string Token { get; set; }
        [Required]
        [Column("DEVICE_SIG")]
        public string DeviceSig { get; set; }
        [Column("USERID", TypeName = "int(10) unsigned")]
        public int Userid { get; set; }
        [Column("ROLE")]
        [StringLength(24)]
        public string Role { get; set; }
        [Column("ISSUE_TIME")]
        public DateTimeOffset IssueTime { get; set; }
        [Column("AUTH_TIME")]
        public DateTimeOffset? AuthTime { get; set; }
        [Column("LAST_USED")]
        public DateTimeOffset LastUsed { get; set; }
        [Column("STATUS", TypeName = "int(10) unsigned")]
        public int Status { get; set; }
    }
}
