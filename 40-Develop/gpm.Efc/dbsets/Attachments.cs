﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("ATTACHMENTS")]
    public partial class Attachments
    {
        [Key]
        [Column("ATTACHMENT_ID", TypeName = "int(10) unsigned")]
        public int AttachmentId { get; set; }
        [Column("GATE_PASS_ID", TypeName = "int(10) unsigned")]
        public int GatePassId { get; set; }
        [Column("ATTACHMENT_TYPE", TypeName = "int(11)")]
        public int AttachmentType { get; set; }
        [Required]
        [Column("ATTACHMENT_DATA", TypeName = "varbinary(255)")]
        public byte[] AttachmentData { get; set; }
        [Column("NOTE")]
        [StringLength(255)]
        public string Note { get; set; }
        [Required]
        [Column("MIME_TYPE")]
        [StringLength(24)]
        public string MimeType { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("GatePassId")]
        [InverseProperty("Attachments")]
        public virtual GatePasses GatePass { get; set; }
    }
}
