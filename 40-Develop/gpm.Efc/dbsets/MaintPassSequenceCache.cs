﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("MAINT_PASS_SEQUENCE_CACHE")]
    public partial class MaintPassSequenceCache
    {
        [Key]
        [Column("CACHE_DATE_INT", TypeName = "bigint(20)")]
        public long CacheDateInt { get; set; }
        [Required]
        [Column("RAW_DATA")]
        public string RawData { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
