﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("RESOURCE_ASSETS")]
    public partial class ResourceAssets
    {
        public ResourceAssets()
        {
            ResourcePassMap = new HashSet<ResourcePassMap>();
        }

        [Key]
        [Column("RESOURCE_ID", TypeName = "int(10) unsigned")]
        public int ResourceId { get; set; }
        [Column("RESOURCE_TYPE", TypeName = "int(11)")]
        public int ResourceType { get; set; }
        [Required]
        [Column("RESOURCE_CODE")]
        [StringLength(20)]
        public string ResourceCode { get; set; }
        [Required]
        [Column("RESOURCE_NAME")]
        [StringLength(60)]
        public string ResourceName { get; set; }
        [Column("RESOURCE_NOTE")]
        [StringLength(1020)]
        public string ResourceNote { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [InverseProperty("Resource")]
        public virtual ICollection<ResourcePassMap> ResourcePassMap { get; set; }
    }
}
