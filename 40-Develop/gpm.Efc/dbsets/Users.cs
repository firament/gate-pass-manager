﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm.Efc.dbsets
{
    [Table("USERS")]
    public partial class Users
    {
        [Key]
        [Column("USER_ID", TypeName = "int(10) unsigned")]
        public int UserId { get; set; }
        [Required]
        [Column("FULL_NAME")]
        [StringLength(60)]
        public string FullName { get; set; }
        [Required]
        [Column("DISPLAY_NAME")]
        [StringLength(24)]
        public string DisplayName { get; set; }
        [Required]
        [Column("USER_LOGIN")]
        [StringLength(24)]
        public string UserLogin { get; set; }
        [Required]
        [Column("USER_PWD")]
        public string UserPwd { get; set; }
        [Column("ROLE", TypeName = "int(11)")]
        public int Role { get; set; }
        [Column("LAST_LOGIN")]
        public DateTimeOffset LastLogin { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
