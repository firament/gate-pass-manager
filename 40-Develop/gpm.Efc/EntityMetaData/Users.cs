﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace gpm.Efc.dbsets
{

	/// <summary>
	/// Additional namespace segment to avoid polluting Entity Namespace.
	/// Intellisense will show both classes if they are in same namespace.
	/// </summary>
	namespace Annotations
	{
		/// <summary>
		/// Data Annotation class.
		/// Copied As-Is from generated code, and add suffix '_Metadata' to class name
		/// Add required Annotations on properties in this class
		/// Remove Properties for which Metadata is not required
		/// </summary>
		public abstract class Users_Metadata
		{
			[Key]
			public int UserId { get; set; }

			[Display(Name = "Full Name of User")]
			[StringLength(60, MinimumLength = 8, ErrorMessage = "")]
			public string FullName { get; set; }

			[StringLength(24, MinimumLength = 8, ErrorMessage = "")]
			public string DisplayName { get; set; }

			[StringLength(24, MinimumLength = 8, ErrorMessage = "")]
			public string UserLogin { get; set; }

			[DataType(DataType.Password)]
			public string UserPwd { get; set; }

			[Display(Name = "User Role")]
			[Range(1, 99, ErrorMessage = "A valid Role is mandatory.")]
			public int Role { get; set; }
			
			[DataType(DataType.DateTime)]
			public DateTimeOffset LastLogin { get; set; }
			
			[Column("STATUS", TypeName = "int(11)")]
			[Range(1, 99, ErrorMessage = "A valid Role is mandatory.")]
			public int Status { get; set; }
			
			[Column("ADD_BY", TypeName = "int(10) unsigned")]
			[Range(1, 32343)]
			public int AddBy { get; set; }
			
			[DataType(DataType.DateTime)]
			public DateTimeOffset AddOn { get; set; }

			[Column("EDIT_BY", TypeName = "int(10) unsigned")]
			[Range(1, 32343)]
			public int EditBy { get; set; }
			
			[DataType(DataType.DateTime)]
			public DateTimeOffset EditOn { get; set; }
		}

	}

	/// <summary>
	/// Partial Class definition to hoist Metadata on EF6 generated class
	/// </summary>
	// [MetadataType(typeof(Annotations.Users_Metadata))]
	[ModelMetadataType(typeof(Annotations.Users_Metadata))]
	public partial class A_DB_INFO
	{ /* No need for any code in this class */ }

}
