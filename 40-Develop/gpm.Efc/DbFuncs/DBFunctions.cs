using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using EFC = gpm.Efc;
using CTX = gpm.Efc.dbsets;
using GC = gpm.Common;

using stCode = gpm.Common.Enums.ErrorCode;
// using MySql.Data.EntityFrameworkCore.Extensions;
// using MySQL.Data.EntityFrameworkCore.Extensions;

namespace gpm.Efc.DBFuncs
{
	public static partial class DBFunctions
	{

		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		/// <summary>
		/// Convenience method to get Read-Only Connections
		/// or connections to a seperate database instance
		/// Use with caution.
		/// TODO: 
		/// 	1. Test connection string before using, or ake parameters and build one.
		/// 	2. Should user close the connection, or just release it?
		/// 	   a using clause should be sufficient.
		/// </summary>
		/// <param name="ConnectionString">Connection string to use with the database.</param>
		/// <returns>An open connection, or null.</returns>
		public static CTX.GPM_DBContext GetConnection(string ConnectionString){
			CTX.GPM_DBContext New_DB = null;
			try
			{
				New_DB = new CTX.GPM_DBContext(ConnectionString);
			}
			catch (System.Exception Ex)
			{
				log.Error(Ex, "Unable to open connection with the given connection string");
				throw;
			}
			return New_DB;
		}

		public static GC.IAppSettingContainer GetAppSettings(string psConnString, string psApplicationCode, int piASVersion)
		{
			GC.IAppSettingContainer lASC = new GC.AppSettingContainer();

			try
			{
				string lsJSON = string.Empty;
				using (CTX.GPM_DBContext efc_DB = new CTX.GPM_DBContext(psConnString))
				{
					CTX.GenAppSettings vAS = efc_DB.GenAppSettings
								.AsNoTracking()
								.Where(p => 
									   p.AppCode == psApplicationCode 
									&& p.AppConfigVer == piASVersion
									&& p.Status == 1
									)
								.FirstOrDefault()
								;

					if (vAS == null)
					{
						log.Debug("GEN_APP_SETTINGS entry == null");
						// log.Info("Use sample to build settings: {0}", getSampleAppSettings());
						log.Error(new IndexOutOfRangeException(""), "Application Settings ver {0} Not Found, see EXCEPTION details", piASVersion);
						return lASC;
					}

					// extract json
					lsJSON = vAS.AppConfig;
					if (string.IsNullOrWhiteSpace(lsJSON))
					{
						log.Error(new ArgumentNullException("AS_DATA", "Cannot deserialize from empty string"), "Data string is empty");
						return lASC;
					}

					// convert to object
					lASC = GC.Utils.FromJSON(lsJSON, lASC.GetType()) as GC.IAppSettingContainer;
					if (lASC == null) log.Debug("lASC == null");
					if (lASC.AppVersion != vAS.AppConfigVer)
					{
						// This is NOT good... inspect
						log.Warn(
								new ArgumentOutOfRangeException(
									"AppVersion"
									, "GEN_APP_SETTINGS.AppConfigVer is not in sync with AppSettingContainer.AppVersion"
									)
							, "Proceeding for now, FIX THIS at the earliest"
							);
						lASC.AppVersion = vAS.AppConfigVer;  // needed to save version back

					}

				};
			}
			catch (System.Exception eX)
			{
				log.Error(eX, "Error getting ApplicatonSettings from database");
			}

			return lASC;

		}
	}
}
