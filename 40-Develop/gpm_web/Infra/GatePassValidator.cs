using System;
using System.Text;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Infra{
	public class GatePassValidator{

		internal readonly ILogger<GatePassValidator> log;
		internal readonly GC.IAppSettingContainer appconfig;
		internal enum ValidationStatus{
			ALL_OK = 0,
			FIXES_DONE = 1,
			WARNINGS = 2,
			ERRORS = 3,
			NOT_SET = 99,
		}
		public GatePassValidator(ILogger<GatePassValidator> logger, GC.IAppSettingContainer App_Config)
		{
			log = logger;
			appconfig = App_Config;
		}
		
		public string ValidateGatepass(CTX.GatePasses GatePass){
			StringBuilder lsbMesg = new StringBuilder();

			ValidationStatus valStatus = ValidatePatientGatePass(GatePass, ref lsbMesg);
			// Inspect the status

			switch ((SE.GATE_PASS_TYPE)GatePass.PassType)
			{
				case SE.GATE_PASS_TYPE.PATIENT_NEW:
				case SE.GATE_PASS_TYPE.PATIENT_REVISIT:
					log.LogDebug("Inspect submitted form");
					break;
				case SE.GATE_PASS_TYPE.VENDOR:
				case SE.GATE_PASS_TYPE.VISITOR_STAFF:
				case SE.GATE_PASS_TYPE.VISITOR_PATIENT:
				case SE.GATE_PASS_TYPE.BLOOD_DONOR:
				case SE.GATE_PASS_TYPE.VISITOR_OTHER:
				case SE.GATE_PASS_TYPE.EMERGENCY:
				case SE.GATE_PASS_TYPE.PARTY:
				case SE.GATE_PASS_TYPE.VEHICLE:
					log.LogInformation("NOT YET IMPLEMENTED.");
					break;
				default:
					break;
			}

			return lsbMesg.ToString();
		}

		private ValidationStatus ValidatePatientGatePass(CTX.GatePasses GatePass, ref StringBuilder ValbMesgs)
		{
			ValidationStatus leStatus = ValidationStatus.NOT_SET;

			switch ((SE.GATE_PASS_TYPE)GatePass.PassType)
			{
				case SE.GATE_PASS_TYPE.PATIENT_NEW:
					// mobiile number || hospital number
					if (string.IsNullOrWhiteSpace(GatePass.MobileNum))
					{
						// Log error
					}
					break;
				case SE.GATE_PASS_TYPE.PATIENT_REVISIT:
					if (string.IsNullOrWhiteSpace(GatePass.HospitalNum) || string.IsNullOrWhiteSpace(GatePass.MobileNum))
					{
						// Log error
					}
					break;
			}

			// Options
			//#		Attendants
			if (GatePass.PartyCount > 0)
			{
				for (int vPC = 0; vPC < GatePass.PartyCount; vPC++)
				{
					CTX.GatePasses loParty = new CTX.GatePasses()
					{

					};
				}
			}
			//#		Wheelchair
			//#		vehicle - plate num


			return leStatus;
		}



	}
}
