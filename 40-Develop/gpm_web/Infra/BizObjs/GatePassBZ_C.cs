using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using BO = gpm_web.BizObjs;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.BizObjs
{
	// Contains all Create helper functions
	public partial class GatePassBZ : BZBase
	{

		private bool CreatePatientPass(CTX.GatePasses GatePass)
		{
			bool allOK = false;
			bool lbCanIssue = false;
			int liTokenCount = 1;
			int liTokenNumber;
			DateTime ldtNow = DateTime.Now.ToLocalTime();
			DateTime ldtToday = DateTime.Today.ToLocalTime();

			// Get count of passes required
			liTokenCount += GatePass.PartyCount;
			if (GatePass.HasVehicle == (int)SE.YES_NO.YES)
			{
				liTokenCount += 1;
			}

			#region // Workaround till resource editor is in place
			// Create the wheel chair entry for usage
			if (!string.IsNullOrWhiteSpace(GatePass.WheelchairNum))
			{
				CTX.ResourceAssets loWC = dbcontext.ResourceAssets
											.FirstOrDefault(f
												=> f.ResourceType == (int)SE.RESOURCE_TYPE.WHELCHR
												&& f.ResourceCode == GatePass.WheelchairNum
												)
										?? new CTX.ResourceAssets()
										{
											ResourceType = (int)SE.RESOURCE_TYPE.WHELCHR,
											ResourceCode = GatePass.WheelchairNum,
											ResourceName = "Wheel chair # " + GatePass.WheelchairNum,
											Status = (int)SE.RECORD_STATUS.ACTIVE,
											AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
											AddOn = ldtNow,
											EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
											EditOn = ldtNow
										};
				if (loWC.ResourceId == 0)
				{
					dbcontext.ResourceAssets.Add(loWC);
					int viRes = dbcontext.SaveChanges();
					log.LogInformation("Add wheel chair with code {0}. Records updated: {1}", GatePass.WheelchairNum, viRes);
				}
			}
			#endregion

			lbCanIssue = Dept_Day_Caps.CanIssueToken(GatePass.Department, (SE.VISIT_TYPE)GatePass.VisitType, dbcontext);
			if (!lbCanIssue)
			{
				log.LogWarning("No capacity availaible for Dept = {0}, visit-type = {1}. Cancelling request.", GatePass.DepartmentName, GatePass.VisitTypeName);
				return allOK;
			}
			liTokenNumber = Token_Counter.GetTokenNumbers(liTokenCount);

			log.LogInformation("Creating Patient Gate Pass with {0} Tokens, starting with {1}", liTokenCount, liTokenNumber);

			// Fill out paperwork
			GatePass.PassNum = liTokenNumber++;
			GatePass.IssueDate = ldtToday;
			GatePass.TimeIn = ldtNow;
			GatePass.Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS;
			GatePass.AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.AddOn = ldtNow;
			GatePass.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.EditOn = ldtNow;

			CTX.GatePasses voPass;
			for (int vI = 0; vI < GatePass.PartyCount; vI++)
			{
				voPass = new CTX.GatePasses()
				{
					PassType = (int)SE.GATE_PASS_TYPE.PARTY,
					PassNum = liTokenNumber++,
					IssueDate = ldtToday,
					Department = GatePass.Department,
					TimeIn = ldtNow,
					Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
					AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					AddOn = ldtNow,
					EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					EditOn = ldtNow,
				};
				GatePass.InverseRootGatePassNavigation.Add(voPass);
			}

			if (GatePass.HasVehicle == (int)SE.YES_NO.YES)
			{
				GatePass.InverseRootGatePassNavigation.Add(new CTX.GatePasses()
				{
					PassType = (int)SE.GATE_PASS_TYPE.VEHICLE,
					PassNum = liTokenNumber++,
					IssueDate = ldtToday,
					Department = GatePass.Department,
					HasVehicle = GatePass.HasVehicle,
					VehicleType = GatePass.VehicleType,
					VehiclePlateNum = GatePass.VehiclePlateNum,
					TimeIn = ldtNow,
					Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
					AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					AddOn = ldtNow,
					EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					EditOn = ldtNow,
				});

			}

			dbcontext.GatePasses.Add(GatePass);
			int liRecords = 0;
			try
			{
				liRecords = dbcontext.SaveChanges();
				allOK = true;
				log.LogInformation("Saved Patient pass. {0} record(s) updated. ID = {1}, Children = {2}, Type = {3}"
									, liRecords
									, GatePass.GatePassId
									, GatePass.InverseRootGatePassNavigation.Count
									, DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_TYPE, GatePass.PassType, true)
									);
			}
			catch (System.Exception exSE)
			{
				log.LogCritical(exSE, "Failed updating database.");
			}
			if (!allOK)
			{
				// Cancel tokens
				lbCanIssue = Dept_Day_Caps.ReturnToken(GatePass.Department, (SE.VISIT_TYPE)GatePass.VisitType, dbcontext);
			}

			return allOK;
		}   // CreatePatientPass

		private bool CreateVisitorPass(CTX.GatePasses GatePass)
		{
			bool allOK = false;
			int liTokenCount = 1;
			int liTokenNumber;
			DateTime ldtNow = DateTime.Now.ToLocalTime();
			DateTime ldtToday = DateTime.Today.ToLocalTime();

			log.LogInformation("Creating Visitor pass of Type {0} with {1} Party.", GatePass.PassType, GatePass.PartyCount);

			// Get count of passes required
			liTokenCount += GatePass.PartyCount;
			if (GatePass.HasVehicle == (int)SE.YES_NO.YES)
			{
				liTokenCount += 1;
			}

			liTokenNumber = Token_Counter.GetTokenNumbers(liTokenCount);
			log.LogInformation("Creating Visitor Gate Pass with {0} Tokens, starting with {1}", liTokenCount, liTokenNumber);

			// Fill out paperwork
			GatePass.PassNum = liTokenNumber++;
			GatePass.IssueDate = ldtToday;
			GatePass.TimeIn = ldtNow;
			GatePass.Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS;
			GatePass.AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.AddOn = ldtNow;
			GatePass.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.EditOn = ldtNow;

			CTX.GatePasses voPass;
			for (int vI = 0; vI < GatePass.PartyCount; vI++)
			{
				voPass = new CTX.GatePasses()
				{
					PassType = (int)SE.GATE_PASS_TYPE.PARTY,
					PassNum = liTokenNumber++,
					IssueDate = ldtToday,
					Department = GatePass.Department,
					TimeIn = ldtNow,
					Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
					AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					AddOn = ldtNow,
					EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					EditOn = ldtNow,
				};
				GatePass.InverseRootGatePassNavigation.Add(voPass);
			}

			// Add vehicle pass if needed
			if (GatePass.HasVehicle == (int)SE.YES_NO.YES)
			{
				GatePass.InverseRootGatePassNavigation.Add(new CTX.GatePasses()
				{
					PassType = (int)SE.GATE_PASS_TYPE.VEHICLE,
					PassNum = liTokenNumber++,
					IssueDate = ldtToday,
					Department = GatePass.Department,
					HasVehicle = GatePass.HasVehicle,
					VehicleType = GatePass.VehicleType,
					VehiclePlateNum = GatePass.VehiclePlateNum,
					TimeIn = ldtNow,
					Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
					AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					AddOn = ldtNow,
					EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
					EditOn = ldtNow,
				});
			}

			// Save the gatepasses
			dbcontext.GatePasses.Add(GatePass);
			int liRecords = 0;
			try
			{
				liRecords = dbcontext.SaveChanges();
				allOK = true;
				log.LogInformation("Saved Visitor pass. {0} record(s) updated. ID = {1}, Children = {2}"
									, liRecords
									, GatePass.GatePassId
									, GatePass.InverseRootGatePassNavigation.Count
									);
			}
			catch (System.Exception exSE)
			{
				log.LogCritical(exSE, "Failed updating database.");
			}
			return allOK;
		}   // CreateVisitorPass

		private bool CreateEmergencyPass(CTX.GatePasses GatePass)
		{
			bool allOK = false;
			int liTokenCount = 1;
			int liTokenNumber;
			DateTime ldtNow = DateTime.Now.ToLocalTime();
			DateTime ldtToday = DateTime.Today.ToLocalTime();

			liTokenNumber = Token_Counter.GetTokenNumbers(liTokenCount);
			log.LogInformation("Creating Emergency Gate Pass with {0} Tokens, starting with {1}", liTokenCount, liTokenNumber);

			// Fill out paperwork
			GatePass.PassNum = liTokenNumber++;
			GatePass.IssueDate = ldtToday;
			GatePass.TimeIn = ldtNow;
			GatePass.Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS;
			GatePass.AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.AddOn = ldtNow;
			GatePass.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.EditOn = ldtNow;

			// Save the gatepasses
			dbcontext.GatePasses.Add(GatePass);
			int liRecords = 0;
			try
			{
				liRecords = dbcontext.SaveChanges();
				allOK = true;
				log.LogInformation("Saved Emergency pass. {0} record(s) updated. ID = {1}, Children = {2}"
									, liRecords
									, GatePass.GatePassId
									, GatePass.InverseRootGatePassNavigation.Count
									);
			}
			catch (System.Exception exSE)
			{
				log.LogCritical(exSE, "Failed updating database.");
			}
			return allOK;

		}   // CreateEmergencyPass

		private bool CreateVehiclePass(CTX.GatePasses GatePass)
		{
			bool allOK = false;
			int liTokenCount = 1;
			int liTokenNumber;
			DateTime ldtNow = DateTime.Now.ToLocalTime();
			DateTime ldtToday = DateTime.Today.ToLocalTime();

			liTokenNumber = Token_Counter.GetTokenNumbers(1);
			log.LogInformation("Creating Vehicle Gate Pass with {0} Tokens, starting with {1}", liTokenCount, liTokenNumber);

			// Fill out paperwork
			GatePass.PassNum = liTokenNumber++;
			GatePass.IssueDate = ldtToday;
			GatePass.TimeIn = ldtNow;
			GatePass.Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS;
			GatePass.AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.AddOn = ldtNow;
			GatePass.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			GatePass.EditOn = ldtNow;

			// Save the gatepasses
			dbcontext.GatePasses.Add(GatePass);
			int liRecords = 0;
			try
			{
				liRecords = dbcontext.SaveChanges();
				log.LogInformation("Saved Vehicle pass. {0} record(s) updated. ID = {1}, Root = {2}"
									, liRecords
									, GatePass.GatePassId
									, GatePass.RootGatePass
									);
				allOK = true;
			}
			catch (System.Exception exSE)
			{
				log.LogCritical(exSE, "Failed updating database.");
			}
			return allOK;
		}   // CreateVehiclePass
	}
}
