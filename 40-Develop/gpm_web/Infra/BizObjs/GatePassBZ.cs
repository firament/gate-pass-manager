using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using BO = gpm_web.BizObjs;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.BizObjs
{
	public partial class GatePassBZ : BZBase
	{

		public IA.TokenCounter Token_Counter { get; set; }
		public IA.DayCapacityCounters Dept_Day_Caps { get; set; }
		public SL.DD_Lookup DDHelp { get; set; }

		public GatePassBZ(
			  ILogger<GatePassBZ> logger
			, GC.IAppSettingContainer App_Config
			, gpm.Efc.dbsets.GPM_DBContext db_context
			, IA.TokenCounter token_counter
			, IA.DayCapacityCounters dept_cap_counter
			, SL.DD_Lookup dd_helper
			)
		: base(logger, App_Config, db_context)
		{
			Token_Counter = token_counter;
			Dept_Day_Caps = dept_cap_counter;
			DDHelp = dd_helper;
		}

		public CTX.GatePasses CreateGatepass(CTX.GatePasses pGatePass, ML.UserPayload CurrUser)
		{
			bool lbSaveResult = false;
			UserData = CurrUser;
			switch ((SE.GATE_PASS_TYPE)pGatePass.PassType)
			{
				case SE.GATE_PASS_TYPE.PATIENT_NEW:
				case SE.GATE_PASS_TYPE.PATIENT_REVISIT:
					pGatePass.ScrubData();
					lbSaveResult = CreatePatientPass(pGatePass);
					break;
				case SE.GATE_PASS_TYPE.VENDOR:
				case SE.GATE_PASS_TYPE.VISITOR_STAFF:
				case SE.GATE_PASS_TYPE.VISITOR_PATIENT:
				case SE.GATE_PASS_TYPE.BLOOD_DONOR:
				case SE.GATE_PASS_TYPE.VISITOR_OTHER:
					pGatePass.ScrubData();
					lbSaveResult = CreateVisitorPass(pGatePass);
					break;
				case SE.GATE_PASS_TYPE.EMERGENCY:
					pGatePass.ScrubData();
					lbSaveResult = CreateEmergencyPass(pGatePass);
					break;
				case SE.GATE_PASS_TYPE.PARTY:
				case SE.GATE_PASS_TYPE.VEHICLE:
					pGatePass.ScrubData();
					lbSaveResult = CreateVehiclePass(pGatePass);
					pGatePass = GetPassForEdit(pGatePass.RootGatePass);
					break;

				default:
					break;
			}

			if (!lbSaveResult)
			{
				log.LogError("Error Saving Gatepass, see prior messages for details");
			}

			// Fill up display details for printing
			//#		add display texts [Department Name, Pass Type]
			pGatePass.DepartmentName = DDHelp.DisplayText((int)SE.DDCategories.DEPARTMENTS, pGatePass.Department, true);
			pGatePass.VisitTypeName = DDHelp.DisplayText((int)SE.DDCategories.VISIT_TYPE, pGatePass.VisitType, true);
			pGatePass.StatusName = DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_STATUS, pGatePass.Status, true);
			pGatePass.IsPassOfInterest = true;  // pGatePass.GatePassId == id;

			pGatePass.InverseRootGatePassNavigation.ToList().ForEach(vGP =>
			{
				vGP.DepartmentName = DDHelp.DisplayText((int)SE.DDCategories.DEPARTMENTS, vGP.Department, true);
				vGP.VisitTypeName = DDHelp.DisplayText((int)SE.DDCategories.VISIT_TYPE, vGP.VisitType, true);
				vGP.StatusName = DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_STATUS, vGP.Status, true);
				vGP.IsPassOfInterest = false;   // vGP.GatePassId == id;
			});
			// Set Vehicle pass number
			pGatePass.VehiclePassNumber = pGatePass.InverseRootGatePassNavigation
											.Where(v => v.PassType == (int)SE.GATE_PASS_TYPE.VEHICLE)
											.FirstOrDefault()
											?.GatePassId
											?? 0
											;

			return pGatePass;
		}


		/// <summary>
		/// Prepare a patient gatepass for filling details.
		/// </summary>
		/// <param name="deptid"></param>
		/// <param name="vtype"></param>
		/// <returns></returns>
		public CTX.GatePasses NewPassPatient(int deptid = 0, int vtype = 0)
		{
			// Validate Dept and Type
			// TODO: Validate against enums
			if (
				   deptid < 1
				|| (vtype != (int)SE.VISIT_TYPE.FIRST_VISIT && vtype != (int)SE.VISIT_TYPE.RE_VISIT)
				)
			{
				return null;
			}

			// Use Dropdown lookup service to get name
			string lsDeptmentName = DDHelp.DisplayText((int)SE.DDCategories.DEPARTMENTS, deptid, true);
			SE.VISIT_TYPE leVisitType = (SE.VISIT_TYPE)vtype;
			CTX.GatePasses loGatePass = new CTX.GatePasses()
			{
				Department = deptid,
				DepartmentName = DDHelp.DisplayText((int)SE.DDCategories.DEPARTMENTS, deptid),
				VisitType = vtype,
				VisitTypeName = DDHelp.DisplayText((int)SE.DDCategories.VISIT_TYPE, vtype),
				PassType = (leVisitType == SE.VISIT_TYPE.FIRST_VISIT)
							? (int)SE.GATE_PASS_TYPE.PATIENT_NEW
							: (int)SE.GATE_PASS_TYPE.PATIENT_REVISIT
							,
				HasResources = (int)SE.YES_NO.NO,
				HasVehicle = (int)SE.YES_NO.NO,
				VehicleType = (int)SE.VEHICLE_TYPE.NO_PARKING_REQD,
				PartyCount = 1, // CR 13
			};
			return loGatePass;
		}

		public CTX.GatePasses NewPassVehicle(int id)
		{
			if (id < 1)
			{
				return null;
			}
			CTX.GatePasses loRootPass = dbcontext.GatePasses.Where(y => y.GatePassId == id).FirstOrDefault();
			if (loRootPass == null)
			{
				return null;
			}

			// Get rootpass to prefill detais
			CTX.GatePasses loVehPass = new CTX.GatePasses()
			{
				// BEGIN - sak - 2021 Jan 27 - Fix vehicle pass bug
				// RootGatePass = loRootPass.PassNum,
				RootGatePass = loRootPass.GatePassId,
				// FINIS - sak - 2021 Jan 27 - Fix vehicle pass bug
				PassType = (int)SE.GATE_PASS_TYPE.VEHICLE,
				Department = loRootPass.Department,
				DepartmentName = DDHelp.DisplayText((int)SE.DDCategories.DEPARTMENTS, loRootPass.Department, true),
				HasVehicle = (int)SE.YES_NO.YES,
				Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
				StatusName = DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_STATUS, loRootPass.Status, true),
				Note = DDHelp.FormattedPassNumber(loRootPass.PassType, loRootPass.PassNum) // TD001: NOT a good idea, move to ideal usage ASAP.
			};
			return loVehPass;

		}

		public async Task<ML.SearchResultsGatepass> FindPagedAsync(ML.SearchFormGatepass SearchFilter)
		{
			IQueryable<CTX.GatePasses> loOptions;
			IA.PaginatedList<CTX.GatePasses> loResults;

			List<CTX.GatePasses> lstResults = new List<CTX.GatePasses>();
			// TODO: make enum for hardcoded types
			switch (SearchFilter.SearchMode)
			{
				case SE.SearchType.TOKEN: // getByToken
					if (SearchFilter.IssueDate == DateTime.MinValue)
					{
						SearchFilter.IssueDate = DateTime.Today;
					}
					loOptions = dbcontext.GatePasses
									.AsNoTracking()
									.Where(y => y.IssueDate == SearchFilter.IssueDate && y.PassNum == SearchFilter.PassNum)
									.Include(rg => rg.RootGatePassNavigation)
									.OrderBy(y => y.GatePassId)
									;
					loResults = await IA.PaginatedList<CTX.GatePasses>.CreateAsync(loOptions, 1, 25);
					break;
				case SE.SearchType.ALL_ACTIVE: // getAll
					loOptions = dbcontext.GatePasses
									.AsNoTracking()
									.Where(y => y.Status != (int)SE.GATE_PASS_STATUS.EXIT)
									.Include(rg => rg.RootGatePassNavigation)
									.OrderBy(y => y.GatePassId)
									;

					break;
				case SE.SearchType.CUSTOM: // getByFilter
					loOptions = SearchOptionBuilder(SearchFilter)
									.OrderBy(y => y.GatePassId)
									;
					break;
				default:
					SearchFilter.IssueDate = DateTime.Today;
					loOptions = null;
					break;
			}
			loResults = await IA.PaginatedList<CTX.GatePasses>.CreateAsync(loOptions, 1, 25);
			return new ML.SearchResultsGatepass
			{
				Options = SearchFilter,
				Items = loResults,
			};
		}

		public List<CTX.GatePasses> FindPasses(CTX.GatePasses GatePass)
		{
			List<CTX.GatePasses> lstResults = new List<CTX.GatePasses>();
			// TODO: make enum for hardcoded types
			switch (GatePass.SearchType)
			{
				case 1: // getByToken
					if (GatePass.IssueDate == DateTime.MinValue)
					{
						GatePass.IssueDate = DateTime.Today;
					}
					lstResults = dbcontext.GatePasses
									.AsNoTracking()
									.Where(y => y.IssueDate == GatePass.IssueDate && y.PassNum == GatePass.PassNum)
									.Include(rg => rg.RootGatePassNavigation)
									.OrderBy(y => y.GatePassId)
									.ToList()
									;
					break;
				case 2: // getAll
					lstResults = dbcontext.GatePasses
									.AsNoTracking()
									.Where(y => y.Status != (int)SE.GATE_PASS_STATUS.EXIT)
									.Include(rg => rg.RootGatePassNavigation)
									.OrderBy(y => y.GatePassId)
									.ToList()
									;
					break;
				case 3: // getByFilter
					lstResults = SearchOptionBuilder(GatePass)
									.OrderBy(y => y.GatePassId)
									.ToList()
									;
					break;
				default:
					GatePass.IssueDate = DateTime.Today;
					break;
			}
			return lstResults;
		}

		private IQueryable<CTX.GatePasses> SearchOptionBuilder(ML.SearchFormGatepass SearchFilter)
		{
			IQueryable<CTX.GatePasses> SearchOptions = dbcontext.GatePasses.AsNoTracking();

			// Issue Date
			if (SearchFilter.IssueDate != DateTime.MinValue)
			{
				SearchOptions = dbcontext.GatePasses.Where(y => y.IssueDate == SearchFilter.IssueDate);
			}

			// Department
			if (SearchFilter.Department != int.MaxValue)
			{
				SearchOptions = SearchOptions.Where(y => y.Department == SearchFilter.Department);
			}

			// Status
			if (SearchFilter.Status != int.MaxValue)
			{
				SearchOptions = SearchOptions.Where(y => y.Status == SearchFilter.Status);
			}

			// Pass Type
			if (SearchFilter.PassType != int.MaxValue)
			{
				SearchOptions = SearchOptions.Where(y => y.PassType == SearchFilter.PassType);
			}

			// Attendants
			switch (SearchFilter.PartyCount)
			{
				case 1:
					SearchOptions = SearchOptions.Where(y => y.PartyCount > 0);
					break;
				case 2:
					SearchOptions = SearchOptions.Where(y => y.PartyCount == 0);
					break;
				case int.MaxValue:
				default:
					break;
			}

			// Vehicle
			switch (SearchFilter.VehicleType)
			{
				case int.MaxValue - 1000:
					SearchOptions = SearchOptions.Where(y => y.VehicleType > 0);
					break;
				case int.MaxValue:
					break;
				default:
					SearchOptions = SearchOptions.Where(y => y.VehicleType == SearchFilter.VehicleType);
					break;
			}

			// Wheelchair
			switch (SearchFilter.HasResources)
			{
				case (int)SE.YES_NO.YES:
				case (int)SE.YES_NO.NO:
					SearchOptions = SearchOptions.Where(y => y.HasResources == SearchFilter.HasResources);
					break;
				case int.MaxValue:
				default:
					break;
			}

			return SearchOptions;
		}

		private IQueryable<CTX.GatePasses> SearchOptionBuilder(CTX.GatePasses gatepass)
		{
			// List<CTX.GatePasses> lstPasses = new List<CTX.GatePasses>();
			IQueryable<CTX.GatePasses> SearchOptions = dbcontext.GatePasses.AsNoTracking();

			// Issue Date
			if (gatepass.IssueDate != DateTime.MinValue)
			{
				SearchOptions = dbcontext.GatePasses.Where(y => y.IssueDate == gatepass.IssueDate);
			}

			// Department
			if (gatepass.Department != int.MaxValue)
			{
				SearchOptions = SearchOptions.Where(y => y.Department == gatepass.Department);
			}

			// Status
			if (gatepass.Status != int.MaxValue)
			{
				SearchOptions = SearchOptions.Where(y => y.Status == gatepass.Status);
			}

			// Pass Type
			if (gatepass.PassType != int.MaxValue)
			{
				SearchOptions = SearchOptions.Where(y => y.PassType == gatepass.PassType);
			}

			// Attendants
			switch (gatepass.PartyCount)
			{
				case 1:
					SearchOptions = SearchOptions.Where(y => y.PartyCount > 0);
					break;
				case 2:
					SearchOptions = SearchOptions.Where(y => y.PartyCount == 0);
					break;
				case int.MaxValue:
				default:
					break;
			}

			// Vehicle
			switch (gatepass.VehicleType)
			{
				case int.MaxValue - 1000:
					SearchOptions = SearchOptions.Where(y => y.VehicleType > 0);
					break;
				case int.MaxValue:
					break;
				default:
					SearchOptions = SearchOptions.Where(y => y.VehicleType == gatepass.VehicleType);
					break;
			}

			// Wheelchair
			switch (gatepass.HasResources)
			{
				case (int)SE.YES_NO.YES:
				case (int)SE.YES_NO.NO:
					SearchOptions = SearchOptions.Where(y => y.HasResources == gatepass.HasResources);
					break;
				case int.MaxValue:
				default:
					break;
			}

			return SearchOptions;
		}

		/// <summary>
		/// To find and update one single gate pass
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public CTX.GatePasses FindPass(int id)
		{
			if (id < 1)
			{
				return null;
			}
			CTX.GatePasses loResult = null;
			loResult = dbcontext.GatePasses
						.AsNoTracking()
						.Where(y => y.PassNum == id)
						.Include(y => y.RootGatePassNavigation)
						.OrderByDescending(y => y.GatePassId)
						.FirstOrDefault()
						;
			return loResult;
		}



		public CTX.GatePasses GetPassForEdit(int? id)
		{
			CTX.GatePasses loGPTree = null;

			//#		get gate pass
			CTX.GatePasses loGPoI = dbcontext.GatePasses
										.Where(gpc => gpc.GatePassId == id)
										.FirstOrDefault()
										;
			if (loGPoI == null)
			{
				log.LogError("No records found for Gatepass with ID: {0}", id);
				log.LogCritical("ATTN: Is this a breakin attempt??");
				return null;
			}

			//#		if not root, get root with all children
			int liRootID = loGPoI.RootGatePass ?? id ?? 0;
			loGPTree = dbcontext.GatePasses
							.Where(gpc => gpc.GatePassId == liRootID)
							.Include(ip => ip.InverseRootGatePassNavigation)
							.FirstOrDefault()
							;
			if (loGPTree == null)
			{
				log.LogError("No records found for Gatepass with Root ID: {0}", liRootID);
				log.LogCritical("ATTN: Is this a breakin attempt??");
				return null;
			}

			//#		add display texts [Department Name, Pass Type]
			loGPTree.DepartmentName = DDHelp.DisplayText((int)SE.DDCategories.DEPARTMENTS, loGPTree.Department, true);
			loGPTree.VisitTypeName = DDHelp.DisplayText((int)SE.DDCategories.VISIT_TYPE, loGPTree.VisitType, true);
			loGPTree.StatusName = DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_STATUS, loGPTree.Status, true);
			loGPTree.IsPassOfInterest = loGPTree.GatePassId == id;

			loGPTree.InverseRootGatePassNavigation.ToList().ForEach(vGP =>
			{
				vGP.DepartmentName = DDHelp.DisplayText((int)SE.DDCategories.DEPARTMENTS, vGP.Department, true);
				vGP.VisitTypeName = DDHelp.DisplayText((int)SE.DDCategories.VISIT_TYPE, vGP.VisitType, true);
				vGP.StatusName = DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_STATUS, vGP.Status, true);
				vGP.IsPassOfInterest = vGP.GatePassId == id;
			});
			// Set Vehicle pass number
			loGPTree.VehiclePassNumber = loGPTree.InverseRootGatePassNavigation
											.Where(v => v.PassType == (int)SE.GATE_PASS_TYPE.VEHICLE)
											.FirstOrDefault()
											?.GatePassId
											?? 0
											;
			return loGPTree;
		}

		public CTX.GatePasses PatchPassStatus(int linkid, int status)
		{
			CTX.GatePasses lGP = dbcontext.GatePasses.Where(y => y.GatePassId == linkid).FirstOrDefault();
			if (lGP == null)
			{
				log.LogError("Gateass with LinkID = {0} does not exist", linkid);
				return null;
			}
			if (lGP.Status == status)
			{
				log.LogInformation("Gatepass with LinkID = {0} already has status = {1}", linkid, status);
				return lGP;
			}

			lGP.Status = status;
			lGP.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			lGP.EditOn = DateTime.Now;

			int liCount = dbcontext.SaveChanges();
			log.LogInformation("Updated {0} records for LinkID = {1}, status = {2}", liCount, linkid, status);

			return lGP;
		}

		public CTX.GatePasses UpdatePass(CTX.GatePasses gatepass, ML.UserPayload CurrUser)
		{

			// Validate submitted Pass
			if (gatepass == null)
			{
				return null;
			}

			log.LogDebug("Inspect submitted object");
			CTX.GatePasses lGP = null;
			UserData = CurrUser;

			switch ((SE.GATE_PASS_TYPE)gatepass.PassType)
			{
				case SE.GATE_PASS_TYPE.PATIENT_NEW:
				case SE.GATE_PASS_TYPE.PATIENT_REVISIT:
					lGP = UpdatePatientPass(gatepass);
					break;
				case SE.GATE_PASS_TYPE.VEHICLE:
					lGP = UpdateVehiclePass(gatepass);
					break;
				case SE.GATE_PASS_TYPE.VENDOR:
				case SE.GATE_PASS_TYPE.VISITOR_STAFF:
				case SE.GATE_PASS_TYPE.VISITOR_PATIENT:
				case SE.GATE_PASS_TYPE.BLOOD_DONOR:
				case SE.GATE_PASS_TYPE.VISITOR_OTHER:
					lGP = UpdateVisitorPass(gatepass);
					break;
				case SE.GATE_PASS_TYPE.EMERGENCY:
					lGP = UpdateEmergencyPass(gatepass);
					break;
				default:
					break;
			}

			return lGP;
		}
	}
}
