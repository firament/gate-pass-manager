using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using BO = gpm_web.BizObjs;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.BizObjs
{
	public partial class SessionBZ : BZBase
	{
		public SL.DD_Lookup DDHelp { get; set; }
		public SessionBZ(
			  ILogger<SessionBZ> logger
			, GC.IAppSettingContainer App_Config
			, gpm.Efc.dbsets.GPM_DBContext db_context
			, SL.DD_Lookup dd_helper
			)
		: base(logger, App_Config, db_context)
		{
			DDHelp = dd_helper;
		}

		public bool TestUser(string UserID, string UserPassword){
			if ((string.IsNullOrWhiteSpace(UserID)) || (string.IsNullOrWhiteSpace(UserPassword)))
			{
				log.LogError("Unable to authenticate user with given data");
				return false;
			}
			log.LogInformation("Testing User {0}", UserID);

			//##	get entry from DB
			int liCount = dbcontext.Users.Count(u => u.UserLogin == UserID);
			if (liCount != 1)
			{
				log.LogError("User lookup Failed. Expected 1 record, got {0} records.", liCount);
				return false;
			}
			CTX.Users loKnocker = dbcontext.Users.First(u => u.UserLogin == UserID);

			//##	get hashed password, and other lookups
			string lsPwdHash = GC.Utils.GetHashedPWD(UserPassword);

			//##	assess request
			if ((loKnocker.Status != (int)Enums.USER_STATUS.Active) || (loKnocker.UserPwd != lsPwdHash))
			{
				log.LogWarning("User authentication failed. UserID = {0}, UserLogin = {1}, Status = {2}, UserPwd Match = {3}"
											, loKnocker.UserId
											, loKnocker.UserLogin
											, loKnocker.Status.ToString()
											, (loKnocker.UserPwd == lsPwdHash)
											);
				dbcontext.MaintLoginHistory.Add(new CTX.MaintLoginHistory()
				{
					UserId = loKnocker.UserId,
					UserLogin = loKnocker.UserLogin,
					Status = (int)Enums.LOG_ENTRY_TYPE.FAILAUTH,
				});
				return false;
			}
			// If here all is WELL.
			log.LogInformation("OK => UserID = {0}, UserPassword = ************");
			return true;
		}

		public ML.UserPayload CreateUserSession(string UserID, string UserPassword)
		{
			if ((string.IsNullOrWhiteSpace(UserID)) || (string.IsNullOrWhiteSpace(UserPassword)))
			{
				// Fail the request, write details in the log
				log.LogError("Unable to authenticate user with given data");
				return null;
			}

			// 200	OkResult
			// 204	NoContentResult
			// 400	BadRequestResult
			// 401	UnauthorizedResult
			// 404	NotFoundResult
			// 409	ConflictResult
			// 415	UnsupportedMediaTypeResult
			// 422	UnprocessableEntityResult
			Models.UserPayload loUPL;
			string lsRoleTxt = "";
			string lsJSON = "";
			string lsToken = Guid.NewGuid().ToString("N");
			DateTime ldtNow = DateTime.Now;
			CTX.MaintSessions loSenEntry;

			//##	get entry from DB
			int liCount = dbcontext.Users.Count(u => u.UserLogin == UserID);
			if (liCount != 1)
			{
				log.LogError("User lookup Failed. Expected 1 record, got {0} records.", liCount);
				return null;
			}
			CTX.Users loKnocker = dbcontext.Users.First(u => u.UserLogin == UserID);

			//##	get hashed password, and other lookups
			string lsPwdHash = GC.Utils.GetHashedPWD(UserPassword);

			//##	assess request
			if ((loKnocker.Status != (int)Enums.USER_STATUS.Active) || (loKnocker.UserPwd != lsPwdHash))
			{
				log.LogWarning("User authentication failed. UserID = {0}, UserLogin = {1}, Status = {2}, UserPwd Match = {3}"
											, loKnocker.UserId
											, loKnocker.UserLogin
											, loKnocker.Status
											, (loKnocker.UserPwd == lsPwdHash)
											);
				dbcontext.MaintLoginHistory.Add(new CTX.MaintLoginHistory()
				{
					UserId = loKnocker.UserId,
					UserLogin = loKnocker.UserLogin,
					Status = (int)Enums.LOG_ENTRY_TYPE.FAILAUTH,
				});
				return null;
			}

			//  create payload container
			lsRoleTxt = Lookups.DD_Lookup.RoleText(loKnocker.Role).ToUpper();
			loUPL = new ML.UserPayload()
			{
				Msid = 0, // loSenEntry.Msid,
				Token = lsToken,
				Userid = loKnocker.UserId,
				LoginID = loKnocker.UserLogin,
				DisplayName = loKnocker.DisplayName,
				Role = lsRoleTxt,
				IssueTime = ldtNow,
				AuthTime = ldtNow,
				LastUsed = ldtNow,
				Status = (int)Enums.SESSION_STATUS.ACTIVE,
				DeviceSig = "",
			};
			lsJSON = GC.Utils.ToJSON(loUPL, false);

			//##	add session entry
			loSenEntry = new CTX.MaintSessions()
			{
				Token = lsToken,
				Userid = loKnocker.UserId,
				Role = lsRoleTxt,
				IssueTime = ldtNow,
				AuthTime = ldtNow,
				LastUsed = ldtNow,
				Status = (int)Enums.SESSION_STATUS.ACTIVE,
				DeviceSig = lsJSON,
			};
			dbcontext.MaintSessions.Add(loSenEntry);
			dbcontext.MaintLoginHistory.Add(new CTX.MaintLoginHistory()
			{
				UserId = loKnocker.UserId,
				UserLogin = loKnocker.UserLogin,
				Status = (int)Enums.LOG_ENTRY_TYPE.LOGIN,
			});

			//##	update last login time
			loKnocker.LastLogin = ldtNow;

			// DEBUG ADMIN SESSION BREAK - BEGIN
			string lsTS = "HH:mm:ss.ffff";
			log.LogDebug("Session: \nUD.IssueTime = {0}, MS.IssueTime = {1} \nUD.LastUsed = {2}, MS.AuthTime = {3} \nUD.AuthTime = {4}, MS.LastUsed = {5}"
				, loUPL.IssueTime.ToLocalTime().ToString(lsTS), loSenEntry.IssueTime.ToString(lsTS)
				, loUPL.LastUsed.ToLocalTime().ToString(lsTS), loSenEntry.LastUsed.ToString(lsTS)
				, loUPL.AuthTime?.ToLocalTime().ToString(lsTS), loSenEntry.AuthTime?.ToString(lsTS)
			);
			// DEBUG ADMIN SESSION BREAK - END


			try
			{
				liCount = dbcontext.SaveChanges();
			}
			catch (System.Exception exS)
			{
				log.LogError(exS, "Database update failed to create login entries");
				return null;
			}

			return loUPL;

		}

		public bool CloseUserSession(string Token){
			bool lbOK = false;
			if (string.IsNullOrWhiteSpace(Token))
			{
				log.LogError("Token is Empty. Need a valid token. Aborting further processing.");
				return false;
			}
			CTX.MaintSessions loSession = null;
			try
			{
				loSession = dbcontext.MaintSessions
								.Where(y => y.Token == Token)
								.FirstOrDefault()
								;
			}
			catch (System.Exception exS)
			{
				log.LogError(exS, "Error getting session record. Token = {0}", Token);
				return lbOK;
			}
			if (loSession == null)
			{
				log.LogInformation("No record found for session. Token = {0}", Token);
				return lbOK;
			}
			if (loSession.Status != (int)SE.SESSION_STATUS.ACTIVE)
			{
				log.LogInformation("Session is not Active. Token = {0}, Curr Status = {1}"
						, Token
						, DDHelp.DisplayText((int)SE.DDCategories.SESSION_STATUS, loSession.Status, true));
				return lbOK;
			}
			try
			{
				loSession.Status = (int)SE.SESSION_STATUS.CLOSED;
				int liCount = dbcontext.SaveChanges();
				log.LogDebug("Updated session status. Record count = {0}", liCount);
				lbOK = true;
			}
			catch (System.Exception exS)
			{
				log.LogError(exS, "Error updating session status. Token = {0}", Token);
				return lbOK;
			}
			return lbOK;
		}

		[Obsolete("Code Coverage Test", true)]
		// Remove - Not used
		public bool VerifyUserSession(string Token){
			bool lbOK = false;
			if (string.IsNullOrWhiteSpace(Token))
			{
				log.LogError("Token is Empty. Need a valid token. Aborting further processing.");
				return false;
			}
			CTX.MaintSessions loSession = null;
			try
			{
				loSession = dbcontext.MaintSessions.AsNoTracking()
								.Where(y => y.Token == Token)
								.FirstOrDefault()
								;
			}
			catch (System.Exception exS)
			{
				log.LogError(exS, "Error getting session record. Token = {0}", Token);
				return lbOK;
			}
			if (loSession == null)
			{
				log.LogInformation("No active session found. Token = {0}", Token);
				return lbOK;
			}
			lbOK = loSession.Status == (int)SE.SESSION_STATUS.ACTIVE;
			if (!lbOK){
				log.LogInformation("Session is not Active. Token = {0}, Curr Status = {1}"
						, Token
						, DDHelp.DisplayText((int)SE.DDCategories.SESSION_STATUS, loSession.Status, true));
			}
			return lbOK;
		}
	}
}
