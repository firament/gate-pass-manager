using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using BO = gpm_web.BizObjs;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.BizObjs
{
	// Contains all Update helper functions
	public partial class GatePassBZ : BZBase
	{
		private int SaveChanges(CTX.GatePasses gatepass)
		{
			int liCount = 0;
			gatepass.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
			gatepass.EditOn = DateTime.Now;
			liCount = dbcontext.SaveChanges();
			log.LogInformation("Updated {0} records for gatepass with LinkID = {1}", liCount, gatepass.GatePassId);
			return liCount;
		}

		private CTX.GatePasses UpdatePatientPass(CTX.GatePasses gatepass)
		{
			bool lbHasChange = false;
			CTX.GatePasses lGP = null;
			lGP = dbcontext.GatePasses.Where(y => y.GatePassId == gatepass.GatePassId).FirstOrDefault();

			// Check for null
			if (lGP == null)
			{
				log.LogError("No Matching record in DB to update. ID = {0}, Aborting further processing.", gatepass.GatePassId);
				return null;
			}

			// Check for changes in feilds MobileNum, HositalNum, wheelchairID, Note
			if (!string.IsNullOrWhiteSpace(gatepass.MobileNum))
			{
				lGP.MobileNum = gatepass.MobileNum.Trim().ToUpper();
				lbHasChange = true;
			}
			if (!string.IsNullOrWhiteSpace(gatepass.HospitalNum))
			{
				lGP.HospitalNum = gatepass.HospitalNum.Trim().ToUpper();
				lbHasChange = true;
			}
			// CR 14 - sak - 2021 Feb 08
			if (!string.IsNullOrWhiteSpace(gatepass.Note))
			{
				lGP.Note = gatepass.Note.Trim();
				lbHasChange = true;
			}
			if (!string.IsNullOrWhiteSpace(gatepass.WheelchairNum))
			{
				lGP.WheelchairNum = gatepass.WheelchairNum.Trim().ToUpper();
				lbHasChange = true;
			}
			// Test for addl attendants
			if (gatepass.PartyCount > lGP.PartyCount)
			{
				DateTime ldtNow = DateTime.Now.ToLocalTime();
				DateTime ldtToday = DateTime.Today.ToLocalTime();
				int liTokenCount = gatepass.PartyCount - lGP.PartyCount;
				int liTokenNumber = Token_Counter.GetTokenNumbers(liTokenCount);
				log.LogInformation("Adding addl Party to pass. ID = {0}, Curr = {1}, New = {2}"
									, lGP.GatePassId, lGP.PartyCount, gatepass.PartyCount);

				for (int vI = 0; vI < liTokenCount; vI++)
				{
					lGP.InverseRootGatePassNavigation.Add(
						new CTX.GatePasses()
						{
							PassType = (int)SE.GATE_PASS_TYPE.PARTY,
							PassNum = liTokenNumber++,
							RootGatePass = lGP.GatePassId,
							IssueDate = ldtToday,
							Department = lGP.Department,
							TimeIn = ldtNow,
							Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
							AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
							AddOn = ldtNow,
							EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
							EditOn = ldtNow,
						}
					);
				}
				lGP.PartyCount = gatepass.PartyCount;
				lbHasChange = true;
			}

			if (lbHasChange)
			{
				SaveChanges(lGP);
			}
			return lGP;
		}   // UpdatePatientPass

		private CTX.GatePasses UpdateVisitorPass(CTX.GatePasses gatepass)
		{
			bool lbHasChange = false;
			DateTime ldtNow = DateTime.Now.ToLocalTime();
			DateTime ldtToday = DateTime.Today.ToLocalTime();
			int liTokenCount = 0;   // gatepass.PartyCount - lGP.PartyCount;
			int liTokenNumber = Token_Counter.GetTokenNumbers(liTokenCount);

			CTX.GatePasses lGP = null;
			lGP = dbcontext.GatePasses.Where(y => y.GatePassId == gatepass.GatePassId).FirstOrDefault();

			// Check for null
			if (lGP == null)
			{
				log.LogError("No Matching record in DB to update. ID = {0}, Aborting further processing.", gatepass.GatePassId);
				return null;
			}

			// Check for changes in feilds MobileNum, HositalNum, wheelchairID
			if (!string.IsNullOrWhiteSpace(gatepass.MobileNum))
			{
				lGP.MobileNum = gatepass.MobileNum.Trim().ToUpper();
				lbHasChange = true;
			}
			// Visit Type
			if (gatepass.VisitType != 0)
			{
				lGP.VisitType = gatepass.VisitType;
				lbHasChange = true;
			}
			// Department
			if (gatepass.Department != 0)
			{
				lGP.Department = gatepass.Department;
				lbHasChange = true;
			}
			// Person meeting
			if (!string.IsNullOrWhiteSpace(gatepass.PersonMeeting))
			{
				lGP.PersonMeeting = gatepass.PersonMeeting.Trim();
				lbHasChange = true;
			}
			// Supplies
			if (gatepass.SupplyDelivery >= 0)
			{
				lGP.SupplyDelivery = gatepass.SupplyDelivery;
				lbHasChange = true;
			}

			// Vehicle Type
			// Vehicle Num
			if (gatepass.VehicleType != 0 && !string.IsNullOrWhiteSpace(gatepass.VehiclePlateNum))
			{
				// Get existing Pass
				CTX.GatePasses pVehicle = lGP.InverseRootGatePassNavigation.Where(v => v.PassType == (int)SE.GATE_PASS_TYPE.VEHICLE).FirstOrDefault();
				if (pVehicle == null)
				{
					// Create New Vehicle Pass
					liTokenNumber = Token_Counter.GetTokenNumbers(1);
					lGP.InverseRootGatePassNavigation.Add(new CTX.GatePasses()
					{
						PassType = (int)SE.GATE_PASS_TYPE.VEHICLE,
						PassNum = liTokenNumber++,
						IssueDate = ldtToday,
						Department = gatepass.Department,
						HasVehicle = gatepass.HasVehicle,
						VehicleType = gatepass.VehicleType,
						VehiclePlateNum = gatepass.VehiclePlateNum.Trim().ToUpper(),
						TimeIn = ldtNow,
						Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
						AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
						AddOn = ldtNow,
						EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
						EditOn = ldtNow,
						RootGatePass = lGP.PassNum,
					});
				}
				lGP.HasVehicle = (int)SE.YES_NO.YES;
				lGP.VehicleType = gatepass.VehicleType;
				lGP.VehiclePlateNum = gatepass.VehiclePlateNum.Trim().ToUpper();
				lbHasChange = true;
			}

			// Purpose
			if (!string.IsNullOrWhiteSpace(gatepass.Note))
			{
				lGP.Note = gatepass.Note.Trim();
				lbHasChange = true;
			}
			// Test for addl attendants
			if (gatepass.PartyCount > lGP.PartyCount)
			{
				liTokenCount = gatepass.PartyCount - lGP.PartyCount;
				liTokenNumber = Token_Counter.GetTokenNumbers(liTokenCount);
				log.LogInformation("Adding addl Party to pass. ID = {0}, Curr = {1}, New = {2}"
									, lGP.GatePassId, lGP.PartyCount, gatepass.PartyCount);

				for (int vI = 0; vI < liTokenCount; vI++)
				{
					lGP.InverseRootGatePassNavigation.Add(
						new CTX.GatePasses()
						{
							PassType = (int)SE.GATE_PASS_TYPE.PARTY,
							PassNum = liTokenNumber++,
							RootGatePass = lGP.GatePassId,
							IssueDate = ldtToday,
							Department = lGP.Department,
							TimeIn = ldtNow,
							Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
							AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
							AddOn = ldtNow,
							EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
							EditOn = ldtNow,
						}
					);
				}
				lGP.PartyCount = gatepass.PartyCount;
				lbHasChange = true;
			}

			if (lbHasChange)
			{
				SaveChanges(lGP);
			}
			return lGP;
		}   // UpdateVisitorPass

		private CTX.GatePasses UpdateEmergencyPass(CTX.GatePasses gatepass)
		{
			bool lbHasChange = false;
			CTX.GatePasses lGP = null;
			lGP = dbcontext.GatePasses.Where(y => y.GatePassId == gatepass.GatePassId).FirstOrDefault();

			// Check for null
			if (lGP == null)
			{
				log.LogError("No Matching record in DB to update. ID = {0}, Aborting further processing.", gatepass.GatePassId);
				return null;
			}

			// Check for changes in feilds Veh Type, Veh Num, Party Count
			// Vehicle Type
			if (gatepass.VehicleType != 0)
			{
				lGP.VehicleType = gatepass.VehicleType;
				lbHasChange = true;
			}
			// Vehicle Num
			if (!string.IsNullOrWhiteSpace(gatepass.VehiclePlateNum))
			{
				lGP.VehiclePlateNum = gatepass.VehiclePlateNum.Trim().ToUpper();
				lbHasChange = true;
			}
			// Test for addl attendants
			if (gatepass.PartyCount > lGP.PartyCount)
			{
				DateTime ldtNow = DateTime.Now.ToLocalTime();
				DateTime ldtToday = DateTime.Today.ToLocalTime();
				int liTokenCount = gatepass.PartyCount - lGP.PartyCount;
				int liTokenNumber = Token_Counter.GetTokenNumbers(liTokenCount);
				log.LogInformation("Adding addl Party to pass. ID = {0}, Curr = {1}, New = {2}"
									, lGP.GatePassId, lGP.PartyCount, gatepass.PartyCount);

				for (int vI = 0; vI < liTokenCount; vI++)
				{
					lGP.InverseRootGatePassNavigation.Add(
						new CTX.GatePasses()
						{
							PassType = (int)SE.GATE_PASS_TYPE.PARTY,
							PassNum = liTokenNumber++,
							RootGatePass = lGP.GatePassId,
							IssueDate = ldtToday,
							Department = lGP.Department,
							TimeIn = ldtNow,
							Status = (int)SE.GATE_PASS_STATUS.IN_CAMPUS,
							AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
							AddOn = ldtNow,
							EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL,
							EditOn = ldtNow,
						}
					);
				}
				lGP.PartyCount = gatepass.PartyCount;
				lbHasChange = true;
			}

			if (lbHasChange)
			{
				SaveChanges(lGP);
			}
			return lGP;
		}   // UpdateEmergencyPass

		private CTX.GatePasses UpdateVehiclePass(CTX.GatePasses gatepass)
		{

			bool lbHasChange = false;
			CTX.GatePasses lGP = null;
			lGP = dbcontext.GatePasses.Where(y => y.GatePassId == gatepass.GatePassId).FirstOrDefault();

			// Check for null
			if (lGP == null)
			{
				log.LogError("No Matching record in DB to update. ID = {0}, Aborting further processing.", gatepass.GatePassId);
				return null;
			}

			// Check for changes in feilds Veh Type, Veh Num, Party Count
			// Vehicle Type
			if (gatepass.VehicleType != 0)
			{
				lGP.VehicleType = gatepass.VehicleType;
				lbHasChange = true;
			}
			// Vehicle Num
			if (!string.IsNullOrWhiteSpace(gatepass.VehiclePlateNum))
			{
				lGP.VehiclePlateNum = gatepass.VehiclePlateNum.Trim().ToUpper();
				lbHasChange = true;
			}

			if (lbHasChange)
			{
				SaveChanges(lGP);
			}
			return lGP;
		}
	}
}
