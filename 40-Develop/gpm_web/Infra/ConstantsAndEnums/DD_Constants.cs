using System;

namespace gpm_web.Consts{
	public static class USER_ROLES{
		public const string NOT_SET = "NOT_SET";
		public const string USER = "USER";
		public const string ADMIN = "ADMIN";
		public const string ANALYST = "ANALYST";
		public const string OPERATOR = "OPERATOR";
		public const string MAINTAINER = "MAINTAINER";
	}

	public static class TAGS{
		public const string OPTION_DISABLED = "cstm-option-disabled";
	}

}
