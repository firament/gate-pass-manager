using System;

namespace gpm_web.Enums
{

	public enum DDCategories
	{
		YES_NO = 1,
		RECORD_STATUS = 2,
		DEPARTMENTS = 3,
		USER_ROLE = 4,
		USER_STATUS = 5,
		QUOTA_STATUS = 6,
		GATE_PASS_TYPE = 7,
		GATE_PASS_STATUS = 8,
		VISIT_TYPE = 9,
		VEHICLE_TYPE = 10,
		RESOURCE_TYPE = 11,
		RESOURCE_STATUS = 12,
		SESSION_STATUS = 13,
		ATTACHMENT_TYPE = 14,
		ATTACHMENT_STATUS = 15,
		MIME_TYPE = 16,
		PASS_LOG_ACTION = 17,
		LOG_ENTRY_TYPE = 18,

		DEPT_V_VENDOR = 19,
		DEPT_V_STAFF = 20,

		DEPTS_ALL = 90,
	}

	// YES_NO = 1,
	public enum YES_NO
	{
		NO = 0,
		YES = 1,
	}

	// RECORD_STATUS = 2,
	public enum RECORD_STATUS
	{
		NOT_SET = 0,
		ACTIVE = 1,
		INACTIVE = 2,
		DELETED = 3,
	}

	// USER_ROLE = 4,
	public enum USER_ROLE
	{
		UNKNOWN = 0,
		User = 1,
		Admin = 2,
		Analyst = 3,
		Operator = 4,
		Maintainer = 5,
	}

	// USER_STATUS = 5,
	public enum USER_STATUS
	{
		Not_Set = 0,
		Active = 1,
		Inactive = 2,
		Blocked = 3,
		Hotlist = 4,
	}

	// QUOTA_STATUS = 6,
	public enum QUOTA_STATUS
	{
		NOT_SET = 0,
		ACTIVE = 1,
		INACTIVE = 2,
	}

	// GATE_PASS_TYPE = 7,
	public enum GATE_PASS_TYPE
	{
		NOT_SET = 0,
		PATIENT_NEW = 110,
		PATIENT_REVISIT = 120,
		VENDOR = 210,
		VISITOR_STAFF = 220,
		VISITOR_PATIENT = 230,
		BLOOD_DONOR = 240,
		VISITOR_OTHER = 290,
		EMERGENCY = 300,
		PARTY = 400,
		VEHICLE = 500,
		OTHER = 999,
	}

	// GATE_PASS_STATUS = 8,
	public enum GATE_PASS_STATUS
	{
		IN_CAMPUS = 1,
		STEP_OUT = 2,
		EXIT = 3,
		MISSING = 4,
		LOST_EXIT = 5,
		CANCEL = 6,
		OTHER = 7,
		NOT_SET = 0,
	}

	// VISIT_TYPE = 9,
	public enum VISIT_TYPE
	{
		NOT_SET = 0,
		FIRST_VISIT = 1,
		RE_VISIT = 2,
		NOT_APPLICABLE = 3,
	}

	// VEHICLE_TYPE = 10,
	public enum VEHICLE_TYPE
	{
		NO_PARKING_REQD = 0,
		BIKE = 1,
		SCOOTER = 2,
		CAR = 3,
		AMBULANCE = 4,
		OTHER = 99,
	}

	// RESOURCE_TYPE = 11,
	public enum RESOURCE_TYPE
	{
		NOT_SET = 0,
		WHELCHR = 1,
		OTHER = 99,
	}

	// RESOURCE_STATUS = 12,
	public enum RESOURCE_STATUS
	{
		NOT_SET = 0,
		AVLBLE = 1,
		ISSUED = 2,
		RETURNED = 3,
		ABANDON = 4,
		DAMAGED = 5,
		NOT_USABLE = 99,
	}

	// SESSION_STATUS = 13,
	public enum SESSION_STATUS
	{
		BROKEN = 0,
		ACTIVE = 1,
		EXPIRED = 2,
		LOG_OUT = 3,
		CLOSED = 4,
	}

	// LOG_ENTRY_TYPE = 18,
	public enum LOG_ENTRY_TYPE
	{
		NOT_SET = 0,
		LOGIN = 1,
		LOGOUT = 2,
		TIMEOUT = 3,
		FAILAUTH = 4,
		OTHER = 99,
	}


}
