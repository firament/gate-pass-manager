using System;

namespace gpm_web.Enums{

	public enum SearchType{
		UNKNOWN = 0,
		TOKEN = 1,
		ALL_ACTIVE = 2,
		CUSTOM = 3,
	}
}
