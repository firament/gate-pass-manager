using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Lookups
{
	public class DD_Lookup
	{
		internal readonly ILogger<DD_Lookup> log;
		internal readonly GC.IAppSettingContainer appconfig;

		private bool IsInitialized { get; set; } = false;
		private Dictionary<int, List<CTX.DdItems>> LookupItems;
		private int STATUS_ACTIVE = -1;

		public DD_Lookup(ILogger<DD_Lookup> logger, GC.IAppSettingContainer App_Config)
		{
			log = logger;
			appconfig = App_Config;
		}

		public List<ML.DD_Item> GetItems(SE.DDCategories Catg, bool ActiveOnly = true)
		{
			if (!IsInitialized) { Initialize(); }
			if (!IsInitialized)
			{
				log.LogError("No data to process.");
				return null;
			}
			List<CTX.DdItems> lstItems = null;

			if (Catg == SE.DDCategories.DEPTS_ALL)
			{
				return GetAllDepartments(ActiveOnly);
			}

			lstItems = ActiveOnly
					? LookupItems[(int)Catg].Where(a => a.Status == (int)SE.RECORD_STATUS.ACTIVE).ToList()
					: LookupItems[(int)Catg]
					;

			List<ML.DD_Item> lstOptions = new List<ML.DD_Item>();

			foreach (CTX.DdItems vItem in lstItems.OrderBy(ds => ds.DdiDispSeq))
			{
				lstOptions.Add(new ML.DD_Item()
				{
					CatgID = vItem.DdiCatgId,
					Value = vItem.DdiCode,
					DisplaySeq = vItem.DdiDispSeq,
					Text = vItem.DdiDispText,
					Label = vItem.DdiDesc,
					Active = vItem.Status == (int)SE.RECORD_STATUS.ACTIVE
				}
				);
			}
			return lstOptions;
		}

		// Get list of all Unique departments
		// TODO: Optimize for performance
		private List<ML.DD_Item> GetAllDepartments(bool ActiveOnly = true)
		{
			List<CTX.DdItems> lstItems = null;
			List<ML.DD_Item> lstOptions = new List<ML.DD_Item>();

			// Add Clinical
			lstItems = ActiveOnly
					? LookupItems[(int)SE.DDCategories.DEPARTMENTS].Where(a => a.Status == (int)SE.RECORD_STATUS.ACTIVE).ToList()
					: LookupItems[(int)SE.DDCategories.DEPARTMENTS]
					;
			foreach (CTX.DdItems vItem in lstItems.OrderBy(ds => ds.DdiDispSeq))
			{
				lstOptions.Add(new ML.DD_Item()
				{
					CatgID = vItem.DdiCatgId,
					Value = vItem.DdiCode,
					DisplaySeq = vItem.DdiDispSeq,
					Text = vItem.DdiDispText,
					Label = vItem.DdiDesc,
					Active = vItem.Status == (int)SE.RECORD_STATUS.ACTIVE
				}
				);
			}

			// Add Vendor
			lstItems = ActiveOnly
					? LookupItems[(int)SE.DDCategories.DEPT_V_VENDOR].Where(a => a.Status == (int)SE.RECORD_STATUS.ACTIVE).ToList()
					: LookupItems[(int)SE.DDCategories.DEPT_V_VENDOR]
					;
			foreach (CTX.DdItems vItem in lstItems.OrderBy(ds => ds.DdiDispSeq))
			{
				lstOptions.Add(new ML.DD_Item()
				{
					CatgID = vItem.DdiCatgId,
					Value = vItem.DdiCode,
					DisplaySeq = vItem.DdiDispSeq,
					Text = vItem.DdiDispText,
					Label = vItem.DdiDesc,
					Active = vItem.Status == (int)SE.RECORD_STATUS.ACTIVE
				}
				);
			}

			// Add Staff
			lstItems = ActiveOnly
					? LookupItems[(int)SE.DDCategories.DEPT_V_STAFF].Where(a => a.Status == (int)SE.RECORD_STATUS.ACTIVE).ToList()
					: LookupItems[(int)SE.DDCategories.DEPT_V_STAFF]
					;
			foreach (CTX.DdItems vItem in lstItems.OrderBy(ds => ds.DdiDispSeq))
			{
				lstOptions.Add(new ML.DD_Item()
				{
					CatgID = vItem.DdiCatgId,
					Value = vItem.DdiCode,
					DisplaySeq = vItem.DdiDispSeq,
					Text = vItem.DdiDispText,
					Label = vItem.DdiDesc,
					Active = vItem.Status == (int)SE.RECORD_STATUS.ACTIVE
				}
				);
			}

			// Consolidate all list, remove duplicates
			List<ML.DD_Item> lstDistinct = new List<ML.DD_Item>();
			int liPrev = -1; //, liCurr = -2;
			foreach (ML.DD_Item vItem in lstOptions.OrderBy(y => y.Value))
			{
				if (liPrev == vItem.Value) { continue; }
				liPrev = vItem.Value;
				lstDistinct.Add(vItem);
			}
			return lstDistinct;
		}


		public List<AM.DD_Catgs_Model> GetCategories(bool ActiveOnly = true, bool includeItems = true)
		{
			if (!IsInitialized) { Initialize(); }
			if (!IsInitialized)
			{
				log.LogError("No data to process.");
				return null;
			}
			List<AM.DD_Catgs_Model> lstCatgs = new List<AM.DD_Catgs_Model>();

			List<CTX.DdCategories> lstCategories = null;

			CTX.GPM_DBContext dbcontext = new CTX.GPM_DBContext(appconfig.DBConnectionStringRO);
			try
			{
				lstCategories = ActiveOnly
								? dbcontext?.DdCategories?.Where(s => s.Status == STATUS_ACTIVE)?.Include(y => y.DdItems)?.ToList()
								: dbcontext?.DdCategories?.Include(y => y.DdItems)?.ToList()
								;

			}
			catch (System.Exception se)
			{
				log.LogCritical(se, "Error fetching data, unrecoverable.");
			}
			if (lstCategories == null)
			{
				log.LogError("Lookup data not availaible. Unrecoverable error. Aborting further processing.");
				return lstCatgs;
			}

			AM.DD_Catgs_Model lCatg = null;

			foreach (CTX.DdCategories vCatg in lstCategories.OrderBy(y => y.DdiCatgId))
			{
				lCatg = new AM.DD_Catgs_Model()
				{
					CatgID = vCatg.DdiCatgId,
					CatgName = vCatg.DdiCatgName,
					CatgDesc = vCatg.DdiCatgDesc,
					Active = vCatg.Status == (int)SE.RECORD_STATUS.ACTIVE,
				};
				if (includeItems)
				{
					foreach (CTX.DdItems vItem in vCatg.DdItems.OrderBy(y => y.DdiDispSeq).ThenBy(y => y.DdiCode))
					{
						// filter only active, using flag
						if (ActiveOnly && vItem.Status != (int)SE.RECORD_STATUS.ACTIVE) continue;
						lCatg.Items.Add(new ML.DD_Item()
						{
							CatgID = vItem.DdiCatgId,
							Value = vItem.DdiCode,
							DisplaySeq = vItem.DdiDispSeq,
							Text = vItem.DdiDispText,
							Label = vItem.DdiDesc,
							Active = vItem.Status == (int)SE.RECORD_STATUS.ACTIVE
						});

					}
				}
				lstCatgs.Add(lCatg);
			}
			return lstCatgs;
		}


		public string DisplayText(int CatgID, int ItemID, bool IncludeInactive = false)
		{
			if (!IsInitialized) { Initialize(); }
			if (!IsInitialized)
			{
				log.LogError("No data to process.");
				return string.Empty;
			}

			string lsDisplayText = string.Empty;
			if (CatgID == (int)SE.DDCategories.DEPARTMENTS)
			{
				lsDisplayText = CustomDepartmentText(ItemID, IncludeInactive);
			}
			else
			{
				lsDisplayText = IncludeInactive
					? LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID)?.DdiDispText
					: LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID && ic.Status == STATUS_ACTIVE)?.DdiDispText
					;
			}
			if (string.IsNullOrWhiteSpace(lsDisplayText))
			{
				log.LogError("No entries defined for Item ID {0} with Category ID {1} [IncludeInactive = {2}]", ItemID, CatgID, IncludeInactive);
			}
			return lsDisplayText;
		}

		/// <summary>
		/// Overide to manage custom list of departments for certain visitors
		/// </summary>
		/// <param name="ItemID"></param>
		/// <param name="IncludeInactive"></param>
		/// <returns></returns>
		private string CustomDepartmentText(int ItemID, bool IncludeInactive = false)
		{
			string lsDisplayText = string.Empty;
			Dictionary<int, List<CTX.DdItems>> lsDepts = new Dictionary<int, List<CTX.DdItems>>();

			int CatgID = 0;
			// First search in default list
			CatgID = (int)SE.DDCategories.DEPARTMENTS;
			lsDisplayText = IncludeInactive
				? LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID)?.DdiDispText
				: LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID && ic.Status == STATUS_ACTIVE)?.DdiDispText
				;

			// Next search in vendor list
			CatgID = (int)SE.DDCategories.DEPT_V_VENDOR;
			if (string.IsNullOrWhiteSpace(lsDisplayText))
			{
				lsDisplayText = IncludeInactive
					? LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID)?.DdiDispText
					: LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID && ic.Status == STATUS_ACTIVE)?.DdiDispText
					;
			}

			// Next search in staff list
			CatgID = (int)SE.DDCategories.DEPT_V_STAFF;
			if (string.IsNullOrWhiteSpace(lsDisplayText))
			{
				lsDisplayText = IncludeInactive
					? LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID)?.DdiDispText
					: LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID && ic.Status == STATUS_ACTIVE)?.DdiDispText
					;
			}

			return lsDisplayText;
		}

		[Obsolete("Code coverage test.Not used now, for future enhancement (2021 Feb 08)", true)]
		public string CodeText(int CatgID, int ItemID, bool IncludeInactive = false)
		{
			if (!IsInitialized) { Initialize(); }
			if (!IsInitialized)
			{
				log.LogError("No data to process.");
				return string.Empty;
			}

			string lsDisplayText = string.Empty;
			lsDisplayText = IncludeInactive
				? LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID)?.DdiCodeTxt
				: LookupItems[CatgID]?.FirstOrDefault(ic => ic.DdiCode == ItemID && ic.Status == STATUS_ACTIVE)?.DdiCodeTxt
				;
			if (string.IsNullOrWhiteSpace(lsDisplayText))
			{
				log.LogError("No entries defined for Item ID {0} for Category ID {0}", CatgID, ItemID);
			}
			return lsDisplayText;
		}

		public string FormattedPassNumber(int PassType, int PassNumber)
		{
			string lsFormatted = "";
			if (PassNumber == 0)
			{
				return "--NA--";
			}
			switch ((SE.GATE_PASS_TYPE)PassType)
			{
				case SE.GATE_PASS_TYPE.PATIENT_NEW:
					lsFormatted = PassNumber.ToString("F 000");
					break;
				case SE.GATE_PASS_TYPE.PATIENT_REVISIT:
					lsFormatted = PassNumber.ToString("R 000");
					break;
				case SE.GATE_PASS_TYPE.VENDOR:
					lsFormatted = PassNumber.ToString("VV 000");
					break;
				case SE.GATE_PASS_TYPE.VISITOR_STAFF:
					lsFormatted = PassNumber.ToString("VS 000");
					break;
				case SE.GATE_PASS_TYPE.VISITOR_PATIENT:
					lsFormatted = PassNumber.ToString("VP 000");
					break;
				case SE.GATE_PASS_TYPE.BLOOD_DONOR:
					lsFormatted = PassNumber.ToString("VL 000");
					break;
				case SE.GATE_PASS_TYPE.EMERGENCY:
					lsFormatted = PassNumber.ToString("E 000");
					break;
				case SE.GATE_PASS_TYPE.PARTY:
					lsFormatted = PassNumber.ToString("A 000");
					break;
				case SE.GATE_PASS_TYPE.VEHICLE:
					lsFormatted = PassNumber.ToString("T 000");
					break;
				default:
					lsFormatted = PassNumber.ToString("XX 000");
					break;
			}
			return lsFormatted;

		}



		private void Initialize()
		{
			log.LogDebug("==> Initialize");
			/*
			Get all lookups
			 */
			if (IsInitialized)
			{
				return;
			}

			STATUS_ACTIVE = (int)SE.RECORD_STATUS.ACTIVE;
			List<CTX.DdCategories> lstCatgs = null;
			List<CTX.DdItems> lstItems = null;

			CTX.GPM_DBContext dbcontext = new CTX.GPM_DBContext(appconfig.DBConnectionStringRO);
			try
			{
				lstCatgs = dbcontext?.DdCategories?
							.Where(s => s.Status == STATUS_ACTIVE)?
							.ToList()
							;
				lstItems = dbcontext.DdItems
							.ToList()
							;
			}
			catch (System.Exception se)
			{
				log.LogCritical(se, "Error fetching data, unrecoverable.");
			}

			if (lstCatgs == null || lstItems == null)
			{
				log.LogError("Lookup data not availaible. Unrecoverable error. Aborting further processing.");
				return;
			}

			// Group all items by category
			int liCatgCode = 0;
			List<CTX.DdItems> vCatgItems;
			LookupItems = new Dictionary<int, List<CTX.DdItems>>();
			foreach (CTX.DdCategories vCat in lstCatgs)
			{
				liCatgCode = vCat.DdiCatgId;
				vCatgItems = new List<CTX.DdItems>();
				foreach (CTX.DdItems vItm in lstItems.Where(f => f.DdiCatgId == liCatgCode).OrderBy(oc => oc.DdiDispSeq))
				{
					vCatgItems.Add(new CTX.DdItems()
					{
						DdiCatgId = liCatgCode,
						DdiCode = vItm.DdiCode,
						DdiDispSeq = vItm.DdiDispSeq,
						DdiCodeTxt = vItm.DdiCodeTxt,
						DdiDispText = vItm.DdiDispText,
						DdiDesc = vItm.DdiDesc,
						Status = vItm.Status,
					});
				}
				LookupItems.Add(liCatgCode, vCatgItems);
			}
			IsInitialized = true;
		}


		// TODO: Fill dictionary from DD_Items to retun uoppercased text using the code given

		/// <summary>
		/// Hardcoded for use in Policy based authorization.
		/// </summary>
		/// <param name="RoleID"></param>
		/// <returns></returns>
		public static string RoleText(int RoleID)
		{
			string lsRoleTxt = Consts.USER_ROLES.NOT_SET;
			switch ((Enums.USER_ROLE)RoleID)
			{
				case Enums.USER_ROLE.User:
					lsRoleTxt = Consts.USER_ROLES.USER;
					break;
				case Enums.USER_ROLE.Admin:
					lsRoleTxt = Consts.USER_ROLES.ADMIN;
					break;
				default:
					break;
			}
			return lsRoleTxt;
		}


	}
}
