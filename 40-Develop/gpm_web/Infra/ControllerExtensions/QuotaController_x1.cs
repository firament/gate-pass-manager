using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using gpm_web.Models;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{
	/// <summary>
	/// 
	/// </summary>
	public partial class QuotaController : Infra.GPMController
	{

		/// <summary>
		/// 
		/// </summary>
		/// <param name="DaySel_1"></param>
		/// <param name="DaySel_2"></param>
		/// <param name="Date">Ignored for now.</param>
		/// <param name="CapNewVisit"></param>
		/// <param name="CapRevisit"></param>
		/// <param name="Department"></param>
		/// <returns></returns>
		internal List<CTX.Quotas> BuildQuoteList(
										  DateTime DaySel_1
										, DateTime DaySel_2
										, string[] Date
										, string[] CapNewVisit
										, string[] CapRevisit
										, string[] Department
										)
		{
			// Convert value array to object list
			log.LogDebug("==> BuildQuoteList");
			List<CTX.Quotas> lstQts = null;
			List<CTX.Quotas> lstQtsDay = null;

			// Verify all arrays are same length
			int liCount = Date.Length;
			if (liCount < 1)
			{
				log.LogError("No records to process, cannot proceed.");
				return lstQts;
			}
			if (CapNewVisit.Length != liCount || CapRevisit.Length != liCount || Department.Length != liCount)
			{
				log.LogError("Data Mismatch, cannot proceed reliably.");
				return lstQts;
			}

			// Working variables
			DateTime ldtDate, ldSelectedDate;
			int liCapNew, liCapRevisit, liDept;
			CTX.Quotas lrQuota;

			// TODO: validate and scrub

			// process for all dates
			bool lbContinue = true;
			ldSelectedDate = DaySel_1;
			lstQts = new List<CTX.Quotas>();
			
			log.LogInformation(
							"Building quota updates for dates {0} to {1}"
							, DaySel_1.ToString(SC.FORMATS.DATE_FORMAT)
							, DaySel_2.ToString(SC.FORMATS.DATE_FORMAT)
							);
			while (lbContinue)
			{
				log.LogDebug("Processing for date {0}", ldSelectedDate.ToString(SC.FORMATS.DATE_FORMAT));
				//get quotas for day sel
				lstQtsDay = getDayQuotas(ldSelectedDate);
				// process each element
				for (int vIdx = 0; vIdx < liCount; vIdx++)
				{
					ldtDate = DateTime.Parse(Date[vIdx]);
					liCapNew = int.Parse(CapNewVisit[vIdx]);
					liCapRevisit = int.Parse(CapRevisit[vIdx]);
					liDept = int.Parse(Department[vIdx]);
					lrQuota = lstQtsDay.Where(y => y.Department == liDept).FirstOrDefault();
					log.LogDebug("Quota Dept = {0}, New = {1}, ReV = {2}, Status = {3}, Date = {4}"
								, liDept, liCapNew, liCapRevisit, lrQuota?.Status ?? -1, lrQuota?.Date
								);
					if (lrQuota == null)
					{
						log.LogWarning("Unexpected condition. Expected 1 record for Date = {0} and Dept = {1}, but got 0 records. Building new record for now."
										, ldSelectedDate.ToString(SC.FORMATS.DATE_FORMAT)
										, liDept
										);
						lrQuota = new CTX.Quotas()
						{
							Department = liDept,
							Date = ldSelectedDate,
							DateInt = ldSelectedDate.Ticks,
							Status = (int)SE.RECORD_STATUS.NOT_SET,
						};
					}
					lrQuota.CapNewVisit = liCapNew;
					lrQuota.CapRevisit = liCapRevisit;
					lstQts.Add(lrQuota);
				}

				// move to next date
				ldSelectedDate = ldSelectedDate.AddDays(1);
				lbContinue = ldSelectedDate <= DaySel_2;
			}
			return lstQts;
		}



		internal List<CTX.Quotas> getDayQuotas(DateTime dyDay)
		{
			log.LogDebug("==> getDayQuotas");
			log.LogDebug("Building quotas for day {0}", dyDay.ToString(SC.FORMATS.DATE_FORMAT));
			List<CTX.Quotas> lstQuotas = null;
			List<CTX.Quotas> lstQuotasDef = null;

			//get department list, active only
			List<ML.DD_Item> lstDepts = DDHelp.GetItems(SE.DDCategories.DEPARTMENTS, true);
			log.LogInformation("Got {0} active departments", lstDepts.Count);

			int liCQ = 0;
			int liCD = lstDepts.Count;

			// get current quotas for day
			lstQuotas = dbcontext.Quotas.Where(f => f.Date == dyDay).ToList();
			log.LogInformation("Got {0} defined quotas for date {1}", lstQuotas.Count, dyDay.ToString(SC.FORMATS.DATE_FORMAT));
			liCQ = lstQuotas.Count;

			CTX.Quotas lmQuota = null;
			CTX.Quotas lmQDef = null;

			// All depts set, nothing more to do.
			if (liCQ == liCD)
			{
				// return lstQuotas;
			}

			// If all depts not set, use defaults
			lstQuotasDef = dbcontext.Quotas.Where(f => f.Date == DateTime.MinValue).ToList();
			log.LogInformation("Adding records for missing quotas for date.");
			foreach (ML.DD_Item vDept in lstDepts)
			{
				lmQuota = lstQuotas.Where(d => d.Department == vDept.Value).FirstOrDefault();
				lmQDef = lstQuotasDef.Where(d => d.Department == vDept.Value).FirstOrDefault();

				// cross-reference, if null
				if (lmQuota == null)
				{
					log.LogInformation("No Quota for Dept {0} - {1}, Creating now.", vDept.Value, vDept.Text);
					lstQuotas.Add(new CTX.Quotas()
					{
						Department = vDept.Value,
						DeprtmentName = vDept.Text,
						Date = dyDay,   		// lmQDef?.Date ?? 
						DateInt = dyDay.Ticks,  // lmQDef?.DateInt ?? 
						CapNewVisit = lmQDef?.CapNewVisit ?? 0,
						CapRevisit = lmQDef?.CapRevisit ?? 0,
						Status = (int)SE.RECORD_STATUS.NOT_SET, // lmQDef?.Status ?? 
					});
				}
				else
				{
					lmQuota.DeprtmentName = vDept.Text;
				}
			}
			log.LogInformation("All OK, have {0} quota records for day {1}", lstQuotas.Count, dyDay.ToString(SC.FORMATS.DATE_FORMAT));
			return lstQuotas;
		}
	}
}
