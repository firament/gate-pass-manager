using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Infra
{
	public static class WF_Tests
	{

		internal static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		static WF_Tests()
		{
			// log = NLog.LogManager.GetCurrentClassLogger() as Microsoft.Extensions.Logging.ILogger;
		}

		/// <summary>
		/// Inspect claims to extract Display Name and Role assignment.
		/// </summary>
		/// <param name="pUser"></param>
		public static void InspectClaims(ClaimsPrincipal pUser)
		{
			if (log == null)
			{
				Console.WriteLine("WARN: Logger isnot configured");
			}
			log.Debug("==> InspectClaims");
			StringBuilder lsbLog = new StringBuilder();
			lsbLog.AppendLine($"\n==> Begin gpm_web.Infra.WF_Tests.InspectClaims(ClaimsPrincipal pUser)");

			int liUserID;
			string lsName;
			string lsDisplayName;
			string lsRole;
			string lsToken;
			// string lsPayload;
			string lsTemp;

			ClaimsPrincipal loUser = pUser;
			ClaimsIdentity loIdentity = loUser.Identity as ClaimsIdentity;
			IEnumerable<Claim> loClaims = loUser.Claims;

			log.Debug("loIdentity.Name => {0}", loIdentity.Name);
			var loRoleClaim = loIdentity.FindFirst(c => c.Type == ClaimTypes.Role);
			log.Debug("Role => {0}", loRoleClaim?.Value ?? "null");

			// Inspect IEnumerable<Claim> loClaims
			int liCount = 1;
			foreach (Claim vClaim in loClaims)
			{
				lsbLog.AppendFormat("S# = {0}, Type = {1}, Value = {2}, ValueType = {3}, PropCount = {4}{5}", liCount++, vClaim.Type, vClaim.Value, vClaim.ValueType, vClaim.Properties.Count, Environment.NewLine);
			}
			log.Debug(lsbLog.ToString());
			/*
			S# = 1, Type = http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name, Value = Guest User, ValueType = http://www.w3.org/2001/XMLSchema#string
			S# = 2, Type = http://schemas.microsoft.com/ws/2008/06/identity/claims/role, Value = Guest, ValueType = http://www.w3.org/2001/XMLSchema#string
			S# = 3, Type = Guest, Value = true, ValueType = http://www.w3.org/2001/XMLSchema#boolean
			S# = 4, Type = http://schemas.microsoft.com/ws/2008/06/identity/claims/serialnumber, Value = 38fbddd3-6791-422d-94e4-d1bc69417da2, ValueType = http://www.w3.org/2001/XMLSchema#string
			S# = 5, Type = http://schemas.microsoft.com/ws/2008/06/identity/claims/userdata, Value = generic Payload data - look in cookies, ValueType = http://www.w3.org/2001/XMLSchema#string
			S# = 6, Type = UserID, Value = 099, ValueType = http://www.w3.org/2001/XMLSchema#string
			S# = 7, Type = LoginID, Value = 121, ValueType = http://www.w3.org/2001/XMLSchema#integer

			 */

			// Extract Specific details

			// lsDisplayName
			lsDisplayName = loUser?.Identity?.Name ?? string.Empty;

			// Name
			lsName = loUser.Claims
				.First(c => c.Type == ClaimTypes.Name)
				?.Value
				?? string.Empty
				;

			// Role
			lsRole = loUser.Claims
				.First(c => c.Type == ClaimTypes.Role)
				?.Value
				?? string.Empty
				;

			// token
			lsToken = loUser.Claims
				.First(c => c.Type == ClaimTypes.SerialNumber)
				?.Value
				?? string.Empty
				;

			// // User payload
			// lsPayload = loUser.Claims
			// 	.First(c => c.Type == ClaimTypes.UserData)
			// 	?.Value
			// 	?? string.Empty
			// 	;

			// USERID
			lsTemp = loUser.Claims
				.First(c => c.Type == Consts.KEYS.USERID_KEY)
				?.Value
				?? string.Empty
				;
			int.TryParse(lsTemp, out liUserID);

			lsbLog.AppendFormat("Name        : {0}{1}", lsName, Environment.NewLine);
			lsbLog.AppendFormat("DisplayName : {0}{1}", lsDisplayName, Environment.NewLine);
			lsbLog.AppendFormat("Role        : {0}{1}", lsRole, Environment.NewLine);
			lsbLog.AppendFormat("Token       : {0}{1}", lsToken, Environment.NewLine);
			lsbLog.AppendFormat("UserID      : {0}{1}", liUserID, Environment.NewLine);
			// lsbLog.AppendFormat("Payload     : {0}{1}", lsPayload, Environment.NewLine);
			// lsbLog.AppendFormat("Temp        : {0}{1}", lsTemp, Environment.NewLine);

			lsbLog.AppendLine($"<== End gpm_web.Infra.WF_Tests.InspectDates()");
			log.Debug(lsbLog.ToString());
		}


		/// <summary>
		/// Inspect default date values and formats
		/// </summary>
		public static void InspectDates()
		{
			log.Debug("==> InspectClaims");
			StringBuilder lsbLog = new StringBuilder();
			lsbLog.AppendLine($"\n==> Begin gpm_web.Infra.WF_Tests.InspectDates()");

			// DateTime loDate = new DateTime();
			var vdateonly = DateTime.Now;
			var vtoday = DateTime.Today;
			var vmindate = DateTime.MinValue;

			// https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings
			lsbLog.AppendFormat("Now     : {0:yyyy-MMM-dd HH:mm:ss zzz}{1}", vdateonly, Environment.NewLine);
			lsbLog.AppendFormat("Today   : {0:yyyy-MMM-dd HH:mm:ss zzz}{1}", vtoday, Environment.NewLine);
			lsbLog.AppendFormat("MinDate : {0:yyyy-MMM-dd HH:mm:ss zzz}{1}", vmindate, Environment.NewLine);

			lsbLog.AppendLine($"<== End gpm_web.Infra.WF_Tests.InspectDates()");
			log.Debug(lsbLog.ToString());
		}
	}
}
