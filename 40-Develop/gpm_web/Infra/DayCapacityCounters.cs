using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;


using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;


namespace gpm_web.Infra
{


	public class DayCapacityCounters
	{

		// Containers and Services from Dependency Injection
		internal readonly ILogger<DayCapacityCounters> log;
		internal readonly GC.IAppSettingContainer appconfig;

		private Dictionary<int, GC.DeptDayCap> TodayDeptCaps { get; set; } = new Dictionary<int, GC.DeptDayCap>();
		private bool IsInitialized = false;

		/// <summary>
		/// Date to identify need to rollover counter on date change
		/// </summary>
		private DateTime LastUpdateDate = DateTime.MinValue;

		public DayCapacityCounters(ILogger<DayCapacityCounters> logger, GC.IAppSettingContainer App_Config)
		{
			log = logger;
			appconfig = App_Config;
			log.LogDebug("==> DayCapacityCounters:CTOR");
		}

		// BEGIN: sak debug quota cache bug
		internal List<GC.DeptDayCap> getCacheData()
		{
			return TodayDeptCaps.Values.ToList<GC.DeptDayCap>();
		}
		// FINIS: sak debug quota cache bug

		[Obsolete("This may have performance implications, avoid using. Use UpdateQuota to update individual quotas")]
		internal void ResetInitialize()
		{
			log.LogInformation("==> ResetInitialize. Triggering reinitialation now.");
			LastUpdateDate = DateTime.MinValue;
			IsInitialized = false;
		}

		internal bool UpdateQuota(int DepartmentCode, int FVisit, int RVisit)
		{
			bool lbOK = false;
			log.LogInformation("Updating quotas for Dept = {0} with values F = {1}, R = {2}", DepartmentCode, FVisit, RVisit);
			if (!IsInitialized)
			{
				log.LogInformation("System not yet initialized, no need to do anything here.");
				return true;
			}

			DoStaleTest();
			GC.DeptDayCap loDayCap = TodayDeptCaps[DepartmentCode];
			if (loDayCap == null)
			{
				log.LogError("Unknown Department Code {0}. Cancelling request.", DepartmentCode);
				return false;
			}

			Monitor.Enter(loDayCap);
			try
			{
				loDayCap.CapFirst = FVisit;
				loDayCap.CapRevisit = RVisit;
				loDayCap.LastDBSet = DateTime.Now; // sak - 2021 Jan 26 - set actual update time in cache too
				lbOK = true;
				log.LogInformation("Success. Updated quotas.");
			}
			catch (System.SystemException exSE)
			{
				log.LogError(exSE, "Error getting lock for update.");
			}
			finally
			{
				Monitor.Exit(loDayCap);
			}

			return lbOK;
		}

		public GC.DeptDayCap AvailaibleCapsDepartment(int DepartmentCode)
		{
			DoStaleTest();
			return TodayDeptCaps[DepartmentCode];
		}

		/// <summary>
		/// Current status for dashboard usage.
		/// TODO: Test if returning the original container will affect locks.
		/// </summary>
		/// <returns></returns>
		public List<ML.DeptCapValues> AvailaibleCaps(CTX.GPM_DBContext db_context)
		{
			log.LogDebug("==> AvailaibleCaps");
			DoStaleTest();

			List<ML.DeptCapValues> lstTodayCaps = new List<ML.DeptCapValues>();
			DateTime ldtToday = DateTime.Today;

			// Convert code from list to dictionary
			TodayDeptCaps.Values.ToList().ForEach(vDept =>
			{
				lstTodayCaps.Add(new ML.DeptCapValues()
				{
					DateFor = ldtToday,
					DeptCode = vDept.DeptCode,
					DeptName = vDept.DeptName,
					CapFirst = vDept.CapFirst,
					CapRevisit = vDept.CapRevisit,
					UsedFirst = vDept.UsedFirst,
					UsedRevisit = vDept.UsedRevisit,
				});
			});
			return lstTodayCaps;
		}

		public bool ReturnToken(int DepartmentCode, SE.VISIT_TYPE VisitType, CTX.GPM_DBContext db_context)
		{
			log.LogDebug("==> ReturnToken");
			DoStaleTest();

			bool lbCanIssue = false;
			bool lbErrorFatal = false;

			GC.DeptDayCap loDayCap = TodayDeptCaps[DepartmentCode];
			if (loDayCap == null)
			{
				log.LogError("Unknown Department Code {0}. Cancelling request.", DepartmentCode);
				lbErrorFatal = true;
			}
			if (lbErrorFatal) { return false; }
			if (!(VisitType == SE.VISIT_TYPE.FIRST_VISIT || VisitType == SE.VISIT_TYPE.RE_VISIT))
			{ }

			int liAvl = 0;
			Monitor.Enter(loDayCap);
			try
			{
				switch (VisitType)
				{
					case SE.VISIT_TYPE.FIRST_VISIT:
						if (loDayCap.UsedFirst > 0) { loDayCap.UsedFirst--; }
						break;
					case SE.VISIT_TYPE.RE_VISIT:
						if (loDayCap.UsedRevisit > 0) { loDayCap.UsedRevisit--; }
						break;
					default:
						log.LogWarning("Unknown visit type {0}", VisitType);
						break;
				}
				lbCanIssue = true;
			}
			catch (System.SystemException exSE)
			{
				log.LogError(exSE, "Error getting lock for update.");
			}
			finally
			{
				Monitor.Exit(loDayCap);
			}

			// Log status for inspection
			log.LogInformation("Cancelled Token for: Visit Type: {1}, Dept Code: {2}, Remaining: {0}"
								, liAvl
								, VisitType
								, DepartmentCode
								);

			return lbCanIssue;
		}

		public bool CanIssueToken(int DepartmentCode, SE.VISIT_TYPE VisitType, CTX.GPM_DBContext db_context)
		{
			log.LogDebug("==> CanIssueToken");
			DoStaleTest();

			bool lbCanIssue = false;
			bool lbErrorFatal = false;

			GC.DeptDayCap loDayCap = TodayDeptCaps[DepartmentCode];
			if (loDayCap == null)
			{
				log.LogError("Unknown Department Code {0}. Cancelling request.", DepartmentCode);
				lbErrorFatal = true;
			}
			if (lbErrorFatal) { return false; }
			if (!(VisitType == SE.VISIT_TYPE.FIRST_VISIT || VisitType == SE.VISIT_TYPE.RE_VISIT))
			{
				log.LogError("Unknown Visit Type {0}. Cancelling request.", VisitType.ToString());
				lbErrorFatal = true;
			}
			if (lbErrorFatal) { return false; }

			int liAvl = 0;
			Monitor.Enter(loDayCap);
			try
			{
				switch (VisitType)
				{
					case SE.VISIT_TYPE.FIRST_VISIT:
						liAvl = loDayCap.CapFirst - loDayCap.UsedFirst;
						if (liAvl > 0) { loDayCap.UsedFirst++; }
						break;
					case SE.VISIT_TYPE.RE_VISIT:
						liAvl = loDayCap.CapRevisit - loDayCap.UsedRevisit;
						if (liAvl > 0) { loDayCap.UsedRevisit++; }
						break;
					default:
						log.LogWarning("Unknown visit type {0}", VisitType);
						break;
				}
				lbCanIssue = liAvl > 0;
			}
			catch (System.SystemException exSE)
			{
				log.LogError(exSE, "Error getting lock for update.");
			}
			finally
			{
				Monitor.Exit(loDayCap);
			}

			// Log status for inspection
			log.LogInformation("Issued Token for: Dept Code: {0}, Visit Type: {1}, Remaining: {2}"
								, DepartmentCode
								, VisitType
								, liAvl
								);

			return lbCanIssue;
		}

		/// <summary>
		/// Call this to trigger initiaization, 
		/// preferable just after startup tasks done,
		/// and before application is up.
		/// </summary>
		[Obsolete("Does not seem to be used anywhere. Remove.", true)]
		public void RunDiagnostics()
		{
			log.LogDebug("==> RunDiagnostics");
			DoStaleTest();
			// TODO: Print list - Nice to have, no functional benefit.
			// Dept CapF UsedF AVlF CapR UsedR AvlR
		}


		private void Initialize()
		{
			log.LogDebug("==> Initialize");
			if (IsInitialized) { return; }

			bool lbErrorFatal = false;
			DateTime ldtToday = DateTime.Today;

			// Create custom copy of context
			CTX.GPM_DBContext lDbContext = new CTX.GPM_DBContext(appconfig.DBConnectionStringRW);

			//#		Get list of departments
			List<CTX.DdItems> lstDeptList = null;
			try
			{
				lstDeptList = lDbContext.DdItems
								.Where(f
									=> f.Status == (int)SE.RECORD_STATUS.ACTIVE
									&& f.DdiCatgId == (int)SE.DDCategories.DEPARTMENTS
									)
								.ToList()
								;
			}
			catch (System.Exception exSE)
			{
				lbErrorFatal = true;
				log.LogCritical(exSE, "FATAL: Error getting ");
			}
			if (lstDeptList == null || lstDeptList.Count == 0)
			{
				// We have a problem
				lbErrorFatal = true;
				log.LogCritical("FATAL: ");
			}
			if (lbErrorFatal) { return; }


			//#		Get capacities for the day
			List<CTX.Quotas> lstQuotas = null;
			bool lbLoadFromDefaults = false;
			try
			{
				lstQuotas = lDbContext.Quotas
							.Where(f
								=> f.Status == (int)SE.RECORD_STATUS.ACTIVE
								&& f.Date == ldtToday
								)
							.ToList()
							;
			}
			catch (System.Exception exSE)
			{
				lbErrorFatal = true;
				log.LogCritical(exSE, "FATAL: Error getting ");
			}
			if (lbErrorFatal) { return; }
			if (lstQuotas == null || lstQuotas.Count == 0)
			{
				// No Capacities set for today, load from defaults.
				lbLoadFromDefaults = true;
			}


			//#		Get from defaults, if missing
			if (lbLoadFromDefaults)
			{
				lstQuotas = null;
				try
				{
					lstQuotas = lDbContext.Quotas
									.Where(f
										=> f.Status == (int)SE.RECORD_STATUS.ACTIVE
										&& f.Date == DateTime.MinValue
										)
									.ToList()
									;
				}
				catch (System.Exception exSE)
				{
					lbErrorFatal = true;
					log.LogCritical(exSE, "FATAL: Error getting ");
				}
				if (lstQuotas == null || lstQuotas.Count == 0)
				{
					// No Defaults, cannot proceed.
					lbErrorFatal = true;



					//################### BEGIN - Test Data
					// TODO: Remove when data seeding is done
					// fill random values for testing
					DateTime lvDefaultDate = DateTime.MinValue;
					long lvDefaultDateTicks = lvDefaultDate.Ticks;
					// Lambda cannot do walk through in debugger. (VSCode v1.37.1)
					lstDeptList.ForEach(vDept =>
					{
						lstQuotas.Add(new CTX.Quotas()
						{
							Department = vDept.DdiCode,
							DateInt = lvDefaultDateTicks,
							Date = lvDefaultDate,
							CapNewVisit = new Random().Next(2, 8),  // keep low values to indicate generated data
							CapRevisit = new Random().Next(2, 8),   // keep low values to indicate generated data
							Status = 1,
						});
					});
					lbErrorFatal = false;
					//################### END - Test Data



				}
			}
			if (lbErrorFatal) { return; }


			//#		Create today's quote entries
			// TODO: Use lock to ensure thread safety.
			if (lbLoadFromDefaults)
			{
				DateTime ldtNow = DateTime.Now;
				long llToday = ldtToday.Ticks;
				// Fill Entries
				foreach (CTX.DdItems vDept in lstDeptList)
				{
					log.LogInformation("Dept: {0}. Loading default Caps", vDept.DdiCode);
					CTX.Quotas vQuota = lstQuotas.FirstOrDefault(d => d.Department == vDept.DdiCode);
					if (vQuota == null)
					{
						log.LogWarning("Missing default quota number, setting to 0");
					}
					log.LogInformation("Dept: {0}[{1}], First Cap: {2}, Revisit Cap: {3}"
										, vDept.DdiCode
										, vDept.DdiDispText
										, vQuota?.CapNewVisit ?? 0
										, vQuota?.CapRevisit ?? 0
										);
					lDbContext.Quotas.Add(
						new CTX.Quotas()
						{
							Department = vDept.DdiCode,
							DateInt = llToday,
							Date = ldtToday,
							CapNewVisit = vQuota?.CapNewVisit ?? 0,
							CapRevisit = vQuota?.CapRevisit ?? 0,
							Status = (int)SE.QUOTA_STATUS.ACTIVE,
							AddBy = SC.KEYS.SYSTEM_USER_ID_VAL,
							AddOn = ldtNow,
							EditBy = SC.KEYS.SYSTEM_USER_ID_VAL,
							EditOn = ldtNow,
						}
					);
				}
				// Save entries
				try
				{
					int liCount = lDbContext.SaveChanges();
					// log result
					log.LogInformation("Wrote {0} entries for {1} Departments"
										, liCount
										, lstDeptList.Count
										);
					// reload entries 
					lstQuotas = lDbContext.Quotas
									.Where(f
										=> f.Status == (int)SE.RECORD_STATUS.ACTIVE
										&& f.Date == ldtToday
										)
									.ToList()
									;
					lbLoadFromDefaults = false;
				}
				catch (System.Exception exSE)
				{
					lbErrorFatal = true;
					log.LogCritical(exSE, "FATAL: Error saving Day Quotas to database.");
				}
			}   // if (lbLoadFromDefaults)
			if (lbErrorFatal) { return; }


			//#		Get Used Capacities from DB
			// Use Model after debugging. => gpm_web.Models.DeptCapValues
			// UsedCaps = List<< anonymous type: int Dept, int Type, int Total, int UsedFirst, int UsedRevisit>>
			List<ML.DeptCapValues> lstUsedCaps = lDbContext.GatePasses
							.Where(f => f.IssueDate >= ldtToday)
							.Where(f
								=> f.PassType == (int)SE.GATE_PASS_TYPE.PATIENT_NEW
								|| f.PassType == (int)SE.GATE_PASS_TYPE.PATIENT_REVISIT
							)
							.GroupBy(g => new { g.Department, g.PassType })
							.Select(gcols => new ML.DeptCapValues()
							{
								DeptCode = gcols.Key.Department,
								// Type = gcols.Key.PassType,
								TotalUsed = gcols.Count(),
								UsedFirst = gcols.Count(t => t.PassType == (int)SE.GATE_PASS_TYPE.PATIENT_NEW),
								UsedRevisit = gcols.Count(t => t.PassType == (int)SE.GATE_PASS_TYPE.PATIENT_REVISIT),
							})
							.ToList()
							;

			//#		initialize register
			// BEGIN: sak debug quota cache bug
			if (IsInitialized) { return; }  // if already updated by another thread.
			// FINIS: sak debug quota cache bug
			Monitor.Enter(TodayDeptCaps);
			try
			{
				lstDeptList.ForEach(vDept =>
				{
					CTX.Quotas vQuota = lstQuotas.FirstOrDefault(d => d.Department == vDept.DdiCode);
					var vUsed = lstUsedCaps.FirstOrDefault(d => d.DeptCode == vDept.DdiCode);

					if (vQuota == null)
					{
						log.LogWarning("Missing default quota number, setting to 0");
					}
					if (vUsed != null)
					{
						log.LogInformation("Dept: {0}, Used Forst: {1}, Used Revisit: {2} => Updating usage register from DB usage."
											, vUsed.DeptCode
											, vUsed.UsedFirst
											, vUsed.UsedRevisit
											);
					}
					// Set value directly, entry will be created if not present, convenient for updates
					TodayDeptCaps[vDept.DdiCode] = new GC.DeptDayCap()
					{
						DeptCode = vDept.DdiCode,
						DeptTag = vDept.DdiCodeTxt,
						DeptName = vDept.DdiDispText,
						CapFirst = vQuota?.CapNewVisit ?? 0,
						CapRevisit = vQuota?.CapRevisit ?? 0,
						UsedFirst = vUsed?.UsedFirst ?? 0,
						UsedRevisit = vUsed?.UsedRevisit ?? 0,
					};
				});
				//#		update state flags - IsInitialized, LastUpdateDate
				IsInitialized = true;
				LastUpdateDate = ldtToday;
			}
			catch (System.SystemException exSE)
			{
				lbErrorFatal = true;
				log.LogError(exSE, "Error initalizing register.");
			}
			finally
			{
				Monitor.Exit(TodayDeptCaps);
			}

			log.LogInformation("bye");

		}

		/// <summary>
		/// NOTE: Manipulating global props directl for speed.
		/// </summary>
		private void DoStaleTest()
		{
			log.LogDebug("==> DoStaleTest");
			if (!IsInitialized || LastUpdateDate != DateTime.Today)
			{
				IsInitialized = false;
				Initialize();
			}
		}


	}
}
