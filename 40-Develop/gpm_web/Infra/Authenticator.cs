using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;


namespace gpm_web.Infra{


	/// <summary>
	/// Implementation pending resolution of DI of DB Context into this container.
	/// </summary>
	public class Authenticator{
		public Authenticator(IHttpContextAccessor httpcontextaccessor, ILogger<DayCapacityCounters> logger, GC.IAppSettingContainer App_Config)
		{
			Console.WriteLine("==> Authenticator:CTOR");

			Console.WriteLine("Inspect the http context");
		}

		/// <summary>
		/// Test method to ensure constructor is fired.
		/// </summary>
		public void InvokeAuthenticator(){
			Console.WriteLine("==> InvokeAuthenticator");
		}


		public (int Status, Models.UserPayload UserData, ClaimsIdentity UserIdentity) AuthenticateUser(
			string UserID, string UserPassword
		){
			return (500, null, null);
		}

		public Models.UserPayload ValidateUser(ClaimsPrincipal pUser)
		{
			return null;
		}

	}

}
