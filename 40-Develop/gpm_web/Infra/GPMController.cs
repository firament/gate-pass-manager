using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using gpm_web.Models;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Infra
{
	public class GPMController : Controller
	{

		// Containers and Services from Dependency Injection
		internal readonly ILogger<GPMController> log;
		internal readonly GC.IAppSettingContainer appconfig;
		internal readonly gpm.Efc.dbsets.GPM_DBContext dbcontext;
		internal UserPayload UserData;
		internal bool IsAPI = false;
		const string COOKIE_KEY = "d28226e0";


		public GPMController()
		: base()
		{
			Console.WriteLine("WARNING: This should not be used, logger ill not be availaible");
		}
		public GPMController(ILogger<GPMController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context)
		: base()
		{
			log = logger;
			dbcontext = db_context;
			appconfig = App_Config;
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}


		//
		// Summary:
		//     Called after the action method is invoked.
		//
		// Parameters:
		//   context:
		//     The action executed context.
		public override void OnActionExecuting(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext context)
		{

			// Debugging Code
			if (log == null)
			{
				Console.WriteLine("ERROR: log == null");
			}
			UserData = ValidateUser(context);
			if (UserData == null)
			{
				// return 401 if api
				if (IsAPI && !context.ActionDescriptor.EndpointMetadata.OfType<AllowAnonymousAttribute>().Any())
				{
					context.Result = new UnauthorizedResult();
				}

				// signout user if logged in
				if (context.HttpContext.User.Identity.IsAuthenticated)
				{
					log.LogWarning("TBD. Turn off authentication for the user. Session is compromised.");
					// signout the user
				}
			}

		}

		public override void OnActionExecuted(Microsoft.AspNetCore.Mvc.Filters.ActionExecutedContext context)
		{
			log.LogDebug("Setting NO-Cache header for URL {0}", context.ActionDescriptor.DisplayName);
			try
			{
				context.HttpContext.Response.Headers.Add("Cache-Control", "no-cache, no-store");
			}
			catch (System.Exception)
			{
				log.LogInformation("ERROR Setting NO-Cache header for URL {0}", context.ActionDescriptor.DisplayName);

			}
		}


		public bool CookieOK(Microsoft.AspNetCore.Http.IRequestCookieCollection QCookies, string Tag = "--NA--")
		{
			if (IsAPI) { return false; }
			string lsCookie;
			bool lbOK = QCookies.ContainsKey(COOKIE_KEY);

			if (lbOK)
			{
				lsCookie = QCookies[COOKIE_KEY];
				log.LogDebug("COOKIE: YES {0} = {1}, Tag = {2}", COOKIE_KEY, lsCookie, Tag);
			}
			else
			{
				lsCookie = Guid.NewGuid().ToString();
				log.LogDebug("COOKIE: NO_ {0} = {1}, Tag = {2}", COOKIE_KEY, lsCookie, Tag);
				HttpContext.Response.Cookies.Append(COOKIE_KEY, lsCookie);
			}
			return lbOK;
		}

		public bool SessionOK(System.Security.Principal.IIdentity UserID, string Tag = "--NA--")
		{
			if (IsAPI) { return false; }
			bool lbOK = UserID.IsAuthenticated;
			log.LogDebug("SESSION: Authenticated = {0}, Tag = {1}", lbOK, Tag);
			return lbOK;
		}

		internal void PostError(string ErrorMesg)
		{
			if (!string.IsNullOrWhiteSpace(ErrorMesg))
			{
				ViewData[Consts.KEYS.ERROR_VIEWBAG] = ErrorMesg;
			}
		}

		/// <summary>
		/// Validate user against local copy, for intrusions. 
		/// TODO: Move to gpm_web.Infra.Authenticator
		/// 	Once Di for transient service or to a static class is resolved.
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		private Models.UserPayload ValidateUser(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext context)
		{
			string lsToken = null;
			Guid loTrackID = Guid.NewGuid();
			bool IsNewSession = true;  // = !context.HttpContext.Request.Headers.ContainsKey(appconfig.AuthKeyName) && !HttpContext.Request.Cookies.ContainsKey(COOKIE_KEY);

			// Get token from session source
			if (IsAPI)
			{
				if (context.HttpContext.Request.Headers.ContainsKey(appconfig.AuthKeyName))
				{
					IsNewSession = false;
					lsToken = context.HttpContext.Request.Headers[appconfig.AuthKeyName];
				}
			}
			else
			{
				IsNewSession = !context.HttpContext.User.Identity.IsAuthenticated;
				if (!IsNewSession)
				{
					lsToken = context.HttpContext.User.Claims.First(c => c.Type == ClaimTypes.SerialNumber)?.Value ?? string.Empty;
					IsNewSession = string.IsNullOrWhiteSpace(lsToken);
				}
			}
			// Process Token and TrackID
			if (IsNewSession)
			{
				lsToken = loTrackID.ToString("N");
			}
			else
			{
				loTrackID = Guid.Parse(lsToken);
			}
			Trace.CorrelationManager.ActivityId = loTrackID;
			log.LogDebug($"IsNewSession = {IsNewSession}, IsAPI = {IsAPI} <- Junk entry to make csharp(CS0219) warnings go away");

			Models.UserPayload loUPL = new UserPayload();
			if (string.IsNullOrWhiteSpace(lsToken))
			{
				log.LogError("Session Token missing, review earlier logs.");
				return null;
			}

			//#	get session entry
			int liCount = dbcontext.MaintSessions.Count(e => e.Token == lsToken);
			if (liCount != 1)
			{
				log.LogError("Session lookup Failed. Expected 1 record, got {0} records.", liCount);
				return null;
			}
			CTX.MaintSessions loSenEntry = dbcontext.MaintSessions.First(e => e.Token == lsToken);

			//#	create payload container
			string lsJson = loSenEntry.DeviceSig;
			loUPL = GC.Utils.FromJSON(lsJson, loUPL.GetType()) as Models.UserPayload;
			if (loUPL == null)
			{
				log.LogError("Failed to parse user payload, see prior log entries for details");
				return null;
			}

			//#	verify validity
			// Check status and then
			if (loUPL.Status != loSenEntry.Status && loSenEntry.Status == (int)Enums.SESSION_STATUS.ACTIVE)
			{
				log.LogWarning("Session Status not Valid. Payload Status = {0}, Session Entry Status = {1}."
								, ((Enums.SESSION_STATUS)loUPL.Status).ToString()
								, ((Enums.SESSION_STATUS)loSenEntry.Status).ToString()
								);
				return null;
			}

			if (
				   loUPL.Token != loSenEntry.Token
				|| loUPL.Userid != loSenEntry.Userid
				|| loUPL.Role != loSenEntry.Role
			)
			{
				// Flag session status to BROKEN.
				loUPL.Status = (int)Enums.SESSION_STATUS.BROKEN;
				log.LogWarning("Mismatch in one or more of Token, Userid, Role, IssueTime, LastUsed. Failing session validation");
				log.LogWarning("Run Forensics. This indicates possible breakin attempt!");
				return null;
			}

			//#	date session entry
			DateTime ldtNow = DateTime.Now;
			loUPL.LastUsed = ldtNow;
			lsJson = GC.Utils.ToJSON(loUPL, false);
			if (string.IsNullOrEmpty(lsJson))
			{
				log.LogError("Failed to serialize user payload, see prior log entries for details");
				return null;
			}
			loSenEntry.LastUsed = ldtNow;
			loSenEntry.DeviceSig = lsJson;

			try
			{
				liCount = dbcontext.SaveChanges();
			}
			catch (System.Exception exS)
			{
				log.LogError(exS, "Database update failed when updating session entries");
				return null;
			}

			//# All is well, return the payload container
			return loUPL;
		}

		/*
			INTERESTING OVERRIDES TO LOOK INTO

		// look into
		public override SignOutResult SignOut(params string[] authenticationSchemes){
			return null;
		}

		// look into
		public override SignInResult SignIn(System.Security.Claims.ClaimsPrincipal principal, Microsoft.AspNetCore.Authentication.AuthenticationProperties properties, string authenticationScheme){
			return null;
		}

		// look into
		public override SignInResult SignIn(System.Security.Claims.ClaimsPrincipal principal, string authenticationScheme){
			return null;
		}

		*/


	}
}
