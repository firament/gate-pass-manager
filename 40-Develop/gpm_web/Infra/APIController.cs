using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers{

	/// <summary>
	/// Base class for all API controllers
	/// </summary>
	[Route("api/[controller]")]
	[Produces("application/json")]
	[ApiController]
	public partial class APIController: ControllerBase{

		// Containers and Services from Dependency Injection
		internal readonly ILogger<APIController> log;
		internal readonly GC.IAppSettingContainer appconfig;
		internal readonly gpm.Efc.dbsets.GPM_DBContext dbcontext;
		internal ML.UserPayload UserData = null;
		
		private string AuthKeyName { get; set; }

		public APIController(ILogger<APIController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context)
		: base()
		{
			log = logger;
			dbcontext = db_context;
			appconfig = App_Config;
			AuthKeyName = appconfig.AuthKeyName;
		}

		internal bool ValidateRequestor(){
			// set UserData if valid.
			bool lbHasKey = Request.Headers.ContainsKey(AuthKeyName);
			string lsToken = lbHasKey ? Request.Headers[AuthKeyName][0] : string.Empty;

			log.LogDebug("API KEY => Exists? = {0}, Value = {1}", lbHasKey, lsToken);
			UserData = null;
			return false;
		}

		// Dump request headers for inspection.
		internal string RequestHeaderList(Microsoft.AspNetCore.Http.IHeaderDictionary pReqHeaders)
		{
			StringBuilder lbResult = new StringBuilder();
			lbResult.AppendLine("Request Headers:");
			lbResult.AppendLine("Count = " + pReqHeaders.Count);
			foreach (string vKey in pReqHeaders.Keys)
			{
				lbResult.AppendLine(vKey + " - " + pReqHeaders[vKey]);
			}
			return lbResult.ToString();
		}




	}
}
