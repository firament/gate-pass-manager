using System;
using System.Collections.Generic;

namespace gpm_web.Infra
{
	public class GPMLookups
	{
		private Dictionary<int, string> m_RoleList;
		public Dictionary<int, string> RoleList
		{
			get
			{
				if (m_RoleList == null) m_RoleList = BuildRoleList();
				return m_RoleList;
			}
		}
		public string getRoleName(int RoleCode){
			string lsName = m_RoleList[RoleCode];
			if (string.IsNullOrWhiteSpace(lsName))
			{
				lsName = "";
			}
			return lsName;
		}



		// Bootstrap Methods

		/// <summary>
		/// Make all system roles mappable from flags to Text
		/// These values should be mapped to table 
		///     DD_ITEMS where DDI_CATG_ID == 4
		///         DDI_CODE => Key
		///         DDI_DISP_TEXT => Value
		/// </summary>
		/// <returns>Populated dictionary that is ready for use.</returns>
		private static Dictionary<int, string> BuildRoleList()
		{
			Dictionary<int, string> ldsRoles = new Dictionary<int, string>();
			ldsRoles.Add(0, "Not_Set");
			ldsRoles.Add(1, "User");
			ldsRoles.Add(2, "Administrator");
			ldsRoles.Add(3, "Data-Analyst");
			ldsRoles.Add(4, "Operator");
			ldsRoles.Add(5, "Maintainer");
			return ldsRoles;
		}
	}
}
