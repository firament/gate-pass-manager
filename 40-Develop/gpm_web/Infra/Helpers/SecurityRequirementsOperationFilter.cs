using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Logging;
using GC = gpm.Common;

namespace gpm_web.Infra.Helpers
{

	public class SecurityRequirementsOperationFilter : IOperationFilter
	{
		internal readonly ILogger<SecurityRequirementsOperationFilter> log;
		internal readonly GC.IAppSettingContainer appconfig;

		public SecurityRequirementsOperationFilter(ILogger<SecurityRequirementsOperationFilter> logger, GC.IAppSettingContainer App_Config)
		{
			log = logger;
			appconfig = App_Config;
		}
		public void Apply(Operation operation, OperationFilterContext context)
		{
			log.LogDebug("==> SecurityRequirementsOperationFilter.Apply(), OperationId = {0}", operation.OperationId);
			var loAttrMethod = context.MethodInfo.GetCustomAttributes(true);

			if (!context.MethodInfo.DeclaringType.GetCustomAttributes(true)
				.Union(context.MethodInfo.GetCustomAttributes(true))
				.OfType<AllowAnonymousAttribute>().Any())
			{
				log.LogInformation("Method 'OperationId' -> Skipping Authorization.");
				operation.Security = new List<IDictionary<string, IEnumerable<string>>>
				{
					new Dictionary<string, IEnumerable<string>>
					{
						{appconfig.AuthKeyName, Array.Empty<string>()}
					}
				};
			}
		}
	}

}
