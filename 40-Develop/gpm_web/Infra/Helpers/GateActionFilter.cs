using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Logging;
using GC = gpm.Common;

namespace gpm_web.Infra.Helpers
{
	public class GateActionFilter : IActionFilter
	{
		private readonly ILogger<SecurityRequirementsOperationFilter> log;
		private readonly GC.IAppSettingContainer appconfig;

		public GateActionFilter(ILogger<SecurityRequirementsOperationFilter> logger, GC.IAppSettingContainer App_Config)
		{
			log = logger;
			appconfig = App_Config;
		}

		public void OnActionExecuting(ActionExecutingContext context)
		{
			log.LogInformation("API-AUTH-TEST : : GateActionFilter ==> OnActionExecuting -> Route => ", context.ActionDescriptor.DisplayName);
		}

		public void OnActionExecuted(ActionExecutedContext context)
		{
			log.LogInformation("API-AUTH-TEST : : GateActionFilter ==> OnActionExecuted  -> Route => ", context.ActionDescriptor.DisplayName);
		}

	}
}
