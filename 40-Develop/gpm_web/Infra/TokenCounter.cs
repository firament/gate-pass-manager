using System;
using System.Linq;
using System.Threading;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;


namespace gpm_web.Infra
{
	public class TokenCounter
	{
		// Containers and Services from Dependency Injection
		internal readonly ILogger<TokenCounter> log;
		internal readonly GC.IAppSettingContainer appconfig;

		internal gpm.Efc.dbsets.GPM_DBContext dbcontext;
		internal bool IsInitialized = false;
		internal DateTime LastUpdated = DateTime.MinValue;
		internal int TokenCount = 0;
		System.Object ThreadLockObj = new Object();

		public TokenCounter(ILogger<TokenCounter> logger, GC.IAppSettingContainer App_Config)
		{
			log = logger;
			appconfig = App_Config;
			log.LogDebug("==> TokenCounter:CTOR");
		}

		[Obsolete("If cancelled, can cause duplicate token numbers. Write to log of unused token number", true)]
		public int CancelTokenNumbers(int token_count){
			log.LogDebug("==> CancelTokenNumbers");
			if (token_count < 1)
			{
				log.LogWarning("Asked for '0' tokens. What??");
				return int.MinValue;
			}
			DoStaleTest();
			int liTokenID = 0;
			log.LogInformation("Preparing to cancel {0} tokens.", token_count);
			Monitor.Enter(ThreadLockObj);
			try
			{
				TokenCount -= token_count;
				liTokenID = TokenCount;
			}
			catch (System.SystemException exSE)
			{
				log.LogError(exSE, "Error getting updated lock");
			}
			finally
			{
				Monitor.Exit(ThreadLockObj);
			}
			log.LogInformation("Cancelled {0} tokens, reset to token #{1}", token_count, liTokenID);
			return liTokenID;
		}

		public int GetTokenNumbers(int token_count)
		{
			log.LogDebug("==> GetTokenCouner");
			if (token_count < 1)
			{
				log.LogWarning("Asked for '0' tokens. What??");
				return int.MinValue;
			}

			DoStaleTest();
			int liTokenID = 0;
			log.LogInformation("Preparing to issue {0} tokens.", token_count);
			Monitor.Enter(ThreadLockObj);
			try
			{
				liTokenID = TokenCount;
				TokenCount += token_count;
			}
			catch (System.SystemException exSE)
			{
				log.LogError(exSE, "Error getting updated lock");
			}
			finally
			{
				Monitor.Exit(ThreadLockObj);
			}
			log.LogInformation("Issued {0} tokens, starting with token #{1}", token_count, liTokenID);
			return liTokenID;
		}

		/// <summary>
		/// Initialize the counter before using it
		/// </summary>
		private void Initialize()
		{
			log.LogDebug("==> Initialize");
			if (IsInitialized)
			{
				return;
			}

			DateTime liToday = DateTime.Today;
			int liCurrCount = 0;

			dbcontext = new CTX.GPM_DBContext(appconfig.DBConnectionStringRO);

			Monitor.Enter(ThreadLockObj);
			try
			{
				// get last count from dB - for today
				liCurrCount = dbcontext.GatePasses.Count(gp => gp.IssueDate >= liToday) + 1;

				var LastToken = dbcontext.GatePasses
								.Where(gp => gp.IssueDate >= liToday)
								.GroupBy(g => new { g.PassNum })
								.Select(gcols => new
								{
									TokenValue = gcols.Max(o => o.PassNum)
								})
								.OrderByDescending(c => c.TokenValue)
								.FirstOrDefault()
								;
				liCurrCount = LastToken?.TokenValue ?? 0;

				// set to current
				TokenCount = liCurrCount + 1;
				LastUpdated = liToday;
				IsInitialized = true;
			}
			catch (System.SystemException exSE)
			{
				log.LogError(exSE, "Error getting updated lock");
			}
			finally
			{
				Monitor.Exit(ThreadLockObj);
			}
			// Inspect counter
			log.LogInformation("{0}: Initialize Token counter to {1} for {2:ddd yyyy-MMM-dd HH:mm:ss}", IsInitialized, TokenCount, liToday);

		}

		/// <summary>
		/// NOTE: Manipulating global props directl for speed.
		/// </summary>
		private void DoStaleTest()
		{
			IsInitialized = (
				LastUpdated == DateTime.Today
			);
			if (!IsInitialized)
			{
				Initialize();
			}
		}



	}
}
