using System;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Models{
	[Obsolete("Use DeptCapValues instead", true)]
	public class DeptCapAvailaible{
		public DeptCapAvailaible(){}
		public DateTime CapDate { get; set; }
		public int DeptCode { get; set; }
		public string DeptName { get; set; }
		public int CapAvlFirst { get; set; }
		public int CapAvlRevisit { get; set; }
	}
}
