using System;
using System.Runtime.Serialization;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using stCode = gpm.Common.Enums.ErrorCode;


namespace gpm_web.Models
{
	// Clone of gpm.Efc.dbsets.MaintSessions
	public class UserPayload
	{
		public long Msid { get; set; }
		public string Token { get; set; }
		public string DeviceSig { get; set; }
		public int Userid { get; set; }
		public string LoginID { get; set; }
		public string DisplayName { get; set; }
		public string Role { get; set; }
		public DateTimeOffset IssueTime { get; set; }
		public DateTimeOffset? AuthTime { get; set; }
		public DateTimeOffset LastUsed { get; set; }
		public int Status { get; set; }


		/* Constructors */
		public UserPayload() { }
		public UserPayload(CTX.MaintSessions SessionEntry)
		{
			Msid = SessionEntry.Msid;
			Token = SessionEntry.Token;
			DeviceSig = SessionEntry.DeviceSig;
			Userid = SessionEntry.Userid;
			LoginID = "";
			DisplayName = "";
			Role = SessionEntry.Role.ToUpper();
			IssueTime = SessionEntry.IssueTime;
			AuthTime = SessionEntry.AuthTime;
			LastUsed = SessionEntry.LastUsed;
			Status = SessionEntry.Status;
		}
	}
}
