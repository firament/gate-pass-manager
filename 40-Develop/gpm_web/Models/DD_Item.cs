using System;

namespace gpm_web.Models{
	public class DD_Item{
		public int CatgID { get; set; }
		public int Value { get; set; }
		public int DisplaySeq { get; set; }
		public string Text { get; set; }
		public string Label { get; set; }
		public bool Active { get; set; }
	}
}
