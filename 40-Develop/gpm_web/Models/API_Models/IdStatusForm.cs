using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using stCode = gpm.Common.Enums.ErrorCode;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm_web.API_Models
{
	public class IdStatusForm
	{
		/// <summary>
		/// GatePassId of the gatepass that is to be updated
		/// </summary>
		/// <value></value>
		[Required]
		[Range(1, uint.MaxValue)]
		public int Id { get; set; }

		/// <summary>
		/// Status to be applied to the gatepass
		/// </summary>
		/// <value>Valid value corresponding to active values of SE.GATE_PASS_STATUS</value>
		[EnumDataType(typeof(SE.GATE_PASS_STATUS))]
		public SE.GATE_PASS_STATUS Status { get; set; }
	}
}
