using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using stCode = gpm.Common.Enums.ErrorCode;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm_web.API_Models
{
	public class EnumForm
	{
		[EnumDataType(typeof(SE.DDCategories))]
		public SE.DDCategories ID { get; set; }
		public bool ActiveOnly { get; set; }
	}
}
