using System;
using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using stCode = gpm.Common.Enums.ErrorCode;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm_web.API_Models
{
	/// <summary>
	/// Convenienve format for API Usage
	/// </summary>
	public class GatePassForm
	{
		[Required]
		[Range(0, uint.MaxValue)]
		public int GatePassId { get; set; }

		[Required]
		[EnumDataType(typeof(SE.GATE_PASS_TYPE))]
		public SE.GATE_PASS_TYPE PassType { get; set; }

		/// <summary>
		/// Required for Update and for Search
		/// </summary>
		/// <value></value>
		[Range(0, uint.MaxValue)]
		public int PassNum { get; set; }

		public DateTime IssueDate { get; set; }

		public int PartyCount { get; set; }

		/// <summary>
		/// Required for updates.
		/// </summary>
		/// <value></value>
		[Range(1, uint.MaxValue)]
		public int Department { get; set; }

		[EnumDataType(typeof(SE.VISIT_TYPE))]
		public SE.VISIT_TYPE VisitType { get; set; }

		public string HospitalNum { get; set; }

		[MaxLength(10)]
		public string MobileNum { get; set; }

		public bool SupplyDelivery { get; set; }

		[StringLength(60)]
		public string PersonMeeting { get; set; }

		public bool HasResources { get; set; }

		[StringLength(20)]
		public string WheelchairNum { get; set; }

		public bool HasVehicle { get; set; }

		[EnumDataType(typeof(SE.VEHICLE_TYPE))]
		public SE.VEHICLE_TYPE VehicleType { get; set; }

		[StringLength(24)]
		public string VehiclePlateNum { get; set; }

		[StringLength(255)]
		public string Note { get; set; }

		public DateTimeOffset TimeIn { get; set; }

		public DateTimeOffset? TimeOut { get; set; }

		public DateTimeOffset? StepOut { get; set; }

		[EnumDataType(typeof(SE.GATE_PASS_STATUS))]
		public SE.GATE_PASS_STATUS Status { get; set; }

		/// <summary>
		/// Convert from gpm_web.API_Models.GatePassForm to gpm.Efc.dbsets.GatePasses format
		/// </summary>
		/// <returns>shallow copy of this instance in gpm.Efc.dbsets.GatePasses format</returns>
		public CTX.GatePasses ToModel() => new CTX.GatePasses()
		{
			HasResources = this.HasResources ? (int)SE.YES_NO.YES : (int)SE.YES_NO.NO,
			HasVehicle = this.HasVehicle ? (int)SE.YES_NO.YES : (int)SE.YES_NO.NO,
			SupplyDelivery = this.SupplyDelivery ? (int)SE.YES_NO.YES : (int)SE.YES_NO.NO,
			IssueDate = this.IssueDate,
			TimeIn = this.TimeIn,
			StepOut = this.StepOut,
			TimeOut = this.TimeOut,
			Department = this.Department,
			GatePassId = this.GatePassId,
			PartyCount = this.PartyCount,
			PassNum = this.PassNum,
			Status = (int)this.Status,
			PassType = (int)this.PassType,
			VehicleType = (int)this.VehicleType,
			VisitType = (int)this.VisitType,
			HospitalNum = this.HospitalNum,
			MobileNum = this.MobileNum,
			Note = this.Note,
			PersonMeeting = this.PersonMeeting,
			VehiclePlateNum = this.VehiclePlateNum,
			WheelchairNum = this.WheelchairNum,
		};

	}
}
