using System;
using System.Collections.Generic;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using stCode = gpm.Common.Enums.ErrorCode;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gpm_web.API_Models
{
	public class DD_Catgs_Model
	{
		public int CatgID { get; set; }
		public string CatgName { get; set; }
		public string CatgDesc { get; set; }
		public bool Active { get; set; }

		public List<ML.DD_Item> Items { get; set; } = new List<ML.DD_Item>();
	}
}
