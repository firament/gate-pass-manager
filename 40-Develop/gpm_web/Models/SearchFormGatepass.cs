using System;
using gpm_web.Enums;

namespace gpm_web.Models
{
	public class SearchFormGatepass
	{

		public int PassNum { get; set; }

		public DateTime IssueDate { get; set; }

		public int Department { get; set; }

		public int Status { get; set; }

		public int PassType { get; set; }

		public int PartyCount { get; set; }

		public int VehicleType { get; set; }

		public int HasResources { get; set; }

		public int PageSize { get; set; }

		public gpm_web.Enums.SearchType SearchMode { get; set; }

	}
}
