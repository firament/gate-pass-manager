using System;

namespace gpm_web.Models
{
	/// <summary>
	/// Same container used for both Capacity and for Used
	/// Will need 1 run each for capacities and one for usage.
	/// </summary>
	public class DeptCapValues
	{
		public DateTime DateFor { get; set; }

		public int DeptCode { get; set; }
		public string DeptName { get; set; }
		
		public int Type { get; set; }	// Not really needed, remove after debugging.

		public int TotalCap { get; set; }
		public int CapFirst { get; set; }
		public int CapRevisit { get; set; }

		public int TotalUsed { get; set; }
		public int UsedFirst { get; set; }
		public int UsedRevisit { get; set; }
	}
}
