﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using System.Reflection;
using GC = gpm.Common;
using EFC = gpm.Efc;


namespace gpm_web
{
	public class Startup
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
		private GC.IAppSettingContainer appconfig;

		public Startup(IConfiguration configuration)
		{
			log.Info("==> Startup:CTOR");
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			log.Info("==> ConfigureServices");

			#region Application Settings and Configuration - from DB
			//
			// Load global application settings, for use by application
			//
			log.Info("AppSettings: Initializing from appSettings.json");
			string lsConnStrRW = Configuration.GetSection("AppSettings:DBConnectionStringRW").Value;
			string lsConnStrRO = Configuration.GetSection("AppSettings:DBConnectionStringRO").Value;
			string lsAppCode = Configuration.GetSection("AppSettings:AppCode").Value;
			string lsAppSettingVersion = Configuration.GetSection("AppSettings:AppSettingVersion").Value;
			int liAppSettingVersion = 0;
			if (!int.TryParse(lsAppSettingVersion, out liAppSettingVersion))
			{
				log.Fatal("Version number not in proper format");
				// throw new ArgumentException("Version number not in proper format");
			}

			log.Info("AppSettingContainer: Fetch from database and hook into DI.");
			GC.IAppSettingContainer loAppSetting;
			loAppSetting = EFC.DBFuncs.DBFunctions.GetAppSettings(lsConnStrRO, lsAppCode, liAppSettingVersion);
			if (loAppSetting == null)
			{
				log.Fatal("Unable to get app settings.");
				loAppSetting = new GC.AppSettingContainer();
			}
			loAppSetting.DBConnectionStringRO = lsConnStrRO;
			loAppSetting.DBConnectionStringRW = lsConnStrRW;
			services.AddSingleton<gpm.Common.IAppSettingContainer>(loAppSetting);
			appconfig = loAppSetting;


			services.AddSingleton<gpm_web.Infra.TokenCounter>();
			services.AddSingleton<gpm_web.Infra.DayCapacityCounters>();
			services.AddSingleton<gpm_web.Lookups.DD_Lookup>();
			services.AddTransient<gpm_web.Infra.GatePassValidator>();

			// Refactoring to enable API endpoints
			services.AddTransient<gpm_web.BizObjs.GatePassBZ>();
			services.AddTransient<gpm_web.BizObjs.SessionBZ>();

			// Application Cookies options
			log.Info("Cookies: Setting application cookie options.");
			services
				.Configure<CookiePolicyOptions>(options =>
					{
						// This lambda determines whether user consent for non-essential cookies is needed for a given request.
						options.CheckConsentNeeded = context => true;
						options.MinimumSameSitePolicy = SameSiteMode.None;
					});

			#endregion Application Settings and Configuration - from DB
			/******************************************************************/
			/*----------------------------------------------------------------*/
			/*================================================================*/
			/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
			/*________________________________________________________________*/


			#region DB Contexts
			//
			// Activate Database connectivity
			//
			log.Info("DbContext: Hook into DI");
			services.AddDbContext<gpm.Efc.dbsets.GPM_DBContext>(options =>
				options.UseMySql(lsConnStrRW)
			);
			#endregion DB Contexts



			#region Configure Cookie-based Authentication
			/* Is this Needed? 2019-08-26
			var loCookieAuthProps = new AuthenticationProperties
			{
				AllowRefresh = true,
				IsPersistent = true,
				RedirectUri = "~/Pages/oops.html"
			};
			services.AddSingleton<Microsoft.AspNetCore.Authentication.AuthenticationProperties>(loCookieAuthProps);
			*/

			// services.AddAntiforgery(options =>
			// 	{
			// 		options.Cookie.Domain = loAppSetting.AuthCookieOpts.AuthCookieDomain;
			// 		options.Cookie.Name = "X-AUTH-TAG";
			// 		options.Cookie.Path = "/";
			// 		options.FormFieldName = "xcsrftag";
			// 		options.HeaderName = "X-CSRF-TOKEN-GPM";
			// 		options.SuppressXFrameOptionsHeader = false;
			// 		// options.RequireSsl = false;
			// 	});


			log.Info("Authentication: Configure Cookie-based Authentication");
			log.Debug("SET hotspot IP, if using. Get IP using command 'ip -4 -ts route'");
			// loAppSetting.AuthCookieOpts.AuthCookieDomain = "192.168.12.1";
			services
				.AddAuthentication(loAppSetting.AuthCookieOpts.AuthSchemeName)
				.AddCookie(
					  loAppSetting.AuthCookieOpts.AuthSchemeName
					, options =>
						{
							options.SlidingExpiration = true;
							options.LoginPath = new PathString(loAppSetting.AuthCookieOpts.SignOnPath);
							options.LogoutPath = new PathString(loAppSetting.AuthCookieOpts.SignOutPath);
							// options.AccessDeniedPath = new PathString(loAppSetting.AuthCookieOpts.AccessDenyPath);
							options.AccessDeniedPath = new PathString(loAppSetting.AuthCookieOpts.SignOnPath);
							options.ReturnUrlParameter = loAppSetting.AuthCookieOpts.ReturnURL_TagName;  // get from loAppSetting
							options.ExpireTimeSpan = loAppSetting.AuthCookieOpts.SessionTimeout;
							// Auth Cookie options
							options.Cookie.Name = loAppSetting.AuthCookieOpts.AuthCookieName;
							options.Cookie.Path = "/";
							options.Cookie.Domain = loAppSetting.AuthCookieOpts.AuthCookieDomain;
							options.Cookie.SameSite = SameSiteMode.Lax;
							options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
							// options.Cookie.IsEssential = true;
						}
				)
				;

			#endregion Configure Cookie-based Authentication

			log.Info("MVC: Configure and set CompatibilityVersion");
			services
				.AddMvc()
				// .AddMvc(options => options.Filters.Add(typeof(gpm_web.Infra.Helpers.GateActionFilter)))
				.AddJsonOptions(
					options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
				)
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
				;
			services.AddAuthorization(options =>
			{
				// ROLE BASED AUTHORIZATION
				// options.AddPolicy("AnySystemUser", policy => policy.RequireRole(Consts.USER_ROLES.USER, Consts.USER_ROLES.ADMIN));
				// options.AddPolicy("RequireAdmin", policy => policy.RequireRole(Consts.USER_ROLES.ADMIN));

				// CLAIM BASED AUTHORIZATION
				options.AddPolicy("ReqSignin", policy => policy.RequireClaim(ClaimTypes.Role, Consts.USER_ROLES.USER, Consts.USER_ROLES.ADMIN));
				options.AddPolicy("RequireAdmin", policy => policy.RequireClaim(ClaimTypes.Role, Consts.USER_ROLES.ADMIN));
			});

			services.AddSwaggerGen(options =>
			{
				options.SwaggerDoc("rc3", new Info
				{
					Title = "SSSIHMS Gatepass"
					,
					Version = "rc3"
					,
					Description = "API Endpoints to support mobile applications."
				});
				options.IgnoreObsoleteActions();
				options.IgnoreObsoleteProperties();

				// Filter to force authorize on all methods except allow anonymous
				options.OperationFilter<Infra.Helpers.SecurityRequirementsOperationFilter>();

				// Add authentication schema
				var security = new Dictionary<string, IEnumerable<string>>
				{
					{appconfig.AuthKeyName, new string[] { }},
				};

				options.AddSecurityDefinition(appconfig.AuthKeyName, new ApiKeyScheme
				{
					Description = $"Authorization header using the Bearer scheme. Example: \"{appconfig.AuthKeyName}: {{token}}\"",
					Name = appconfig.AuthKeyName,
					In = "header",
					Type = "apiKey"
				});
				options.AddSecurityRequirement(security);

				// Set the comments path for the Swagger JSON and UI.
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				options.IncludeXmlComments(xmlPath);
			});


			log.Info("ConfigureServices: Done");
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			log.Info("==> Configure");
			log.Info("Configuration = {0}", env.EnvironmentName);
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				// app.UseHsts();
			}

			app
				// .UseHttpsRedirection()
				.UseStaticFiles()
				.UseCookiePolicy()
				.UseAuthentication()
				.UseSwagger(options =>
				{
					options.RouteTemplate = "api/{documentName}/swagger.json";
				})
				.UseSwaggerUI(options =>
				{
					options.SwaggerEndpoint("/api/rc3/swagger.json", "SSSIHMS Gatepass - RC3");
					options.RoutePrefix = "api";
				})
				.UseMvc(routes =>
					{
						routes.MapRoute(
							name: "default",
							template: "{controller=Session}/{action=Login}/{id?}");
						routes.MapRoute(
							name: "twoparm",
							template: "{controller=GatePass}/{action=PassStatus}/{linkid?}/{status?}");
					})
				;

			log.Info("Configure: Done");

			// TODO: Get instance of gpm_web.Infra.DayCapacityCounters Service and trigger initialization.

		}
	}
}
