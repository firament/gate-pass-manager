﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using gpm_web.Models;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{
	public class HomeController : Infra.GPMController
	{
		public HomeController(ILogger<HomeController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context)
			: base(logger, App_Config, db_context)
		{ }

		public IActionResult Index()
		{
			log.LogDebug("==> Index");
			log.LogDebug("User Role => {0}", UserData?.Role);

			if (UserData?.Role == SC.USER_ROLES.ADMIN)
			{
				return View("IndexAdmin");
			}
			else if (UserData?.Role == SC.USER_ROLES.USER)
			{
				return View("IndexUser");
			}


			return RedirectToAction("Login", "Session");
		}

		public IActionResult IndexUser()
		{
			log.LogDebug("==> IndexUser");
			return View();
		}

		public IActionResult IndexAdmin()
		{
			log.LogDebug("==> IndexAdmin");
			return View();
		}

		public IActionResult IndexOperator()
		{
			log.LogDebug("==> IndexOperator");
			return View();
		}

		public IActionResult Privacy()
		{
			log.LogDebug("==> Privacy");
			return View();
		}

	}
}
