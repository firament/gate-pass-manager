using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using gpm_web.Models;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{


	/// <summary>
	/// 
	/// </summary>
	public class SessionController : Infra.GPMController
	{


		public SessionController(ILogger<SessionController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context)
			: base(logger, App_Config, db_context)
		{ }



		/// <summary>
		/// The login display form
		/// </summary>
		/// <returns></returns>
		public IActionResult Login()
		{
			log.LogDebug("==> Login");
			return View();
		}



		/// <summary>
		/// Validate and Veriy credentials posted by Login
		/// </summary>
		/// <returns></returns>
		public IActionResult ValidateCredentials([FromForm]string UserID, [FromForm]string UserPWD)
		{
			log.LogDebug("==> ValidateCredentials");

			// Signout user if already logged in
			if (User.Identity.IsAuthenticated)
			{
				log.LogInformation("User already logged in, clearing session before proceeding");

				// Add log history entry
				int liUserID = int.Parse(User.Claims.First(c => c.Type == Consts.KEYS.USERID_KEY)?.Value ?? "0");
				dbcontext.MaintLoginHistory.Add(new CTX.MaintLoginHistory()
				{
					UserId = liUserID,
					UserLogin = User.Identity.Name,
					Status = (int)Enums.LOG_ENTRY_TYPE.LOGIN,
				});
				dbcontext.SaveChanges();

				HttpContext.SignOutAsync(appconfig.AuthCookieOpts.AuthSchemeName);
			}

			(StatusCodeResult Status, Models.UserPayload UserData, System.Threading.Tasks.Task AuthResult) loAuthID;
			loAuthID = AuthenticateUser(UserID, UserPWD);
			if (!(loAuthID.Status is OkResult))
			{
				log.LogWarning("Authentication failed. See Prior entries for details.");
				TempData[Consts.KEYS.ERROR_VIEWBAG] = "Login Failed.";
				return RedirectToAction("Login");
			}

			switch (loAuthID.UserData.Role)
			{
				case Consts.USER_ROLES.USER:
					log.LogInformation("User {0}/{1} Authenticated to role {2}. Redirecting to User landing page."
									, loAuthID.UserData.Userid
									, UserID
									, loAuthID.UserData.Role
									);
					return RedirectToAction("IndexUser", "Home");
				case Consts.USER_ROLES.ADMIN:
					log.LogInformation("User {0}/{1} Authenticated to role {2}. Redirecting to Admin landing page."
									, loAuthID.UserData.Userid
									, UserID
									, loAuthID.UserData.Role
									);
					return RedirectToAction("IndexAdmin", "Home");
				default:
					break;
			}

			log.LogInformation("User {0}/{1} Authenticated to role {2}. Role is Unknown, redirecting back to Login Page."
									, loAuthID.UserData.Userid
									, UserID
									, loAuthID.UserData.Role
									);
			TempData[Consts.KEYS.ERROR_VIEWBAG] = "Unknown Role assigned. Contact your Administrator.";
			return RedirectToAction("Login");
		}



		/// <summary>
		/// Logout the current user, and redirect to login form.
		/// </summary>
		/// <returns></returns>
		public IActionResult Logout()
		{
			log.LogDebug("==> Logout");
			HttpContext.SignOutAsync(appconfig.AuthCookieOpts.AuthSchemeName);
			return RedirectToAction("Login");
		}



		// TODO: Move to gpm_web.Infra.Authenticator
		// 	Once Di for transient service or to a static class is resolved.
		private (
					  StatusCodeResult Status
					, Models.UserPayload UserData
					, System.Threading.Tasks.Task AuthResult
					)
				AuthenticateUser(
					  string UserID
					, string UserPassword
					)
		{
			if ((string.IsNullOrWhiteSpace(UserID)) || (string.IsNullOrWhiteSpace(UserPassword)))
			{
				// Fail the request, write details in the log
				log.LogError("Unable to authenticate user with given data");
				return (new BadRequestResult(), null, null);
			}

			// 200	OkResult
			// 204	NoContentResult
			// 400	BadRequestResult
			// 401	UnauthorizedResult
			// 404	NotFoundResult
			// 409	ConflictResult
			// 415	UnsupportedMediaTypeResult
			// 422	UnprocessableEntityResult
			Models.UserPayload loUPL;
			string lsRoleTxt = "";
			string lsJSON = "";
			string lsToken = Guid.NewGuid().ToString("N");
			DateTime ldtNow = DateTime.Now;
			CTX.MaintSessions loSenEntry;

			//##	get entry from DB
			int liCount = dbcontext.Users.Count(u => u.UserLogin == UserID);
			if (liCount != 1)
			{
				log.LogError("User lookup Failed. Expected 1 record, got {0} records. For Login {1}", liCount, UserID);
				return (new ConflictResult(), null, null);
			}
			CTX.Users loKnocker = dbcontext.Users.First(u => u.UserLogin == UserID);

			//##	get hashed password, and other lookups
			string lsPwdHash = GC.Utils.GetHashedPWD(UserPassword);

			//##	assess request
			if ((loKnocker.Status != (int)Enums.USER_STATUS.Active) || (loKnocker.UserPwd != lsPwdHash))
			{
				log.LogWarning("User authentication failed. UserID = {0}, UserLogin = {1}, Status = {2}, UserPwd Match = {3}"
											, loKnocker.UserId
											, loKnocker.UserLogin
											, loKnocker.Status
											, (loKnocker.UserPwd != lsPwdHash)
											);
				dbcontext.MaintLoginHistory.Add(new CTX.MaintLoginHistory()
				{
					UserId = loKnocker.UserId,
					UserLogin = loKnocker.UserLogin,
					Status = (int)Enums.LOG_ENTRY_TYPE.FAILAUTH,
				});
				return (new UnauthorizedResult(), null, null);
			}

			//  create payload container
			lsRoleTxt = Lookups.DD_Lookup.RoleText(loKnocker.Role).ToUpper();
			loUPL = new UserPayload()
			{
				Msid = 0, // loSenEntry.Msid,
				Token = lsToken,
				Userid = loKnocker.UserId,
				LoginID = loKnocker.UserLogin,
				DisplayName = loKnocker.DisplayName,
				Role = lsRoleTxt,
				IssueTime = ldtNow,
				AuthTime = ldtNow,
				LastUsed = ldtNow,
				Status = (int)Enums.SESSION_STATUS.ACTIVE,
				DeviceSig = "",
			};
			lsJSON = GC.Utils.ToJSON(loUPL, false);

			//##	add session entry
			loSenEntry = new CTX.MaintSessions()
			{
				Token = lsToken,
				Userid = loKnocker.UserId,
				Role = lsRoleTxt,
				IssueTime = ldtNow,
				AuthTime = ldtNow,
				LastUsed = ldtNow,
				Status = (int)Enums.SESSION_STATUS.ACTIVE,
				DeviceSig = lsJSON,
			};
			dbcontext.MaintSessions.Add(loSenEntry);
			dbcontext.MaintLoginHistory.Add(new CTX.MaintLoginHistory()
			{
				UserId = loKnocker.UserId,
				UserLogin = loKnocker.UserLogin,
				Status = (int)Enums.LOG_ENTRY_TYPE.LOGIN,
			});

			//##	update last login time
			loKnocker.LastLogin = ldtNow;

			try
			{
				liCount = dbcontext.SaveChanges();
				log.LogInformation("User signin OK. Login = {0}", loKnocker.UserLogin);
			}
			catch (System.Exception exS)
			{
				log.LogError(exS, "Database update failed to create login entries");
				return (new UnprocessableEntityResult(), null, null);
			}

			if (IsAPI)
			{
				// Skip identity for API. Will not be used.
				log.LogInformation("Skiping ClaimsPrincipal setup for API calls.");
				return (new OkResult(), loUPL, null);
			}

			//##	create identity object
			// set auth options
			var loAuthProperties = new AuthenticationProperties
			{
				AllowRefresh = true,
				IsPersistent = false
			};
			// build set of claims
			List<Claim> lstClaims = new List<Claim>
				{
				new Claim(ClaimTypes.Name, loKnocker.DisplayName),
				new Claim(ClaimTypes.Role, loUPL.Role),
				new Claim(loUPL.Role, "true", ClaimValueTypes.Boolean),
				new Claim(ClaimTypes.SerialNumber, loUPL.Token),
				new Claim(Consts.KEYS.USERID_KEY, loUPL.Userid.ToString(), ClaimValueTypes.Integer),
				};
			// prepare identity for auth
			ClaimsIdentity loClaimsIdentity = new ClaimsIdentity(
					  lstClaims
					, appconfig.AuthCookieOpts.AuthSchemeName
					);

			//##	Authenticate user
			System.Threading.Tasks.Task ltResult = HttpContext.SignInAsync(
				  appconfig.AuthCookieOpts.AuthSchemeName
				, new ClaimsPrincipal(loClaimsIdentity)
				, loAuthProperties
				);

			// Inspect result
			if (!ltResult.IsCompletedSuccessfully)
			{
				log.LogWarning("SessionController Signon failed. Status = {0} Inspect."
						, ltResult.Status
						);
			}
			return (new OkResult(), loUPL, ltResult);
		}



	}
}
