using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using gpm_web.Models;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{
	/// <summary>
	/// 
	/// </summary>
	public class WFController : Infra.GPMController
	{
		// public string TestResults { get; set; } = "";
		public StringBuilder SBResultData { get; set; }

		public WFController(ILogger<WFController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context, gpm_web.Infra.Authenticator authservice)
			: base(logger, App_Config, db_context)
		{
			SBResultData = new StringBuilder();

			// Test authenticator ctor
			authservice.InvokeAuthenticator();
		}


		[Authorize("RequireAdmin")]
		public IActionResult Index(int id = 0)
		{
			log.LogDebug("==> Index");
			switch (id)
			{
				case 2:
					PutSampleAppConfig();
					break;
				case 1:
					DBConnectionTest();
					break;
				default:
					break;
			}

			string lsRawText = SBResultData.ToString().Replace("\n", "\n<br/>");
			// <br/> is not working, pass array
			ViewBag.Result = lsRawText;
			return View();
		}

		/// <summary>
		/// Puts an entry in the database of AppSettingContainer
		/// for use in preparing data seeds
		/// </summary>
		/// <returns></returns>
		[Authorize("RequireAdmin")]
		public IActionResult PutSampleAppConfig()
		{
			log.LogDebug("==> PutSampleAppConfig");
			SBResultData.AppendLine("Begin PutSampleAppConfig");

			string lsJSON = appconfig.ToJSON();
			lsJSON = GC.Utils.ToJSON(appconfig, false);


			SBResultData.AppendLine("Got AppConfig JSON and Building DB Record");
			CTX.GenAppSettings loAS = new CTX.GenAppSettings()
			{
				AppCode = "abcd1234",
				AppConfigVer = 1001,
				AppConfig = lsJSON,
				Status = 1
			};

			SBResultData.AppendLine("Linking record to db context");
			dbcontext.GenAppSettings.Add(loAS);

			SBResultData.AppendLine("Saving to DB");
			int liResult = dbcontext.SaveChanges();

			SBResultData.AppendLine("Saved to DB, unless ou see an error");

			return null;
		}

		[Authorize("RequireAdmin")]
		public IActionResult DBConnectionTest()
		{
			log.LogDebug("==> DBConnectionTest");
			List<EFC.dbsets.DdCategories> loCatgs = dbcontext
							.DdCategories
							.OrderBy(c => c.DdiCatgId)
							.ToList()
							;

			foreach (EFC.dbsets.DdCategories vCatg in loCatgs)
			{
				log.LogDebug(vCatg.DdiCatgName);
			}

			return View();
		}


		[Authorize("RequireAdmin")]
		public override void OnActionExecuted(Microsoft.AspNetCore.Mvc.Filters.ActionExecutedContext context)
		{
			log.LogDebug("==> WFController.OnActionExecuted");
			// Converttext to HTML ready
			string lsRawText = SBResultData.ToString().Replace("\n", "<br/>");
			ViewBag.Result = lsRawText;
		}

	}


}

