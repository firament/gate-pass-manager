using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using gpm_web.Models;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;


using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{
	/// <summary>
	/// 
	/// </summary>
	public class UserController : Infra.GPMController
	{

		public UserController(ILogger<UserController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context)
			: base(logger, App_Config, db_context)
		{ }

		[Authorize("RequireAdmin")]
		public IActionResult Index()
		{
			log.LogDebug("==> Index");
			List<CTX.Users> loUsers = dbcontext.Users.OrderBy(c => c.UserLogin).ToList();
			return View("Index", loUsers);
		}

		[Authorize("RequireAdmin")]
		public IActionResult Edit(int id)
		{
			log.LogDebug("==> Edit");
			CTX.Users loUser;
			if (id == 0)
			{
				loUser = new CTX.Users();
			}
			else
			{
				loUser = dbcontext.Users
							.Where(c => c.UserId == id)
							.FirstOrDefault()
							;
			}
			if (loUser == null)
			{
				PostError("Selected User does not exist in the system");
				log.LogWarning("User ID == {0} is not found in system. Inspect Requester and DB. This condition is unexpected.");
			}
			return View(loUser);
		}


		[Authorize("RequireAdmin")]
		// Used for both create and update
		public IActionResult Put(CTX.Users UserForm)
		{
			log.LogDebug("==> Put");

			// Allow blank passwords in edits.
			if (!ModelState.IsValid && !(ModelState.ErrorCount == 1 && ModelState["UserPwd"].ValidationState.ToString() == "Invalid"))
			{
				// ModelState write to error mesg
				return View("Edit", UserForm);
			}
			DateTime ldtNOW = DateTime.Now;
			// string lsHashPWD = GC.Utils.GetHashedPWD(UserForm.UserPwd);
			// log.LogDebug("Hashed {0} to {1}", UserForm.UserPwd, lsHashPWD);

			// Get entity from DB
			CTX.Users luCurrRecord = dbcontext.Users
											.Where(y => y.UserId == UserForm.UserId)
											.FirstOrDefault()
											;
			if (UserForm.UserId > 0 && luCurrRecord == null)
			{
				log.LogError("No record in Db to update. ID = {0}. Aborting further processing.", UserForm.UserId);
				// Set error message
				return View("Edit", UserForm);
			}

			if (luCurrRecord == null)
			{
				luCurrRecord = new CTX.Users();
			}

			// Update all fields
			luCurrRecord.FullName = UserForm.FullName.Trim();
			luCurrRecord.DisplayName = UserForm.DisplayName.Trim();
			luCurrRecord.UserLogin = UserForm.UserLogin.Trim().ToLower();
			luCurrRecord.Role = UserForm.Role;
			luCurrRecord.Status = UserForm.Status;
			if (!string.IsNullOrWhiteSpace(UserForm.UserPwd))
			{
				luCurrRecord.UserPwd = GC.Utils.GetHashedPWD(UserForm.UserPwd.Trim());
			}
			luCurrRecord.EditOn = ldtNOW;
			luCurrRecord.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;

			if (UserForm.UserId == 0)
			{
				luCurrRecord.AddOn = ldtNOW;
				luCurrRecord.AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;

				// dbcontext.Add(UserForm);
				dbcontext.Users.Add(luCurrRecord);
			}

			// Save
			int liCount = dbcontext.SaveChanges();
			log.LogInformation("User save ({0}) updated {1} records.", UserForm.UserId, liCount);

			return RedirectToAction("Index");
		}

	}
}
