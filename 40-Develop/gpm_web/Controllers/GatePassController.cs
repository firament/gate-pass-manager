using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using gpm_web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Text;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using BO = gpm_web.BizObjs;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{
	/// <summary>
	/// 
	/// </summary>
	public partial class GatePassController : Infra.GPMController
	{
		public IA.TokenCounter Token_Counter { get; set; }
		public IA.DayCapacityCounters Dept_Day_Caps { get; set; }
		public SL.DD_Lookup DDHelp { get; set; }
		public BO.GatePassBZ GPBizObj { get; set; }

		public GatePassController(ILogger<GatePassController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context, IA.TokenCounter token_counter, IA.DayCapacityCounters dept_cap_counter, SL.DD_Lookup dd_helper, BO.GatePassBZ boGP)
			: base(logger, App_Config, db_context)
		{
			log.LogInformation("==> GatePassController:CTOR");
			Token_Counter = token_counter;
			Dept_Day_Caps = dept_cap_counter;
			DDHelp = dd_helper;
			GPBizObj = boGP;
		}

		[Authorize]
		public IActionResult TypeSelect()
		{
			return View(new CTX.GatePasses());
		}

		[Authorize]
		public IActionResult DeptSelect()
		{
			log.LogDebug("User.Identity.IsAuthenticated = {0}", User.Identity.IsAuthenticated);
			return View(Dept_Day_Caps.AvailaibleCaps(dbcontext));
		}

		// Refactor done
		[Authorize]
		public IActionResult NewPassPatient(int deptid = 0, int vtype = 0)
		{
			log.LogInformation("==> NewPassPatient");
			CTX.GatePasses loNewGPP = GPBizObj.NewPassPatient(deptid, vtype);
			if (loNewGPP == null)
			{
				PostError("Invalid values for Department and Type. Try again with valid values.");
				return RedirectToAction("DeptSelect");
			}
			ViewData.Model = loNewGPP;
			return View();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id">Pass ID, for which the vehicle pass is to be created</param>
		/// <returns></returns>
		[Authorize]
		public IActionResult NewPassVehicle(int id = 0)
		{
			log.LogInformation("==> NewPassVehicle");
			CTX.GatePasses loNewGPP = GPBizObj.NewPassVehicle(id);
			if (loNewGPP == null)
			{
				PostError("Invalid value for Root Pass. Try again with valid values.");
				return RedirectToAction("TypeSelect");
			}
			string lsTest = loNewGPP.AddOn.ToLocalTime().ToString(SC.FORMATS.DATE_TIME_FORMAT);
			ViewData.Model = loNewGPP;
			ViewBag.RootPassNum = loNewGPP.Note;    // TD001: NOT a good idea, move to ideal usage ASAP.
			return View();
		}

		[Authorize]
		public IActionResult NewPassVisitor(int vtype = 0)
		{
			return View();
		}

		[Obsolete("Not to be used. Use Type select directly.", true)]
		[Authorize]
		public IActionResult NewPassEmergency(int vtype = 0)
		{
			CTX.GatePasses gpEmergency = new CTX.GatePasses()
			{
				PassType = (int)SE.GATE_PASS_TYPE.EMERGENCY,
				SupplyDelivery = 0,
			};
			if (gpEmergency.SupplyDelivery == null)
			{
				gpEmergency.SupplyDelivery = 0;
			}
			return View();
		}

		// Refactor done
		[Authorize]
		public IActionResult PassStatus(int linkid, int status)
		{
			CTX.GatePasses lGP = GPBizObj.PatchPassStatus(linkid, status);
			return RedirectToAction("Edit", new { id = linkid });
		}

		// Refactor done
		[Authorize]
		public IActionResult Find(CTX.GatePasses GatePass)
		{
			List<CTX.GatePasses> lstResults = GPBizObj.FindPasses(GatePass);
			ViewBag.SearchFilter = GatePass;
			return View(lstResults);
		}

		[Authorize]
		public async Task<IActionResult> FindPagedAsync(ML.SearchFormGatepass SearchFilter)
		{
			ML.SearchResultsGatepass loResults;
			// loResults = GPBizObj.FindPagedAsync(SearchFilter).Result;
			loResults = await GPBizObj.FindPagedAsync(SearchFilter);
			return View(loResults);
		}

		[Authorize]
		public IActionResult FindOne(int id)
		{
			ViewBag.SearchFilter = id;
			if (!Request.Query.ContainsKey("id"))
			{
				ViewBag.SearchFilter = -1;
				return View(null);
			}
			CTX.GatePasses loResult = GPBizObj.FindPass(id);
			if (loResult == null)
			{
				return View(loResult);
			}
			loResult.IsPassOfInterest = true;
			if (loResult.RootGatePassNavigation == null)
			{
				loResult.StatusName = DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_STATUS, loResult.Status, true);
				ViewBag.RootPassNum = DDHelp.FormattedPassNumber(loResult.PassType, loResult.PassNum);
			}
			else
			{
				loResult.StatusName = DDHelp.DisplayText((int)SE.DDCategories.GATE_PASS_STATUS, loResult.RootGatePassNavigation.Status, true);
				ViewBag.RootPassNum = DDHelp.FormattedPassNumber(loResult.RootGatePassNavigation.PassType, loResult.RootGatePassNavigation.PassNum);
			}
			return View(loResult);
		}

		// Refactor done
		[Authorize]
		public IActionResult Put([FromForm]CTX.GatePasses GatePass, [FromRoute]int? id)
		{
			log.LogDebug("==> Put");
			return View("Edit", GPBizObj.CreateGatepass(GatePass, UserData));
		}

		// Refactor done
		[Authorize]
		public IActionResult Edit(int? id)
		{
			log.LogDebug("==> Put");
			int LinkID = id ?? ViewBag.LinkID ?? 0;
			if (id < 1)
			{
				log.LogError("Invalid login ID {0}. Cannot proceed");
				return new System.ArgumentOutOfRangeException() as IActionResult;
			}
			CTX.GatePasses loGPTree = GPBizObj.GetPassForEdit(id);
			if (loGPTree == null)
			{
				return new NotFoundResult();
			}
			return View(loGPTree);
		}

		[Authorize()]
		public IActionResult Print(int id = 0)
		{
			log.LogDebug("==> Put");
			return View();
		}

		// Refactor done
		[Authorize]
		public IActionResult Update(CTX.GatePasses gatepass)
		{
			if (!ModelState.IsValid)
			{
				ViewData[SC.KEYS.ERROR_VIEWBAG] = "Model validation failed!";
				return RedirectToAction("Edit", new { id = gatepass?.GatePassId ?? 0 });
			}
			CTX.GatePasses loGP = GPBizObj.UpdatePass(gatepass, UserData);
			if (loGP == null)
			{
				// Post error message for display.
			}
			return RedirectToAction("Edit", new { id = gatepass?.GatePassId ?? 0 });
		}

	}
}
