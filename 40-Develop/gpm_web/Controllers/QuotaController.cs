using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using gpm_web.Models;

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using stCode = gpm.Common.Enums.ErrorCode;

using static Microsoft.EntityFrameworkCore.EntityState;
using Microsoft.EntityFrameworkCore.ChangeTracking;


namespace gpm_web.Controllers
{
	/// <summary>
	/// 
	/// </summary>
	public partial class QuotaController : Infra.GPMController
	{
		public SL.DD_Lookup DDHelp { get; set; }
		public IA.DayCapacityCounters Dept_Day_Caps { get; set; }

		public QuotaController(ILogger<QuotaController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context, IA.DayCapacityCounters dept_cap_counter, SL.DD_Lookup dd_helper)
			: base(logger, App_Config, db_context)
		{
			DDHelp = dd_helper;
			Dept_Day_Caps = dept_cap_counter;
		}

		// BEGIN: sak debug quota cache bug
		// show working cache of current quotas
		[Authorize("RequireAdmin")]
		public IActionResult QuotaCache()
		{
			// FOR DEBUGGIN
			log.LogWarning("==> QuotaCache");
			log.LogWarning("Building quotas for day {0}", DateTime.Now.ToString(SC.FORMATS.DATE_FORMAT));
			log.LogWarning("For debugging cache sync bug. Remove after debug. sak 2021 Jan 07");

			return View(Dept_Day_Caps.getCacheData());
		}
		// FINIS: sak debug quota cache bug

		// show current day's quotas
		[Authorize("RequireAdmin")]
		public IActionResult Index([FromForm]DateTime? dyDaySel)
		{
			log.LogDebug("==> Index");
			dyDaySel = dyDaySel ?? (DateTime?)TempData["dyDaySel"] ?? DateTime.Today;
			ViewBag.dyDay = dyDaySel;
			log.LogInformation("Get quotas for day {0}", dyDaySel?.ToString(SC.FORMATS.DATE_FORMAT));
			List<CTX.Quotas> lstQuotas = getDayQuotas((DateTime)dyDaySel);
			return View(lstQuotas);
		}

		// Show full editor
		[Authorize("RequireAdmin")]
		public IActionResult Edit(
							CTX.Quotas DeptQuota = null
							)
		{
			log.LogDebug("==> Edit");

			CTX.Quotas lqSel = null;
			bool lbOK = false;
			int liCount = 0;

			log.LogDebug("Request.HasFormContentType = {0}", Request.HasFormContentType);
			// log.LogDebug("Request.Form.Keys.Contains(\"doUpdate\") = {0}", Request.Form.Keys.Contains("doUpdate"));
			if (Request.HasFormContentType && Request.Form["doUpdate"] == "Save")
			{
				lqSel = dbcontext.Quotas
								.Where(y => y.Date == DateTime.Today && y.Department == DeptQuota.Department)
								.FirstOrDefault();
				if (lqSel == null)
				{
					log.LogError("Record not found in DB for Department = {0} [{1}]", DeptQuota.Department, DeptQuota.DeprtmentName);
					return View(new CTX.Quotas());
				}

				// update counter with new values
				lqSel.CapNewVisit = DeptQuota.CapNewVisit;
				lqSel.CapRevisit = DeptQuota.CapRevisit;
				lqSel.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
				lqSel.EditOn = DateTime.Now;

				log.LogInformation("Attempting update of current register");
				lbOK = Dept_Day_Caps.UpdateQuota(lqSel.Department, lqSel.CapNewVisit, lqSel.CapRevisit);

				if (lbOK)
				{
					log.LogInformation("Updating quotas");
					liCount = dbcontext.SaveChanges();
					log.LogDebug("Updated {0} records, saving Quota update.", liCount);
				}
				else
				{
					log.LogInformation("Current register update failed. Aborting save to database.");
				}
			}
			else
			{
				log.LogInformation("Loading data for department: {0}", DeptQuota.Department);
				if (DeptQuota?.Department > 0)
				{
					GC.DeptDayCap loCurrUse = Dept_Day_Caps.AvailaibleCapsDepartment(DeptQuota.Department);
					lqSel = dbcontext.Quotas.AsNoTracking().Where(y => y.Date == DateTime.Today && y.Department == DeptQuota.Department).FirstOrDefault();
					// Add used values also
					lqSel.UsedNewVisit = loCurrUse.UsedFirst;
					lqSel.UsedRevisit = loCurrUse.UsedRevisit;
				}
			}

			// No record found for department
			if (lqSel == null)
			{
				lqSel = new CTX.Quotas();
			}

			return View(lqSel);
		}

		// redirect to index
		[Authorize("RequireAdmin")]
		public IActionResult Put(
							  [FromForm]DateTime? dyDay = null
							, [FromForm]DateTime? dyBegin = null
							, [FromForm]DateTime? dyEnd = null
							, [FromForm]bool dyDefault = true
							, [FromForm]int flAction = 0
							, List<CTX.Quotas> quotalist = null
							)
		{
			log.LogDebug("==> Put");

			// Validate required inputs
			bool lbInputBad = false;
			switch (flAction)
			{
				case 1:
					if (dyDay == null)
					{
						lbInputBad = true;
					}
					if (lbInputBad || dyDay <= DateTime.Today)
					{
						lbInputBad = true;
					}
					break;
				case 2:
					if (lbInputBad || dyBegin == null || dyEnd == null)
					{
						lbInputBad = true;
					}
					if (lbInputBad || dyBegin >= dyEnd)
					{
						lbInputBad = true;
					}
					if (lbInputBad || dyBegin <= DateTime.Today)
					{
						lbInputBad = true;
					}
					break;
				case 9:

					break;
				default:
					log.LogError("");
					break;
			}
			if (lbInputBad)
			{
				log.LogError("Validation failed, aborting processing.");
				// return View("Index");
				return RedirectToAction("Index");
			}

			ViewBag.dyDay = dyDay ?? dyBegin ?? DateTime.Today;
			List<CTX.Quotas> lstQuotas; // = getDayQuotas((DateTime)dyDay);

			switch (flAction)
			{
				case 1:
					lstQuotas = BuildQuoteList(
									  (DateTime)dyDay
									, (DateTime)dyDay
									, Request.Form["Date"]
									, Request.Form["CapNewVisit"]
									, Request.Form["CapRevisit"]
									, Request.Form["Department"]
									);
					break;
				case 2:
					lstQuotas = BuildQuoteList(
									  (DateTime)dyBegin
									, (DateTime)dyEnd
									, Request.Form["Date"]
									, Request.Form["CapNewVisit"]
									, Request.Form["CapRevisit"]
									, Request.Form["Department"]
									);
					break;
				case 9:
					lstQuotas = BuildQuoteList(
									  DateTime.MinValue
									, DateTime.MinValue
									, Request.Form["Date"]
									, Request.Form["CapNewVisit"]
									, Request.Form["CapRevisit"]
									, Request.Form["Department"]
									);
					break;
				default:
					lstQuotas = getDayQuotas(ViewBag.dyDay);
					log.LogError("");
					return View("Index", lstQuotas);
			}

			// Add all new records to database
			DateTime ldtNow = DateTime.Now;
			int liNew = (int)SE.RECORD_STATUS.NOT_SET;
			// update timestamps
			foreach (CTX.Quotas vQ in lstQuotas)
			{
				vQ.EditBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
				vQ.EditOn = ldtNow;
				if (vQ.Status == liNew)
				{
					vQ.Status = (int)SE.RECORD_STATUS.ACTIVE;
					vQ.AddBy = UserData?.Userid ?? SC.KEYS.SYSTEM_USER_ID_VAL;
					vQ.AddOn = ldtNow;
					dbcontext.Quotas.Add(vQ);
				}
			}

			int liRecords = dbcontext.SaveChanges();

			switch (flAction)
			{
				case 1:
					log.LogInformation("{0} records affected, updated quotas for date {1}", liRecords, dyDay?.ToString(SC.FORMATS.DATE_FORMAT));
					break;
				case 2:
					log.LogInformation(
									  "{0} records affected, updated quotas for dates {1} to {2}"
									, liRecords
									, dyBegin?.ToString(SC.FORMATS.DATE_FORMAT)
									, dyEnd?.ToString(SC.FORMATS.DATE_FORMAT)
									);
					break;
				case 9:
					log.LogInformation("{0} records affected, updated DEFAULT quotas", liRecords);
					break;
			}


			// If date is today, reset flag for counters
			if (ViewBag.dyDay == DateTime.Today)
			{
				Dept_Day_Caps.ResetInitialize();
			}

			TempData["dyDaySel"] = ViewBag.dyDay;
			return RedirectToAction("Index");

		}
	}
}
