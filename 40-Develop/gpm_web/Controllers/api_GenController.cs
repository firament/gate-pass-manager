using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using stCode = gpm.Common.Enums.ErrorCode;


namespace gpm_web.Controllers
{

	/// <summary>
	/// Endpoints to get items for use in Dropdowns and Options
	/// </summary>
	public partial class GenController : Infra.GPMController
	{
		public SL.DD_Lookup DDHelp { get; set; }

		public GenController(ILogger<GenController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context, SL.DD_Lookup dd_helper)
		: base(logger, App_Config, db_context)
		{
			IsAPI = true;
			DDHelp = dd_helper;
		}

		/// <summary>
		/// Get all Dropdown Categories, optionally with related items. Optionally including deactivated categories and items.
		/// </summary>
		/// <param name="ActiveOnly"></param>
		/// <param name="includeItems"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpGet("api/getEnumCategories")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(typeof(List<AM.DD_Catgs_Model>), 204)]
		[ProducesResponseType(typeof(List<AM.DD_Catgs_Model>), 200)]
		public List<AM.DD_Catgs_Model> GetEnumCategories([FromQuery] bool ActiveOnly = true, [FromQuery] bool includeItems = true)
		{
			return DDHelp.GetCategories(ActiveOnly, includeItems);
		}

		/// <summary>
		/// Get all Items of one dropdown category. Optionally including deactivated items.
		/// </summary>
		/// <param name="frmEnum"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpGet("api/GetEnumItems")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(typeof(List<ML.DD_Item>), 204)]
		[ProducesResponseType(typeof(List<ML.DD_Item>), 200)]
		[Consumes("application/json")]
		public List<ML.DD_Item> GetEnumItems([FromQuery]AM.EnumForm frmEnum)
		{
			return DDHelp.GetItems(frmEnum.ID, frmEnum.ActiveOnly);
		}

		/// <summary>
		/// Get list of all Departments. Optionally including deactivated departments.
		/// </summary>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpGet("api/getDepartmentList")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(204)]
		[ProducesResponseType(typeof(List<ML.DD_Item>), 200)]
		public List<ML.DD_Item> GetDepartmentList(bool activeOnly = true)
		{
			return DDHelp.GetItems(SE.DDCategories.DEPARTMENTS, activeOnly);
		}
	}
}
