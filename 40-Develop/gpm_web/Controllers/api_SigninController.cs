using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using gpm_web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Text;
using Microsoft.EntityFrameworkCore;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using BO = gpm_web.BizObjs;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{

	public partial class SigninController : Infra.GPMController
	{

		public BO.SessionBZ SessBizObj { get; set; }
		public SigninController(ILogger<SigninController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context, BO.SessionBZ boSn)
		: base(logger, App_Config, db_context)
		{
			IsAPI = true;
			SessBizObj = boSn;
		}

		/// <summary>
		/// Convenience method to test user exists in system.
		/// </summary>
		/// <param name="frmInput"></param>
		/// <returns></returns>
		[HttpPost()]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(204)]
		[ProducesResponseType(200)]
		public IActionResult GetUser([FromBody]AM.CredsForm frmInput)
		{
			bool lbResult = SessBizObj.TestUser(frmInput.loginID, frmInput.Password);
			if (lbResult)
			{
				return new OkResult();
			}
			else
			{
				return new NoContentResult();
			}
		}

		/// <summary>
		/// Creates a session for the user using the given credentials.
		/// </summary>
		/// <param name="Credentials"></param>
		/// <returns>null (204) if unable to authenticate user session</returns>
		[HttpPost("api/Open")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(204)]
		[ProducesResponseType(typeof(ML.UserPayload), 200)]
		[AllowAnonymous]
		public IActionResult CreateSession([FromBody]AM.CredsForm Credentials)
		{
			ML.UserPayload upl = null;
			try
			{
				upl = SessBizObj.CreateUserSession(Credentials.loginID, Credentials.Password);
			}
			catch (System.Exception exS)
			{
				log.LogError(exS, "Unable to authenticate user. ID = {0}", Credentials.loginID);
				return new BadRequestResult();
			}
			log.LogInformation("Created session for ID = {0}, User = {1}, Token = {2}", upl.Userid, upl.LoginID, upl.Token);
			if (upl == null)
			{
				return new NoContentResult();
			}
			else
			{
				return new OkObjectResult(upl);
			}
		}

		/// <summary>
		/// Close the given session.
		/// </summary>
		/// <param name="Token">Authentication token issued for the session.</param>
		/// <returns>true on success.</returns>
		[HttpDelete("api/Close")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(200)]
		[ProducesResponseType(204)]
		public IActionResult CloseSession(string Token)
		{
			bool lbResult = SessBizObj.CloseUserSession(Token);
			if (lbResult)
			{
				return new OkResult();
			}
			else
			{
				return new NoContentResult();
			}
		}

	}
}

