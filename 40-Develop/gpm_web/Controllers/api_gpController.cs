using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using HSC = Microsoft.AspNetCore.Http.StatusCodes;

using Microsoft.Extensions.Logging;
using CTX = gpm.Efc.dbsets;
using EFC = gpm.Efc;
using GC = gpm.Common;
using GCE = gpm.Common.Enums;
using SE = gpm_web.Enums;
using SC = gpm_web.Consts;
using SL = gpm_web.Lookups;
using SLD = gpm_web.Lookups.DD_Lookup;
using IA = gpm_web.Infra;
using ML = gpm_web.Models;
using AM = gpm_web.API_Models;
using BO = gpm_web.BizObjs;
using stCode = gpm.Common.Enums.ErrorCode;

namespace gpm_web.Controllers
{

	/// <summary>
	/// Container for all gatepass endpoints
	/// </summary>
	public partial class gpController : Infra.GPMController
	{
		public IA.TokenCounter Token_Counter { get; set; }
		public IA.DayCapacityCounters Dept_Day_Caps { get; set; }
		public SL.DD_Lookup DDHelp { get; set; }
		public BO.GatePassBZ GPBizObj { get; set; }

		public gpController(ILogger<gpController> logger, GC.IAppSettingContainer App_Config, gpm.Efc.dbsets.GPM_DBContext db_context, IA.TokenCounter token_counter, IA.DayCapacityCounters dept_cap_counter, SL.DD_Lookup dd_helper, BO.GatePassBZ boGP)
		: base(logger, App_Config, db_context)
		{
			IsAPI = true;
			Token_Counter = token_counter;
			Dept_Day_Caps = dept_cap_counter;
			DDHelp = dd_helper;
			GPBizObj = boGP;
		}


		/// <summary>
		/// Get list of all active departments. Along with current vailaible capacity and with current used capacity.
		/// </summary>
		/// <returns></returns>
		[HttpGet("api/getAvailaibility")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(204)]
		[ProducesResponseType(typeof(List<ML.DeptCapValues>), 200)]
		public IActionResult getAvailaibility()
		{
			log.LogDebug("==> [API] getAvailaibility");
			// return Dept_Day_Caps.AvailaibleCaps(dbcontext);
			List<ML.DeptCapValues> loResult = Dept_Day_Caps.AvailaibleCaps(dbcontext);
			if (loResult == null)
			{
				return new BadRequestResult();
			}
			else if (loResult.Count == 0)
			{
				return new NoContentResult();
			}
			else
			{
				return new OkObjectResult(loResult);
			}

		}

		/// <summary>
		/// Prepare a patient gatepass for filling details.
		/// </summary>
		/// <param name="gatepassForm">Only relevent feilds are required.</param>
		/// <returns>Prepared gatepass, or Null if quota not availaible for department.</returns>
		[HttpPost("api/NewPatientGatePass")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(typeof(CTX.GatePasses), 201)]
		[ProducesResponseType(typeof(CTX.GatePasses), 200)]
		[Consumes("application/json")]
		public IActionResult CreatePatientPass([FromBody] AM.GatePassForm gatepassForm)
		{
			log.LogDebug("==> [API] NewGatePass => Dept = {0}, VisitType = {1}", gatepassForm.Department, gatepassForm.VisitType);
			// return GPBizObj.CreateGatepass(gatepassForm.ToModel(), UserData);
			CTX.GatePasses loResult = GPBizObj.CreateGatepass(gatepassForm.ToModel(), UserData);
			if (loResult == null)
			{
				return new BadRequestResult();
			}
			else
			{
				return new CreatedResult(loResult.GatePassId.ToString(), loResult);
			}
		}

		/// <summary>
		/// Get a gatepass tree using GatepassID
		/// </summary>
		/// <param name="Id"></param>
		/// <returns>Prepared gatepass, or Null if unable to find record.</returns>
		[HttpGet("api/getByID")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(typeof(Nullable), 204)]
		[ProducesResponseType(typeof(CTX.GatePasses), 200)]
		// ActionResult
		public ActionResult<CTX.GatePasses> GetGatePass(int Id)
		{
			log.LogDebug("==> [API] getByID => id = {0}", Id);
			CTX.GatePasses loResult = GPBizObj.GetPassForEdit(Id);
			if (loResult == null)
			{
				return new NoContentResult();
			}
			else
			{
				return new ObjectResult(loResult);
			}
			// return GPBizObj.GetPassForEdit(Id);
		}

		/// <summary>
		/// Update a Gatepass.
		/// </summary>
		/// <param name="gatepassForm"></param>
		/// <returns></returns>
		[HttpPut("api/UpdateGatePass")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(typeof(CTX.GatePasses), 200)]
		[Consumes("application/json")]
		public IActionResult Update([FromBody] AM.GatePassForm gatepassForm)
		{
			log.LogDebug("==> [API] UpdateGatePass => id = {0}", gatepassForm.GatePassId);
			// return GPBizObj.UpdatePass(gatepassForm.ToModel(), UserData);
			CTX.GatePasses loResult = GPBizObj.UpdatePass(gatepassForm.ToModel(), UserData);
			if (loResult == null)
			{
				return new BadRequestResult();
			}
			else
			{
				return new OkObjectResult(loResult);
			}
		}

		/// <summary>
		/// Idempotent change of Gatepass Status
		/// </summary>
		/// <param name="frmInput"></param>
		[HttpPatch("api/setStatus")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(typeof(CTX.GatePasses), 200)]
		[Consumes("application/json")]
		public IActionResult Status([FromBody]AM.IdStatusForm frmInput)
		{
			log.LogDebug("==> [API] setStatus => id = {0}, status = {1}", frmInput.Id, frmInput.Status);
			// GPBizObj.PatchPassStatus(frmInput.Id, (int)frmInput.Status);
			CTX.GatePasses loResult = GPBizObj.PatchPassStatus(frmInput.Id, (int)frmInput.Status);
			if (loResult == null)
			{
				return new BadRequestResult();
			}
			else
			{
				return new OkObjectResult(loResult);
			}
		}

		/// <summary>
		/// Testing method to move gatepass status directly to 'Exit'
		/// </summary>
		/// <param name="frmInput"></param>
		[HttpPatch("api/Checkout")]
		[ProducesResponseType(400)]
		[ProducesResponseType(401)]
		[ProducesResponseType(500)]
		[ProducesResponseType(typeof(CTX.GatePasses), 200)]
		[Consumes("application/json")]
		[Obsolete("For testing only, do not use.")]
		public IActionResult Checkout(AM.IdStatusForm frmInput)
		{
			log.LogDebug("==> [API] Checkout => id = {0}, status = {1}", frmInput.Id, frmInput.Status);
			CTX.GatePasses loResult = GPBizObj.PatchPassStatus(frmInput.Id, (int)SE.GATE_PASS_STATUS.EXIT);
			if (loResult == null)
			{
				return new BadRequestResult();
			}
			else
			{
				return new OkObjectResult(loResult);
			}
		}

	}
}
