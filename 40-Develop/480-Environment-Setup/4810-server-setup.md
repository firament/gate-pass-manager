# Setup Host Server

## Notes
- Reboot Command
    ```sh
    shutdown -r now ""
    shutdown -r now "reason for reboot"
    ```
- dotnet core {`SDK 2.2.108` | `Runtime 2.2.6`}
	- https://dotnet.microsoft.com/download/dotnet-core/2.2#sdk-2.2.108
	- https://download.visualstudio.microsoft.com/download/pr/f7337a51-c66d-41e6-b901-6e13faabd1da/8b0b3cce21d3910176a9123a35dd59bb/dotnet-sdk-2.2.108-linux-x64.tar.gz
	- Current release has known issues with linux
	- Watch and update to current when stabilized
- TODO: Consolidate all apt-get install commands, to one single command
- Optimize logs
- [ ] Harden Security
	- DB => 
		- [ ] Remove development utilities
	- APP => 
		- [ ] Enable full-time SSL
		- [ ] Move passwords to secrets
	- Operator =>
		- [ ] Configure for certificate only
	- FTP => 
		- [ ] https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-for-a-user-s-directory-on-ubuntu-18-04
		- [ ] Configure for SSL/SSH

***
## With partial updates
> Used with snowflake
> to be organized
```sh
REL_TAG="R-20210208-1612767314"
REL_DIR="/NoveltyData/gpm/ftproot/files/apppacks/${REL_TAG}";
sudo mkdir -vp ${REL_DIR};

# After copy
sudo chown -R gftp-user ${REL_DIR};

sudo systemctl status gpm-web-app.service;
sudo systemctl stop gpm-web-app.service;
sudo systemctl start gpm-web-app.service;

su gweb-updater;
	# a2b9-1789-a45f-d278-e9b2
echo  ${REL_DIR};
[[ -z ${REL_DIR} ]] && { 
        echo ""; echo ""; echo ""; echo "FATAL:: Source folder not set, FIX before continuing!"; echo ""; 
    } || { 
        echo ""; echo "OK, continue."; echo "";
        cp -vf ${REL_DIR}/* /NoveltyData/gpm/webroot/
    } ; 

ls -l /NoveltyData/gpm/webroot/

```


## Update Application Releases
- Prepare for update
```sh
# Start FTP service to upload update package
sudo systemctl start vsftpd.service
systemctl status vsftpd.service
```

### FTP 
- Push updated binary package to FTP location


### Application
```sh
# Stop application before updating
sudo systemctl stop apache2
sudo systemctl stop gpm-web-app.service;
systemctl status apache2
systemctl status gpm-web-app.service;

su gweb-updater;
	# a2b9-1789-a45f-d278-e9b2
ls -1 /NoveltyData/gpm/ftproot/files/apppacks/gpmweb-*.tar.gz
RELEASE_FILE="gpmweb-20191125-1574654077.tar.gz";	# Verify the correct name to use
echo "${RELEASE_FILE}";
rm -vrf /NoveltyData/gpm/webroot/*;
ls -l /NoveltyData/gpm/webroot
tar -xz --strip-components=1 -C /NoveltyData/gpm/webroot -f /NoveltyData/gpm/ftproot/files/apppacks/${RELEASE_FILE};
ls -l /NoveltyData/gpm/webroot
exit;

# Restart application
sudo systemctl start gpm-web-app.service;
systemctl status gpm-web-app.service;
sudo systemctl start apache2
systemctl status apache2

```

### Database	 [TODO:]
- Login as gpm-dev-rw
	- sudo mysql -v -h 127.0.0.1 -P 3306 -D SSSIHMS_GPM -u gpm-dbadmin -p
- execute each script with command
	- source /NoveltyData/gpm/ftproot/files/dbscripts/db-50a-seed-p2.sql

- Post update
```sh
sudo systemctl stop vsftpd.service
systemctl status vsftpd.service
```


***

## Initial Environment Setup
- All Installs
```sh
	sudo apt-get update;
	sudo apt-get install vsftpd apache2;
	sudo a2enmod proxy proxy_http proxy_html;
	# sudo apt-get install mysql-client mysql-server

    # Set time zone
    sudo unlink /etc/localtime;
    sudo ln -s /usr/share/zoneinfo/Asia/Kolkata /etc/localtime;
    timedatectl; # Verify, after reboot
```

### Users
```sh
## Disable shell access
sudo touch /bin/ftponly;
echo '#!/bin/sh' | sudo tee -a /bin/ftponly;
echo 'echo "This account is limited to FTP access only."' | sudo tee -a /bin/ftponly;
sudo chmod a+x /bin/ftponly
echo '/bin/ftponly' | sudo tee -a /etc/shells

# test files
cat /bin/ftponly
cat /etc/shells

# Prepare auth groups
sudo addgroup gpm-operator-group
sudo addgroup gpm-ftp-group
sudo addgroup gpm-web-group
sudo addgroup gpm-log-group

# Inspect Groups
getent group | grep gpm

# prepare root folders
sudo mkdir -vp /NoveltyData/gpm
sudo chown -R nobody:gpm-operator-group /NoveltyData/gpm

# Lib-Dependeencies Folders
sudo mkdir -vp /NoveltyData/gpm/base-libs
sudo chown nobody:gpm-operator-group /NoveltyData/gpm/base-libs
sudo chmod g+rw /NoveltyData/gpm/base-libs
# FTP folders
sudo mkdir -vp /NoveltyData/gpm/ftproot
sudo chown nobody:nogroup /NoveltyData/gpm/ftproot
sudo chmod a-w /NoveltyData/gpm/ftproot/
sudo mkdir /NoveltyData/gpm/ftproot/files
sudo chown nobody:gpm-ftp-group /NoveltyData/gpm/ftproot/files
sudo chmod g+w /NoveltyData/gpm/ftproot/files
## Folder for installables
sudo mkdir /NoveltyData/gpm/ftproot/files/installs
sudo chown nobody:gpm-ftp-group /NoveltyData/gpm/ftproot/files/installs
sudo chmod g+w /NoveltyData/gpm/ftproot/files/installs
## Folder for application updates
sudo mkdir /NoveltyData/gpm/ftproot/files/apppacks
sudo chown nobody:gpm-ftp-group /NoveltyData/gpm/ftproot/files/apppacks
sudo chmod g+w /NoveltyData/gpm/ftproot/files/apppacks
sudo mkdir /NoveltyData/gpm/ftproot/files/dbscripts/
sudo chown nobody:gpm-ftp-group /NoveltyData/gpm/ftproot/files/dbscripts/
sudo chmod g+w /NoveltyData/gpm/ftproot/files/dbscripts/
## Folder for Miscellanoes files
sudo mkdir /NoveltyData/gpm/ftproot/files/misc
sudo chown gftp-user:gpm-ftp-group /NoveltyData/gpm/ftproot/files/misc
sudo chmod g+w /NoveltyData/gpm/ftproot/files/misc

# Web Folders
sudo mkdir -vp /NoveltyData/gpm/webroot
sudo chown nobody:gpm-web-group /NoveltyData/gpm/webroot
sudo chmod g+rw /NoveltyData/gpm/webroot
# Log Folders
sudo mkdir -vp /NoveltyData/gpm/logroot
sudo chown nobody:gpm-web-group /NoveltyData/gpm/logroot
sudo chmod g+rw /NoveltyData/gpm/logroot
# todo segregate logroot to allow log-group only read access

# Create Users
## Operator
sudo useradd -m -N -g gpm-operator-group -G sudo gpm-operator
echo -e "a14f8160-c72f4cd3-b1db8530-7e2c48a7\na14f8160-c72f4cd3-b1db8530-7e2c48a7" | sudo passwd gpm-operator

## FTP User
sudo useradd -m -s /bin/ftponly -d /NoveltyData/gpm/ftproot -g gpm-ftp-group -N gftp-user
echo -e "dd69-e728-117b-914f\ndd69-e728-117b-914f" | sudo passwd gftp-user
# Add user to FPT whitelist
echo "gftp-user" | sudo tee -a /etc/vsftpd.userlist
#TODO: Add one more user, for test, confirmation

## Web runner
sudo useradd -m -N -g gpm-web-group gweb-runner
echo -e "a5b1-445d-aa85-c08a-d347\na5b1-445d-aa85-c08a-d347" | sudo passwd gweb-runner

## Application Updater
sudo useradd -r -N -g gpm-web-group -G gpm-ftp-group gweb-updater
echo -e "a2b9-1789-a45f-d278-e9b2\na2b9-1789-a45f-d278-e9b2" | sudo passwd gweb-updater
## Log Viewer - TODO: analyse before implementation
# sudo useradd -r -N -g gpm-log-group -G gpm-web-group glog-viewer
# sudo passwd glog-viewer
# 	bf3f-6f10-bc63-b76b-29d6
# echo -e "bf3f-6f10-bc63-b76b-29d6\nbf3f-6f10-bc63-b76b-29d6" | sudo passwd glog-viewer

# Inspect users
getent passwd gpm-operator gftp-user gweb-runner gweb-updater glog-viewer
grep -i gftp-user /etc/passwd
lslogins -u

```

### FTP
- Read and apply from
	- 

- Open firewall [Configured in `gpm-web-nsg`]
	```sh
	sudo ufw allow 20/tcp
	sudo ufw allow 21/tcp
	sudo ufw allow 990/tcp # for TLS
	sudo ufw allow 40000:50000/tcp
	sudo ufw status
	```
- Install vsftpd
	```sh
	# sudo apt-get update
	# sudo apt-get install vsftpd
	sudo cp /etc/vsftpd.conf /etc/vsftpd.conf_orig
	sudo nano /etc/vsftpd.conf
	sudo systemctl disable vsftpd.service   # enable only when needed

	# Commands to inspect service
	systemctl status vsftpd.service
	systemctl stop vsftpd.service
	systemctl start vsftpd.service
	systemctl restart vsftpd.service
	```

- Test vsftpd
	```sh
	## Prepare test files
	echo "vsftpd test file" | sudo tee /NoveltyData/gpm/ftproot/files/test.txt
	echo "vsftpd upload test file" > file-up.txt

	## Connect
	localhost
	gftp-user
	dd69-e728-117b-914f

	## Get a file, put a file
	cd files
	ls
	get test.txt
	put file-up.txt file-up-svr.txt
	```

- Contents of /etc/vsftpd.conf - Working
	```
	listen_ipv6=YES
	anonymous_enable=NO
	local_enable=YES
	write_enable=YES
	chroot_local_user=YES
	local_root=/NoveltyData/gpm/ftproot
	pasv_min_port=40000
	pasv_max_port=50000
	userlist_enable=YES
	userlist_file=/etc/vsftpd.userlist
	userlist_deny=NO
	chmod_enable=YES
	file_open_mode=0444
	local_umask=0313
	ftpd_banner=The Gate Pass Manager FTP Service.
	# user_sub_token=$USER
	#
	# # Earlier test - also works
	# listen=NO
	# anonymous_enable=NO
	# local_enable=YES
	# write_enable=YES
	# local_umask=022
	# dirmessage_enable=YES
	# xferlog_enable=YES
	# connect_from_port_20=NO
	# # nopriv_user=gpmftp
	#
	# # Analyze and include
	xferlog_enable=YES
	xferlog_file
	vsftpd_log_file
	```

### dotnet core 
```sh
# testing code - begin
sudo ln -vsT /10-Base/DNC/dotnet /bin/dotnet
# testing code - end

# using var for convenient updates
DNETCORE_TAR="/NoveltyData/gpm/ftproot/files/installs/dotnet-sdk-2.2.108-linux-x64.tar.gz";

sudo mkdir -vp /NoveltyData/gpm/base-libs/dnc
sudo rm -vrf /NoveltyData/gpm/base-libs/dnc/*
sudo tar -xz -C /NoveltyData/gpm/base-libs/dnc -f ${DNETCORE_TAR};

sudo ln -vsT /NoveltyData/gpm/base-libs/dnc/dotnet /bin/dotnet

# Verify setup
dotnet --info
```

### Web Server
```sh
# sudo apt-get update
# sudo apt-get install apache2
# sudo a2enmod proxy proxy_http proxy_html
# systemctl restart apache2
# take a break and test http://127.0.0.1/

sudo mkdir -vp /NoveltyData/gpm/logroot/apache2/
sudo nano /etc/apache2/conf-enabled/gpm-web-hosts.conf
systemctl restart apache2

# check logging on the startup sequence
journalctl -xe
```

- Contents of /etc/apache2/conf-enabled/gpm-web-hosts.conf
	```
	<VirtualHost *:80>
	ProxyPreserveHost On
	ProxyPass / http://127.0.0.1:5020/
	ProxyPassReverse / http://127.0.0.1:5020/
	ErrorLog  /NoveltyData/gpm/logroot/apache2/gpm-web-hosts-error.log
	CustomLog /NoveltyData/gpm/logroot/apache2/gpm-web-hosts-access.log common
	</VirtualHost>
	<VirtualHost *:443>
	ProxyPreserveHost On
	ProxyPass / https://127.0.0.1:5030/
	ProxyPassReverse / https://127.0.0.1:5030/
	ErrorLog  /NoveltyData/gpm/logroot/apache2/gpm-web-hosts-error.log
	CustomLog /NoveltyData/gpm/logroot/apache2/gpm-web-hosts-access.log common
	</VirtualHost>
	```

- Status Commands
	```sh
	systemctl status apache2
	systemctl stop apache2
	systemctl start apache2
	systemctl restart apache2

	# check logging on the startup sequence
	journalctl -xe
	```

### Web Application
```sh
# sudo mkdir -vp /NoveltyData/gpm/webroot/gpm-web-site1
sudo nano /etc/systemd/system/gpm-web-app.service

sudo mkdir -vp /NoveltyData/gpm/base-libs/dnc/sdk/NuGetFallbackFolder
sudo chmod -R 666 /NoveltyData/gpm/base-libs/dnc/sdk/NuGetFallbackFolder

sudo systemctl enable gpm-web-app.service
sudo systemctl start gpm-web-app.service

```
- Contents of /etc/systemd/system/gpm-web-app.service
```
[Unit]
Description=Gate Pass Manager - Web application
[Service]
WorkingDirectory=/NoveltyData/gpm/webroot
ExecStart=/bin/dotnet /NoveltyData/gpm/webroot/gpm_web.dll --urls="http://*:5020;https://*:5030"
Restart=always
RestartSec=10
SyslogIdentifier=gpm-web
User=gweb-runner
Environment=ASPNETCORE_ENVIRONMENT=Production
[Install]
WantedBy=multi-user.target
```

- Status Commands
	```sh
	systemctl status gpm-web-app.service
	sudo systemctl stop gpm-web-app.service
	sudo systemctl start gpm-web-app.service
	sudo systemctl restart gpm-web-app.service

	# check logging on the startup sequence
	journalctl -xe
	```

### Prepare and Mount Data Disk
```sh
# Confirm sdc is our disk
dmesg | grep SCSI
# [    1.905403] SCSI subsystem initialized
# [    2.627211] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 243)
# [    3.067650] sd 0:0:0:0: [sda] Attached SCSI disk
# [    3.316411] sd 3:0:1:0: [sdb] Attached SCSI disk
# [    3.511462] sd 5:0:0:0: [sdc] Attached SCSI disk
# [    8.324248] Loading iSCSI transport class v2.0-870.


sudo fdisk /dev/sdc
# <- n
# <- p
# <- 1
# <- 
# <- 
# <- p
# <- w

sudo mkfs -t ext4 -v -L DB_DATA_DIR /dev/sdc1

sudo mkdir -vp /NoveltyData/work-t15/data-disk-2
sudo touch /NoveltyData/work-t15/data-disk-2/local-file.txt
ls -l /NoveltyData/work-t15/data-disk-2

sudo mount -v -t ext4 /dev/sdc1 /NoveltyData/work-t15/data-disk-2
ls -l /NoveltyData/work-t15/data-disk-2
sudo touch /NoveltyData/work-t15/data-disk-2/mount-file.txt
ls -l /NoveltyData/work-t15/data-disk-2

sudo -i blkid | sort
/dev/sda14: PARTUUID="598610d3-97c7-46d3-aae1-aea425e66b0d"
/dev/sda15: LABEL="UEFI" UUID="66CA-0134" TYPE="vfat" PARTUUID="cddf6177-2b9b-4d68-9685-58a5ef523881"
/dev/sda1: LABEL="cloudimg-rootfs" UUID="cd899ca9-1943-46c8-9c55-42fc136ef940" TYPE="ext4" PARTUUID="2f441e81-315b-4f69-8176-3c7b8a16729a"
/dev/sdb1: UUID="3d3284a1-0c7f-422f-9f98-f458a7eae338" TYPE="ext4" PARTUUID="135f77d7-01"
/dev/sdc1: LABEL="DB_DATA_DIR" UUID="fd8d858c-83ea-4ce1-9327-50494445c6a5" TYPE="ext4" PARTUUID="4f371dc9-01"

# Entry of interest
/dev/sdc1: LABEL="DB_DATA_DIR" UUID="fd8d858c-83ea-4ce1-9327-50494445c6a5" TYPE="ext4" PARTUUID="4f371dc9-01"

sudo nano /etc/fstab
UUID=fd8d858c-83ea-4ce1-9327-50494445c6a5   /NoveltyData/work-t15/data-disk-2   ext4   defaults,nofail   1   2

# To be used post mysql install
# UUID=fd8d858c-83ea-4ce1-9327-50494445c6a5   /var/lib/mysql   ext4   defaults,nofail   1   2

```

### Link Logroot to FTP root.
```sh
# Add Link
ln -vsT /NoveltyData/gpm/logroot /NoveltyData/gpm/ftproot/files/logroot;

# Appl permissions TBD
#	Check access before modifying
/NoveltyData/gpm/ftproot/files/logroot;
sudo chmod g+rw sudo chmod g+rw /NoveltyData/gpm/logroot
```

- Permissions
	- Owner
	- Group
	- Worls
- Flags
	- 4 => r => Read
	- 2 => w => Write
	- 1 => x => Execute


### Database
- Install
```sh
sudo apt-get update
sudo apt-get install mysql-client mysql-server
# root password if asked
# a3eae48da4e4f585d5a9f658e659b8f6

# mysql  Ver 14.14 Distrib 5.7.27, for Linux (x86_64) using  EditLine wrapper

sudo systemctl start mysql
sudo systemctl status mysql
journalctl -xe

# Test connectivity
sudo mysql
select @@datadir;

# -- Create dba login
# CREATE USER 'nds-dba'@'%' IDENTIFIED BY 'f089e093a1b6d2bef321b3e2';
# GRANT ALL PRIVILEGES ON * . * TO 'nds-dba'@'%';
# GRANT GRANT OPTION ON * . * TO 'nds-dba'@'%';

# Restrict access to schema only - TODO: after DB is created
#	CREATE USER 'gpm-dba'@'%' IDENTIFIED BY 'a3eae48da4e4f585d5a9f658';
#	GRANT ALL PRIVILEGES ON * . * TO 'gpm-dba'@'%';
#	GRANT GRANT OPTION ON * . * TO 'gpm-dba'@'%';
```

- Move db root to data disk
```sh
# before starting

# Inspect User and Group
sudo ls -l /var/lib/ | grep mysql
# drwx------  5 mysql     mysql     4096 Sep  5 08:23 mysql
# drwx------  2 mysql     mysql     4096 Sep  5 08:23 mysql-files
# drwx------  2 mysql     mysql     4096 Sep  5 08:23 mysql-keyring
# drwxr-xr-x  2 root      root      4096 Jul 22 18:02 mysql-upgrade

sudo cat /etc/passwd | grep mysql
# mysql:x:112:117:MySQL Server,,,:/nonexistent:/bin/false
getent group | grep mysql
# mysql:x:117:
groups mysql
# mysql : mysql

sudo systemctl stop mysql
sudo systemctl status mysql

# disable autostart, till data disk mount is tested
sudo systemctl disable mysql;

# Copy contents to new location
sudo chown -R mysql:mysql /NoveltyData/work-t15/data-disk-2
sudo rsync -av /var/lib/mysql/ /NoveltyData/work-t15/data-disk-2
sudo ls -l /NoveltyData/work-t15/data-disk-2/

# Unmount from current mount
sudo umount -v /dev/sdc1

# Mount to /var/lib/mysql
sudo mount -v -t ext4 /dev/sdc1 /var/lib/mysql

# Start DB
sudo systemctl start mysql
sudo systemctl status mysql

# Test DB
sudo mysql;
# SELECT User, Host, plugin, password_expired, password_last_changed, password_lifetime, account_locked FROM mysql.user;

# Create new user to verify after reboot
CREATE USER 'dd-move-test'@'%' IDENTIFIED BY 'ca1bba98aa9fdb1f';

# Move mount point
sudo nano /etc/fstab
UUID=fd8d858c-83ea-4ce1-9327-50494445c6a5   /var/lib/mysql   ext4   defaults,nofail   1   2

# reboot, restart DB and check test user
sudo systemctl stop mysql
sudo systemctl status mysql

## REBOOT INSTANCE ##

sudo systemctl status mysql
sudo ls -l /var/lib/mysql
sudo systemctl start mysql
sudo systemctl status mysql

sudo mysql;
# SELECT User, Host, plugin, password_expired, password_last_changed, password_lifetime, account_locked FROM mysql.user;

# test with created user
sudo mysql -v -h 127.0.0.1 -P 3306 -u nds-dba -p
	# f089e093a1b6d2bef321b3e2
	# show processlist;

# enabe if all is well
sudo systemctl enable mysql;
```

## DB Initialize
```sh
sudo mysql -v -h 127.0.0.1 -P 3306 -u nds-dba -p

source /NoveltyData/gpm/ftproot/files/dbscripts/db-10-init.ddl
source /NoveltyData/gpm/ftproot/files/dbscripts/db-20a-create-tables.ddl
source /NoveltyData/gpm/ftproot/files/dbscripts/db-50a-seed.sql


```

## x

## TODO
- harden db security
	- https://dev.mysql.com/doc/refman/5.7/en/default-privileges.html
	```sql
	ALTER USER 'root'@'localhost' IDENTIFIED BY 'a3eae48da4e4f585d5a9f658e659b8f6';
	```
- Change mysql port number to 6699
	- sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf
- change ssh port number to 
- Make link in FTP root for log folders
	- /NoveltyData/gpm/logroot
