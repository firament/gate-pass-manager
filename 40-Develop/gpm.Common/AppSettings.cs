﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace gpm.Common
{
	public class AppSettingContainer : IAppSettingContainer
	{
		public AppSettingContainer()
		{
			// Initialize non-primitive props
		}

		public int AppVersion { get; set; } = 0;

		[JsonIgnore]
		public string DBConnectionStringRO { get; set; }
		[JsonIgnore]
		public string DBConnectionStringRW { get; set; }

		public string AuthKeyName { get; set; } = "NOT-SET";

		public Dictionary<int, IDeptDayCap> DayDeptCapacities { get; set; } = new Dictionary<int, IDeptDayCap>();
		public IASCSessionSettings AuthCookieOpts { get; set; } = new ASCSessionSettings();

		[JsonIgnore]
		public DateTime LastLoaded { get; set; } = DateTime.Now;

		public IAppSettingContainer FromJSON(string JSONText){
			AppSettingContainer loSettings = null;

			// ensure we have something to parse
			if (string.IsNullOrWhiteSpace(JSONText))
			{
				return loSettings;
			}

			loSettings = Utils.FromJSON(JSONText, loSettings.GetType()) as AppSettingContainer;
			return loSettings;
		}

		public string ToJSON(){
			string lsJson = Utils.ToJSON(this, true);
			return lsJson;
		}

	}
	
}
