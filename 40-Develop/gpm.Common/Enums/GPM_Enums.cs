namespace gpm.Common.Enums
{

	/// <summary>
	/// 
	/// </summary>
	/// <remarks>from caweb</remarks>
	public enum ErrorCode
	{
		NOT_SET = -99,
		UnknownError = -1,
		SUCCESS = 0,
		BadParameters = 1,
		ExecutionError = 2,
		ValidationError = 3,
		NoChangesDone = 4,
		NoMatchFound = 5,
		Partial_Success = 6,
		Already_Exists = 7,
	}

}
