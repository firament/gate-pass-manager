using GC = gpm.Common;


namespace gpm.Common
{
	public static class wf
	{

		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		public static string AppSettingsSampleJSON()
		{
			AppSettingContainer lASC = new AppSettingContainer();

			// Fill Sample Department capacities
			string[] laTags = new string[] { "_", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O" };
			string[] laNames = new string[] { "________________________", "0d803553312139ec5c792ad6", "e1a130c5bba2585c0cb2e3e5", "a628c0e03c48121efdeb17f1", "435f77c81105814a9f403f4a", "53a3952157919aaabd0f205d", "9283bc4fb5fe96a9d5ef6fb9", "de638b5bf04815e08532a907", "f103ff841cb53c84a0595494", "74876c70fedf643c42f6efe4", "fcd2f7a1c24cab82b3090011", "da3a1ec2a43778a935959bc9", "7436b79f5f04cdbf7a207c86", "bc8b90e2ac4a3620ae9d0a75", "d606bef28d0608863a7f1983", "b1e6e9d723fefa0e749470c7", "fac0682a39b69ed7060f1ecc", "f3432c961eb450499339946a", "5a6920e21216a2bd396fb180", "d6daf05fdcb4dde37323775c", "69494bd2d90f0a0bbb1254cc" };
			System.Random L_RndGen = new System.Random(System.DateTime.Now.Millisecond);
			for (int vI = 1; vI < 11; vI++)
			{
				lASC.DayDeptCapacities.Add(vI
					, new DeptDayCap()
					{
						DeptCode = vI,
						DeptTag = laTags[vI],
						DeptName = laNames[vI],
						CapFirst = vI * 10,
						CapRevisit = vI * 51,
					}
				);
			}

			string lsJSON = lASC.ToJSON();
			return lsJSON;
		}
	}
}
