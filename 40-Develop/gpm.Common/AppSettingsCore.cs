﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace gpm.Common
{
	#region Interfaces
	public interface IAppSettingContainer
	{
		int AppVersion { get; set; }

		/// <summary>
		/// Required for opening DB Connections from libraries.
		/// </summary>
		/// <value></value>
		[JsonIgnore]
		string DBConnectionStringRO { get; set; }
		/// <summary>
		/// Required for opening DB Connections from libraries.
		/// </summary>
		/// <value></value>
		[JsonIgnore]
		string DBConnectionStringRW { get; set; }

		/// <summary>
		/// Name of the kay to get authorization key from
		/// </summary>
		/// <value></value>
		string AuthKeyName { get; set; }

		IASCSessionSettings AuthCookieOpts { get; set; }
		
		/// <summary>
		/// One entry per Department
		/// </summary>
		/// <value></value>
		Dictionary<int, IDeptDayCap> DayDeptCapacities { get; set; }
		
		[JsonIgnore]
		DateTime LastLoaded { get; set; }

		// Methods
		/// <summary>
		/// Initializes the container from serialized JSON text
		/// Usually from the database.
		/// </summary>
		/// <param name="JSONSText"></param>
		/// <returns></returns>
		IAppSettingContainer FromJSON(string JSONSText);
		string ToJSON();
	}
	public interface IDeptDayCap
	{
		int DeptCode { get; set; }
		string DeptTag { get; set; }
		string DeptName { get; set; }
		int CapFirst { get; set; }
		int CapRevisit { get; set; }
		int UsedFirst { get; set; }
		int UsedRevisit { get; set; }
		DateTime LastDBSet { get; set; }
	}
	public interface IASCSessionSettings
	{
		string AuthSchemeName { get; set; }
		string AuthCookieName { get; set; }
		string AuthCookieDomain { get; set; }
		string AccessDenyPath { get; set; }
		string SignOnPath { get; set; }
		string SignOutPath { get; set; }
		string ReturnURL_TagName { get; set; }
		TimeSpan SessionTimeout { get; set; }
	}
	#endregion Interfaces

	#region Classes

	public class DeptDayCap : IDeptDayCap
	{
		public int DeptCode { get; set; } = 0;
		public string DeptTag { get; set; } = "XX";
		public string DeptName { get; set; } = "Not Set";
		public int CapFirst { get; set; } = 99;
		public int CapRevisit { get; set; } = 199;
		public int UsedFirst { get; set; } = 0;
		public int UsedRevisit { get; set; } = 0;

		public DateTime LastDBSet { get; set; } = DateTime.Now;
	}

	public class ASCSessionSettings : IASCSessionSettings{
		// public string AuthSchemeName { get; set; } = "GPMIdentityManager";
		public string AuthSchemeName { get; set; } = "gpmauth";
		
		public string AuthCookieName { get; set; }  = "gpmauth";
		public string AuthCookieDomain { get; set; }  = "gatepass.sssihms.org";
		public string AccessDenyPath { get; set; }  = "/Pages/Code_403.html";
		public string SignOnPath { get; set; }  = "/Session/Login/";
		public string SignOutPath { get; set; }  = "/Session/Logout/";
		public string ReturnURL_TagName { get; set; }  = "gpmAuthFailURL";
		public TimeSpan SessionTimeout { get; set; } = new TimeSpan(0, 20, 0);
	}

	#endregion  Classes



}
