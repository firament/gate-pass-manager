# Discussion post Parallel run 2
> 2019 Dec 20
> Telephonic (DV & sak)
 

## I. Items discussed

### A. Power Users
1. A New Role for users with privilages between Administrators and Users
1. Power users should have the following features in addition to User features
    1. Quota Update
    2. Current User List
    3. 2-factor login switch
    4. User Hotlist
    5. Active users dashboard
    6. Activity Logs
    7. Turned away users Dashboard

1. Quota update
    1. Quota updates and maintainence to be performed by each department
    1. Each department will be assigned one or more Power User logins
    1. Powers users will ensure due diligence to update only the quotas of their department correctly

1. Active users dashboard
    1. Display list of all users currently logged into the system
    1. Time of last transaction by a user
    1. Number of gatepasses issued in last 'n' hours
    1. Hotlist any suspicious user
    1. De-Hotlist a user hotlisted by any power user


### B. Off duty access
1. 2-factor login switch
    1. Login attempt by a user should be authorized by a power user before the user is allowed into the system
    1. Switchable by power user
    1. When switched on and a user logs in,  a notification goes to the power user to allow or block the login
    1. User login is successful only when the power user manually authorizes the login

1. User Hotlist
    1. Instantly block a user login from accessing the system
    2. A hotlisted user will be denied any access to the system until the hotlist is revoked
    3. Power user can hotlist or de-hotlist any user in the system
    4. For use when a malicious user is suspected to heve gained access to the system
    5. or when someone attempts to access the system during off-duty hours


### C. Activity Logs
1. Extension of Active users dashboard
1. Passess created by each user in last 'n' hours
1. Changes to quotas (last 'n' changes)
    - with details of each change


### D. Turned away users
1. When quota of a department is consumed, and patients need to be turned away,
1. Keep a count of patients turned away for the day
1. This information is expected to help in capacity planning
1. One option is to change the button for the department to read as 'Turn Away'
    - When a patient is turned aaway from a department, the counter is incremented each time the 'Turn Away' button is clicked.
1. A report/dashboard page to display patients turned for the day, for each department


### E. WiFi Hotspot
1. White list devices
    - Based on MACID
    - Allow only listed devices to connect to the hotspot
2. Restrict outgoing addresseses/IP
    - Configure hotspot to allow traffic to only the specified addresses
    - To prevent misuse of network towards non-work usage
3. Inputs from **Ram**
    - Use a Router/Switch with built-in dongle
    - Medium configuration Routers/Switches support the desired settings
    - WiFi range should be sufficient to enable one device per gate, 
    - Device installation location to be studied once the model is shortlisted
    - Ram will be identifying the models that support the desired features and are cost-effective


### F. Tab Specs
1. **TODO:** RnD on fingerprint authentication from browser
1. Access Camera from browser,
    - (to scan Patients Hospital ID)
1. Tab case usable as a stand
1. Screen protection
1. Barcode scanner integration
1. Penultimate minimum hardware configuration
1. Ram will be identifying the models that support the desired features and are cost-effective
1. One feasible suggestion from Ram is
    - ??

***


## II. Observations
> From the 2nd Parallel Run

### A. Keyboard
1. Users needed to switch their keypads between alphabets and numbers panel more than once for each gatepass
1. A keyboard with both alphabets and numbers in one panel should improve time per gatepass significantly
1. Suggested keyboard to be installed on all user devices is
    - [CodeBoard Keyboard](https://play.google.com/store/apps/details?id=com.gazlaws.codeboard&hl=en_IN)
    - Key size and colors are configurable
    - Arrow key navigation reduces time to correct typing errors
    - Developed and maintained by [National Institute of Technology Calicut](http://www.nitc.ac.in/)

### B. Pairs
1. Users working in pairs were observed to be faster than single user
1. One user interacts with the patient, asking for details and inspecting any documents shown by the patient
1. The partner user inputs data to the system based on the answers
1. This situation may change when the users become familiar and profecient with the system


### D. Defects

1. Button Text
    - Change button text in Department Selection screen 
    - from 'Follow up' to 'Revisit'

2. Quota Cache
    1. Updated Quota was not reflecting on the gatepass screen
    2. Due to the browser caching the quota information
    3. Caching is done for improving the performance of the system
    4. TODO: Turn off caching, or expire the cache whenever any quota is updated.

***

## III. Next Step
1. Implement thermal printing functionality
1. Setup and configure WiFi hotspot(s)
1. Perform Parrallel run 3, with new tickets
1. Analyze new features identified in the 'Items discussed' section
1. Develop the new features
1. Implement the new features

***
