# Discussion pre go-live
> 2021 Jan 21
> Telephonic (DV & sak)
> 
> **Daywise quota template:** /70-CurrentWork/NDS/gate-pass-manager/30-Design/310-DB/GPM-Daywise-quotas-210122.xlsx

## Items discussed

### Day-Wise Quotas
- Each day of the week has different quotas
    - Quota defaults should be set per day of week
    - This is different from current working, and needs to be implemented
    - Appx timeline is 2 weeks, given the current limited productivity
- For go live, load day wise data directly into the DB, using spredsheet
    - For 2-4 weeks
    - **Daywise quota template:** `GPM-Daywise-quotas-210122.xlsx`


### Go-Live
- Go live with few departments/specialities
- Plan for weekend, if weekend does not happen, Monday morning will be go-live with handwritten tickets
- Users will use personal handsets to connect to the application
- Inactive and other non-active users should be made permanently disabled
    - This will be done by implementing delete (soft) functionality that will make the users permanently disabled

***
eof
